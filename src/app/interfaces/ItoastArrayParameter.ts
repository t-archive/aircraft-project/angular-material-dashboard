import {ItoastParameter, ToastParameter} from "./ItoastParameter";
import {ToastStatus} from "../keys/Requirements";

export interface ItoastArrayParameter {
    data: ItoastParameter;
    orderNumber: number;
    status: ToastStatus;
}

export class ToastArrayParameter implements ItoastArrayParameter {

    data: ItoastParameter;
    orderNumber: number;
    status: ToastStatus;

    constructor(data: ItoastArrayParameter) {
        this.orderNumber = data.orderNumber;
        this.status = data.status;
        this.data = new ToastParameter(data.data);
        return {
            data: this.data,
            orderNumber: this.orderNumber,
            status: this.status
        };
    }
}
