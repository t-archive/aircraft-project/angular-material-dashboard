import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {ItoastParameter, ToastParameter} from "./ItoastParameter";
import {ToastStatus} from "../keys/Requirements";
import {ItoastArrayParameter} from "./ItoastArrayParameter";
import {FlightDataComponent} from "../components/flight-data/flight-data.component";
import {IflightDetails} from "./Iflight-details";
import {IflightBasicData} from "./Iflight-basic-data";

export interface IflightDetailsModalData {
    aircraft: IflightBasicData;
    flightDetails: IflightDetails;
    self: FlightDataComponent;
}
export class FlightDetailsModalData implements IflightDetailsModalData {

    aircraft: IflightBasicData;
    flightDetails: IflightDetails;
    self: FlightDataComponent;

    constructor(data: IflightDetailsModalData) {
        this.aircraft = data.aircraft;
        this.flightDetails = data.flightDetails;
        this.self = data.self;
        return {
            aircraft: this.aircraft,
            flightDetails: this.flightDetails,
            self: this.self
        };
    }
}
