import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {IflightDataTime} from "./Iflight-data-time";
import {IflightDetails} from "./Iflight-details";

export interface IflightTimeSpanData {
    alreadyFlown: number;
    arrival: number;
    alreadyFlownDate: string;
    arrivalDate: string;
}
export class FlightTimeSpanData implements IflightTimeSpanData {

    alreadyFlown: number;
    arrival: number;
    alreadyFlownDate: string;
    arrivalDate: string;

    constructor(data: IflightTimeSpanData) {
        this.alreadyFlown = data.alreadyFlown;
        this.arrival = data.arrival;
        this.alreadyFlownDate = data.alreadyFlownDate;
        this.arrivalDate = data.arrivalDate;
        return {
            alreadyFlown: this.alreadyFlown,
            arrival: this.arrival,
            alreadyFlownDate: this.alreadyFlownDate,
            arrivalDate: this.arrivalDate
        };
    }
}
