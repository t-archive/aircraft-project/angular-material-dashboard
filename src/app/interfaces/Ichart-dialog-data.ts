import {BehaviorSubject} from "rxjs";
import {AircraftRepository} from "../repository/AircraftRepository";
import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";

export interface IchartDialogData {
    aircraft: Aircraft | CivilAircraft | MilitaryAircraft;
    behaviorSubject: BehaviorSubject<AircraftRepository>;
    type: string;
}

export class ChartDialogDataInterface implements IchartDialogData {
    aircraft: Aircraft | CivilAircraft | MilitaryAircraft;
    behaviorSubject: BehaviorSubject<AircraftRepository>;
    type: string;

    constructor(data: IchartDialogData) {
        this.aircraft = data.aircraft;
        this.behaviorSubject = data.behaviorSubject;
        this.type = data.type;
        return {
            aircraft: this.aircraft,
            behaviorSubject: this.behaviorSubject,
            type: this.type
        };
    }

}
