export interface IaverageDataMinMax {
    distance?: number;
    seatCount?: number;
    maxSeatCount?: number;
    length?: number;
    wingSpan?: number;
    height?: number;
    consumption?: number;
}
