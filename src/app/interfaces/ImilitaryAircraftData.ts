interface ImilitaryAircraftData extends IaircraftData {
    flare: boolean;
    airRefueling: boolean;
}
