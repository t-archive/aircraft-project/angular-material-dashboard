import {IflightTimeSpanData} from "./Iflight-time-span-data";

export interface IflightDataTime {
    departure?: number;
    arrival?: number;
    departureTime?: string;
    arrivalTime?: string;
    flightTime?: number;
    flightTimeCalc?: number;
    flightTimePercent?: number;
    flightTimeData?: IflightTimeSpanData;
}

export class FlightDataTime implements IflightDataTime {

    departure: number;
    arrival: number;
    departureTime: string;
    arrivalTime: string;
    flightTime: number;
    flightTimeCalc: number;
    flightTimePercent: number;
    flightTimeData: IflightTimeSpanData;

    constructor(data: IflightDataTime) {
        this.departure = data.departure;
        this.arrival = data.arrival;
        this.departureTime = data.departureTime;
        this.arrivalTime = data.arrivalTime;
        this.flightTime = data.flightTime;
        this.flightTimeCalc = data.flightTimeCalc;
        this.flightTimePercent = data.flightTimePercent;
        this.flightTimeData = data.flightTimeData;

        return {
            departure: this.departure,
            arrival: this.arrival,
            departureTime: this.departureTime,
            arrivalTime: this.arrivalTime,
            flightTime: this.flightTime,
            flightTimeCalc: this.flightTimeCalc,
            flightTimePercent: this.flightTimePercent,
            flightTimeData: this.flightTimeData
        };
    }
}
