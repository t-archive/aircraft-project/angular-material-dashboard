import {IflightDataTime} from "./Iflight-data-time";
import {IflightDetails} from "./Iflight-details";

export interface IflightBasicData {
    "0"?: any;
    "1"?: any;
    "2"?: any;
    "3"?: any;
    "4"?: any;
    "5"?: any;
    "6"?: any;
    "7"?: any;
    "8"?: any;
    "9"?: any;
    "10"?: any;
    "11"?: any;
    "12"?: any;
    "13"?: any;
    "14"?: any;
    "15"?: any;
    "16"?: any;
    "17"?: any;
    "18"?: any;
    id?: any;
    result?: any;
    status?: any;
    airline?: any;
    from?: any;
    to?: any;
}
export class FlightBasicData implements IflightBasicData {

    "0": any;
    "1": any;
    "2": any;
    "3": any;
    "4": any;
    "5": any;
    "6": any;
    "7": any;
    "8": any;
    "9": any;
    "10": any;
    "11": any;
    "12": any;
    "13": any;
    "14": any;
    "15": any;
    "16": any;
    "17": any;
    "18": any;
    id: any;
    result: any;
    status: any;
    airline: any;
    from: any;
    to: any;

    constructor(data: IflightBasicData) {
        this["0"] = data["0"];
        this["1"] = data["1"];
        this["2"] = data["2"];
        this["3"] = data["3"];
        this["4"] = data["4"];
        this["5"] = data["5"];
        this["6"] = data["6"];
        this["7"] = data["7"];
        this["8"] = data["8"];
        this["9"] = data["9"];
        this["10"] = data["10"];
        this["11"] = data["11"];
        this["12"] = data["12"];
        this["13"] = data["13"];
        this["14"] = data["14"];
        this["15"] = data["15"];
        this["16"] = data["16"];
        this["17"] = data["17"];
        this["18"] = data["18"];
        this.id = data.id;
        this.result = data.result;
        this.status = data.status;
        this.airline = data.airline;
        this.from = data.from;
        this.to = data.to;
        return {
            "0": data["0"],
            "1": data["1"],
            "2": data["2"],
            "3": data["3"],
            "4": data["4"],
            "5": data["5"],
            "6": data["6"],
            "7": data["7"],
            "8": data["8"],
            "9": data["9"],
            "10": data["10"],
            "11": data["11"],
            "12": data["12"],
            "13": data["13"],
            "14": data["14"],
            "15": data["15"],
            "16": data["16"],
            "17": data["17"],
            "18": data["18"],
            id: data.id,
            result: data.result,
            status: data.status,
            airline: data.airline,
            from: data.from,
            to: data.to
        };
    }
}
