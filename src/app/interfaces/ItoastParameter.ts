import {SweetAlertIconRequirements} from "../keys/Requirements";

export interface ItoastParameter {
    status: SweetAlertIconRequirements;
    text: string;
    time?: number;
}

export class ToastParameter implements ItoastParameter {
    status: SweetAlertIconRequirements;
    text: string;
    time?: number;

    constructor(data: ItoastParameter) {
        this.status = data.status;
        this.text = data.text;
        this.time = data.time;
        return {
            status: this.status,
            text: this.text,
            time: this.time
        };
    }

}
