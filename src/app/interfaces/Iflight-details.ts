import {IflightDataTime} from "./iflight-data-time";
import {IflightBasicData} from "./Iflight-basic-data";

export interface IflightDetails {
    action: boolean;
    data?: IflightBasicData;
    previewPicture?: string;
    airlineLogo?: string;
    details?: object | any;
    age?: string | number;
    time?: IflightDataTime;
}

export class FlightDetails implements IflightDetails {

    action: boolean;
    data: IflightBasicData;
    previewPicture: string;
    airlineLogo: string;
    details: object;
    age: string | number;
    time: IflightDataTime;

    constructor(data: IflightDetails) {
        this.action = data.action;
        this.data = data.data;
        this.previewPicture = data.previewPicture;
        this.airlineLogo = data.airlineLogo;
        this.details = data.details;
        this.age = data.age;
        this.time = data.time;
        return {
            action: this.action,
            data: this.data,
            previewPicture: this.previewPicture,
            airlineLogo: this.airlineLogo,
            details: this.details,
            age: this.age,
            time: this.time
        };
    }
}
