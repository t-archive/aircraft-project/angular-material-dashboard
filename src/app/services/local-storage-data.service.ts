import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class LocalStorageDataService {

  constructor() { }
}

// localStorage.setItem('key', value);
// localStorage.setItem('key', JSON.stringify(object));
// localStorage.getItem('key');
// JSON.parse(localStorage.getItem('key'));
