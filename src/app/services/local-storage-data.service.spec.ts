import { TestBed } from '@angular/core/testing';

import { LocalStorageDataService } from './local-storage-data.service';

describe('LocalStorageDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalStorageDataService = TestBed.get(LocalStorageDataService);
    expect(service).toBeTruthy();
  });
});
