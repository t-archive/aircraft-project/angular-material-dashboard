import {Inject, Injectable} from "@angular/core";
import {AircraftService} from "./AircraftService";
import {Aircraft} from "../aircrafts/Aircraft";
import {IchartDialogData} from "../interfaces/Ichart-dialog-data";
import {UnitExtension} from "../keys/UnitExtension";
import {AircraftTitle} from "../keys/AircraftTitle";
import {AircraftKeys} from "../keys/AircraftKeys";
import {Dictionary} from "../keys/Dictionary";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {Chart} from "chart.js";

import {Label} from "ng2-charts";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";

import {IaverageDataMinMax} from "../interfaces/iaverage-data-min-max";
import {AverageData} from "../class/AverageData";
import {AircraftRepository} from "../repository/AircraftRepository";
import {AppRoutingModule} from "../app-routing.module";
import {SimilarAircraft} from "../class/SimilarAircraft";


@Injectable({
    providedIn: "root"
})
export class ChartService {
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();


    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _data: IchartDialogData;
    private _chartLabels: Label[] = [this.aircraftTitle.distance, this.aircraftTitle.seatCount, this.aircraftTitle.maxSeatCount,
        this.aircraftTitle.length, this.aircraftTitle.wingSpan, this.aircraftTitle.height, this.aircraftTitle.consumption];
    private _chart: any;
    private averageDataAll: AverageData = new AverageData();
    private averageDataCivil: AverageData = new AverageData();
    private averageDataMilitary: AverageData = new AverageData();
    private minDataAll: IaverageDataMinMax;
    private maxDataAll: IaverageDataMinMax;
    private minDataCivil: IaverageDataMinMax;
    private maxDataCivil: IaverageDataMinMax;
    private minDataMilitary: IaverageDataMinMax;
    private maxDataMilitary: IaverageDataMinMax;

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService) {


        this._aircraftService.allAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {
                this.averageDataAll.start(this._aircraftService.allAircraftsBehaviorSubject.getValue().aircrafts);
                this.minDataAll = this.averageDataAll.minData;
                this.maxDataAll = this.averageDataAll.maxData;
            }
        });
        this._aircraftService.civilAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {
                this.averageDataCivil.start(this._aircraftService.civilAircraftsBehaviorSubject.getValue().aircrafts);
                this.minDataCivil = this.averageDataCivil.minData;
                this.maxDataCivil = this.averageDataCivil.maxData;
            }
        });
        this._aircraftService.militaryAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {
                this.averageDataMilitary.start(this._aircraftService.militaryAircraftsBehaviorSubject.getValue().aircrafts);
                this.minDataMilitary = this.averageDataMilitary.minData;
                this.maxDataMilitary = this.averageDataMilitary.maxData;
            }
        });

    }

    public setData(data: IchartDialogData, from: string = ""): any {
        this.data = data;
        let average: any;
        let minData: IaverageDataMinMax;
        let maxData: IaverageDataMinMax;
        if (this.data.type === this.aircraftKeys.all) {
            average = this.averageDataAll;
            minData = this.minDataAll;
            maxData = this.maxDataAll;
        } else if (this.data.type === this.aircraftKeys.civil) {
            average = this.averageDataCivil;
            minData = this.minDataCivil;
            maxData = this.maxDataCivil;
        } else if (this.data.type === this.aircraftKeys.military) {
            average = this.averageDataMilitary;
            minData = this.minDataMilitary;
            maxData = this.maxDataMilitary;
        }
        const similarAircrafts: SimilarAircraft = new SimilarAircraft(this.data.behaviorSubject.getValue().aircrafts, true);
        const similarAircraftsResult: any = similarAircrafts.getSimilarAircraft(this.data.aircraft);
        return {
            averageData: average,
            minData: minData,
            maxData: maxData,
            percent: this.compileToPercent(data.aircraft, minData, maxData),
            similarAircrafts: similarAircraftsResult
        };
    }

    private compileToPercent(aircraft: Aircraft | CivilAircraft | MilitaryAircraft, minData: any, maxData: any): object {
        if (aircraft) {
            const distance: number = (100 * (aircraft.distance - minData.distance)) / (maxData.distance - minData.distance);
            const seatCount: number = (100 * (aircraft.seatCount - minData.seatCount)) / (maxData.seatCount - minData.seatCount);
            const maxSeatCount: number = (100 * (aircraft.maxSeatCount - minData.maxSeatCount)) / (maxData.maxSeatCount - minData.maxSeatCount);
            const length: number = (100 * (aircraft.length - minData.length)) / (maxData.length - minData.length);
            const wingSpan: number = (100 * (aircraft.wingSpan - minData.wingSpan)) / (maxData.wingSpan - minData.wingSpan);
            const height: number = (100 * (aircraft.height - minData.height)) / (maxData.height - minData.height);
            const consumption: number = (100 * (aircraft.consumption - minData.consumption)) / (maxData.consumption - minData.consumption);
            const distanceReserve: number = 100 - distance;
            const seatCountReserve: number = 100 - seatCount;
            const maxSeatCountReserve: number = 100 - maxSeatCount;
            const lengthReserve: number = 100 - length;
            const wingSpanReserve: number = 100 - wingSpan;
            const heightReserve: number = 100 - height;
            const consumptionReserve: number = 100 - consumption;


            return {
                "aircraft": {
                    "distance": distance,
                    "seatCount": seatCount,
                    "maxSeatCount": maxSeatCount,
                    "length": length,
                    "wingSpan": wingSpan,
                    "height": height,
                    "consumption": consumption
                },
                "aircraftReserve": {
                    "distance": distanceReserve,
                    "seatCount": seatCountReserve,
                    "maxSeatCount": maxSeatCountReserve,
                    "length": lengthReserve,
                    "wingSpan": wingSpanReserve,
                    "height": heightReserve,
                    "consumption": consumptionReserve
                }
            };
        }

    }


    get data(): IchartDialogData {
        return this._data;
    }

    set data(value: IchartDialogData) {
        this._data = value;
    }


    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get chart(): any {
        return this._chart;
    }

    set chart(value: any) {
        this._chart = value;
    }

    get chartLabels(): Label[] {
        return this._chartLabels;
    }

}
