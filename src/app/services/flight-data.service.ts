import {Inject, Injectable} from "@angular/core";
import {MatTableDataSource} from "@angular/material";
import {HttpClient} from "@angular/common/http";
import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {debounceTime} from "rxjs/operators";
import {InformationNotificationService} from "./information-notification.service";
import {FlightBasicData, IflightBasicData} from "../interfaces/Iflight-basic-data";
import {Dictionary} from "../keys/Dictionary";
import {environment} from "../../environments/environment";
import {LoginService} from "./login.service";

@Injectable({
    providedIn: "root"
})
export class FlightDataService {
    public aircraftListData: object = {};
    private _dictionary: Dictionary = new Dictionary();
    private _debouncer: Subject<any> = new Subject<any>();
    private _aircraftCode: any;
    private _airlinesCode: any;
    private _airportsCode: any;
    private _countriesCode: any;
    private _statusSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private _status: boolean = false;
    private checkLoadDataCount: number = 0;
    private subjectCode: Subject<any> = new Subject();

    constructor(
        @Inject(HttpClient) private http: HttpClient,
        @Inject(InformationNotificationService) private informationNotificationService: InformationNotificationService,
        @Inject(LoginService) private loginService: LoginService
    ) {
        this.checkTimeDeleteLS();
        this.firstStartLoadDataFromLocalStorage();
        this._debouncer.pipe(debounceTime(400))
            .subscribe((value) => {
                this.aircraftListData[value.result["icao"]] = value;
                this.putAircraft(value.result["icao"], value);
                // setInterval(() => this.putAircraft(value.result["icao"], value), 30000);

            });

        this._statusSubject.subscribe((data: boolean) => {
            this._status = data;
        });
        const checkLoadData: any = setInterval(() => {
            if (this._aircraftCode && this._airlinesCode && this._airportsCode && this._countriesCode) {
                this._statusSubject.next(true);
                clearInterval(checkLoadData);
            } else {
                this.firstStartLoadDataFromLocalStorage();
            }
            this.checkLoadDataCount === 180 ? window.location.reload() : this.checkLoadDataCount++;
        }, 250);
    }

    private checkTimeDeleteLS(): void {
        const now: number = Math.floor(new Date().getTime() / 1000);
        const deleteTime: number = environment.localStorageDeleteTime;
        const _aircraftCodeTime: number = Number(localStorage.getItem("aircraftCodeTime"));
        const _airlinesCodeTime: number = Number(localStorage.getItem("airlinesCodeTime"));
        const _airportsCodeTime: number = Number(localStorage.getItem("airportsCodeTime"));
        const _countriesCodeTime: number = Number(localStorage.getItem("countriesCodeTime"));
        if (_aircraftCodeTime + deleteTime < now) {
            localStorage.removeItem("aircraftCodeTime");
            localStorage.removeItem("aircraftCode");
        }
        if (_airlinesCodeTime + deleteTime < now) {
            localStorage.removeItem("airlinesCodeTime");
            localStorage.removeItem("airlinesCode");
        }
        if (_airportsCodeTime + deleteTime < now) {
            localStorage.removeItem("airportsCodeTime");
            localStorage.removeItem("airportsCode");
        }
        if (_countriesCodeTime + deleteTime < now) {
            localStorage.removeItem("countriesCodeTime");
            localStorage.removeItem("countriesCode");
        }
    }

    private firstStartLoadDataFromLocalStorage(): void {
        // localStorage.setItem(key, 'Value');
        const _aircraftCode: any = JSON.parse(localStorage.getItem("aircraftCode"));
        const _airlinesCode: any = JSON.parse(localStorage.getItem("airlinesCode"));
        const _airportsCode: any = JSON.parse(localStorage.getItem("airportsCode"));
        const _countriesCode: any = JSON.parse(localStorage.getItem("countriesCode"));
        if (_aircraftCode) {
            this._aircraftCode = _aircraftCode;
        } else {
            this.getAircraftCode();
        }
        if (_airlinesCode) {
            this._airlinesCode = _airlinesCode;
        } else {
            this.getAirlinesCode();
        }
        if (_airportsCode) {
            this._airportsCode = _airportsCode;
        } else {
            this.getAirportsCode();
        }
        if (_countriesCode) {
            this._countriesCode = _countriesCode;
        } else {
            this.getCountriesCode();
        }
    }

    private firstStartLoadData(): void {
        this.getAircraftCode();
        this.getAirlinesCode();
        this.getAirportsCode();
        this.getCountriesCode();
    }

    private getAircraftCode(): void {
        this.http.get(environment.baseApiEndpoint + "/aircrafts/aircraftCode").subscribe((data: any) => {
            if (data) {
                this._aircraftCode = data;
                localStorage.setItem("aircraftCode", JSON.stringify(data));
                localStorage.setItem("aircraftCodeTime", Math.floor(new Date().getTime() / 1000).toString());
            }
        });
    }

    private getAirlinesCode(): void {
        this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/airlines").subscribe((data: any) => {
            if (data) {
                this._airlinesCode = data;
                localStorage.setItem("airlinesCode", JSON.stringify(data));
                localStorage.setItem("airlinesCodeTime", Math.floor(new Date().getTime() / 1000).toString());
            }
        });
    }

    private getAirportsCode(): void {
        this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/airports").subscribe((data: any) => {
            if (data) {
                this._airportsCode = data;
                localStorage.setItem("airportsCode", JSON.stringify(data));
                localStorage.setItem("airportsCodeTime", Math.floor(new Date().getTime() / 1000).toString());
            }
        });
    }

    private getCountriesCode(): void {
        this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/countries").subscribe((data: any) => {
            if (data) {
                this._countriesCode = data;
                localStorage.setItem("countriesCode", JSON.stringify(data));
                localStorage.setItem("countriesCodeTime", Math.floor(new Date().getTime() / 1000).toString());
            }
        });
    }

    public start(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): Subject<any> {
        // this.status.next(false);
        return this.getCodes(aircraft);
    }

    public getCodes(newAircraft: Aircraft | CivilAircraft | MilitaryAircraft): Subject<any> {
        if (!this._aircraftCode) {
            setTimeout(() => {
                this.getCodesFunction(newAircraft);
            }, 1000);
        } else {
            setTimeout(() => {
                this.getCodesFunction(newAircraft);
            }, 1);
            // this.getCodesFunction(newAircraft);
        }
        return this.subjectCode;
    }

    public countryByID(id: number): any | object {
        if (id && (id - 1) >= 0) {
            // @ts-ignore
            for (const key: any of this._countriesCode.data.data) {
                if (key.id === id) {
                    return key;
                }
            }
        }

    }

    private getCodesFunction(newAircraft: Aircraft | CivilAircraft | MilitaryAircraft): any {
        let result: any = [];
        if (this._aircraftCode) {
            result = this._aircraftCode.data.filter((aircraft: any) => {
                if (aircraft.icao.indexOf(newAircraft.name) >= 0) {
                    return aircraft;
                }
            });
            if (result.length === 0) {
                result = this._aircraftCode.data.filter((aircraft: any) => {
                    if (aircraft.name.indexOf(newAircraft.name) >= 0) {
                        return aircraft;
                    }
                });
                if (result.length === 0) {
                    result = this._aircraftCode.data.filter((aircraft: any) => {
                        let name: string = newAircraft.name;
                        name = name.replace("XLR", "neo");
                        name = name.replace("ER", "");
                        name = name.replace("LR", "");
                        name = name.replace("600HGW", "600");
                        name = name.replace("8i", "8");
                        name = name.replace("300E", "300");
                        name = name.replace("900ULR", "900");
                        name = name.replace("A220-300", "CS300");
                        name = name.replace("A220-100", "CS100");
                        name = name.replace("CRJ701", "CRJ700");
                        name = name.replace("CRJ705", "CRJ700");
                        name = name.replace("CRJ", "Canadair Regional Jet ");
                        if (aircraft.name.indexOf(name) >= 0) {
                            return aircraft;
                        }
                    });
                }
            }

            if (result.length === 0) {
                result = this._aircraftCode.data.filter((aircraft: any) => {
                    let name: string = newAircraft.name;
                    name = name.split("-")[0];
                    if (aircraft.name.indexOf(name) >= 0) {
                        return aircraft;
                    }
                });
            }
            if (result.length !== 0) {
                this._debouncer.next({
                    result: result[0],
                    aircraft: newAircraft
                });
            } else {

            }
        } else {
            // this.informationNotificationService.createToast("error", "Failed to Load Data");
            // window.location.reload();
            // this.firstStartLoadData();
        }
        return result;
    }

    public putAircraft(icaoCode: any, result: any = null): any {
        // const url: string = "https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=72.83,-85.66,-428.52,428.52&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1&enc=kn0MBc2iBblCHHLR_MwjwVi8PVET_oeSRIoO0Rwafwo&type=" + icaoCode;
        const url: string = "https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=83.63,-83.31,-409.07,409.07&faa=1&satellite=1&mlat=1&flarm=1&adsb=1&gnd=1&air=1&vehicles=1&estimated=1&maxage=14400&gliders=1&stats=1&enc=nTDGEqBdOTCyc_1ABa1WDgWLZ2LzkMGdMi2IuslZaoc&type=" + icaoCode;
        this._statusSubject.subscribe(() => {
            this.http.get(url).subscribe((data: any) => {
                this.aircraftListData[icaoCode]["data"] = data;
                this.aircraftListData[icaoCode]["dataSource"] = new MatTableDataSource(this.fromFR24toReadable(data, result));
                this.aircraftListData[icaoCode]["details"] = {};

                if (this.aircraftListData[icaoCode]["dataSource"].data.length !== 0) {

                    this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/detail/" + this.aircraftListData[icaoCode]["dataSource"].data[0].id + "/trail/false").subscribe((moredata: any) => {
                        if (moredata.data.aircraft && moredata.data.aircraft.images) {
                            if (moredata.data.aircraft.images.sideview) {
                                this.aircraftListData[icaoCode]["picture"] = moredata.data.aircraft.images.sideview;
                            } else if (moredata.data.aircraft.images.large.length > 0) {
                                const random: number = this.randomInteger(0, moredata.data.aircraft.images.large.length - 1);
                                if (moredata.data.aircraft.images.large[random]) {
                                    this.aircraftListData[icaoCode]["picture"] = moredata.data.aircraft.images.large[random].src;
                                } else {
                                    this.aircraftListData[icaoCode]["picture"] = moredata.data.aircraft.images.large[0].src;
                                }
                            }
                        }
                        // Los...
                        this.subjectCode.next(this.aircraftListData[icaoCode].result);
                    });
                } else {
                    this.subjectCode.next(this.aircraftListData[icaoCode].result);
                }


            });
        });
    }

    public fromFR24toReadable(data: any, value: any = null): any {
        const result: Array<object> = [];
        Object.keys(data).forEach((key) => {

            if (data[key][8] !== undefined) {
                let status: string = "-";
                if (data[key][4] < 10) {
                    status = this.dictionary.Taxing_Standby;
                } else if (data[key][4] <= 10000) {
                    status = this.dictionary.Start_Landing;
                } else if (data[key][4] >= 10000) {
                    status = this.dictionary.fly;
                }

                const aircraft: IflightBasicData = new FlightBasicData({
                    "0": data[key][0], //   0  = ICAO 24-bit Address
                    "1": data[key][1], //   1  = Latitude
                    "2": data[key][2], //   2  = Longitude
                    "3": data[key][3], //   3  = Track
                    "4": data[key][4], //   4  = Calibrated altitude
                    "5": data[key][5], //   5  = Ground speed
                    "6": data[key][6], //   6  = Squawk
                    "7": data[key][7], //   7  =
                    "8": data[key][8], //   8  = Aircraft type (ICAO Code)
                    "9": data[key][9], //   9  = Registration
                    "10": data[key][10], // 10 =
                    "11": data[key][11], // 11 = Abflug Flughafen
                    "12": data[key][12], // 12 = Ziel Flughafen
                    "13": data[key][13], // 13 = Flugnummer
                    "14": data[key][14], // 14 =
                    "15": data[key][15], // 15 = Vertical speed
                    "16": data[key][16], // 16 = Flug Route
                    "17": data[key][17], // 17 =
                    "18": data[key][18], // 18 = Airline Kürzel / ICAO
                    id: key,
                    result: value.result,
                    status: status,
                    airline: this.getAirlinesByICAO(data[key][18], "name"),
                    from: this.getAirportsByIATA(data[key][11], "city+name"),
                    to: this.getAirportsByIATA(data[key][12], "city+name")
                });
                result.push(aircraft);

            }
        });
        return result;
    }

    public getMoreDetails(id: number): Observable<any> {
        return this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/detail/" + id);
    }

    public getAirlinesByICAO(icao: string, key?: string): any {
        let _return: any;
        if (this._airportsCode) {
            let result: Array<any>;
            result = this._airlinesCode.data.filter((airline: any) => {
                if (airline.icao.indexOf(icao) >= 0) {
                    return airline;
                }
            });
            if (result.length === 0) {
                result = this._airlinesCode.data.filter((airline: any) => {
                    let _icao: string = icao;
                    _icao = _icao.replace("SZN", "SNG");
                    if (airline.icao.indexOf(_icao) >= 0) {
                        return airline;
                    }
                });
            }


            if (result.length > 0) {
                _return = result[0];
                if (key && key !== "" && key !== null) {
                    _return = result[0][key];
                }
            } else {
                _return = icao;
            }
        } else {
            _return = icao;
        }
        return _return;
    }

    public getAirportsByIATA(iata: string, key?: string): any {
        let _return: any;
        if (this._airportsCode) {
            let result: Array<any>;
            result = this._airportsCode.data.filter((airport: any) => {
                if (airport.iata.indexOf(iata) >= 0) {
                    return airport;
                }
            });
            if (result.length > 0) {
                _return = result[0];
                if (key === "city+name") {
                    _return = result[0]["city"] + " - " + result[0]["name"];
                } else if (key && key !== "" && key !== null) {
                    _return = result[0][key];
                }
            } else {
                _return = iata;
            }
        } else {
            _return = iata;
        }
        return _return;
    }

    public randomInteger(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public destroy(): void {
        // this.subjectCode.unsubscribe();
        // this._status.unsubscribe();
        // this._aircraftCodeSubject.unsubscribe();
        // this._debouncer.unsubscribe();
    }

    get status(): boolean {
        return this._status;
    }

    get statusSubject(): BehaviorSubject<boolean> {
        return this._statusSubject;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }
}
