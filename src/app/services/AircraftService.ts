import {Inject, Injectable} from "@angular/core";
import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {AircraftFactory} from "../factories/AircraftFactory";
import {AircraftRepository} from "../repository/AircraftRepository";
import {BehaviorSubject} from "rxjs";
import {HTTPService} from "./http.service";
import {InformationNotificationService} from "./information-notification.service";

@Injectable({
    providedIn: "root"
})
export class AircraftService {


    private _allAircrafts: BehaviorSubject<AircraftRepository>;
    private _civilAircrafts: BehaviorSubject<AircraftRepository>;
    private _militaryAircrafts: BehaviorSubject<AircraftRepository>;


    constructor(@Inject(HTTPService) private httpService: HTTPService, @Inject(InformationNotificationService) private informationNotification: InformationNotificationService) {
        this._allAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this._civilAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this._militaryAircrafts = new BehaviorSubject<AircraftRepository>(new AircraftRepository());
        this.start();
    }

    public reload(): void {
        this._allAircrafts.getValue().hardClear();
        this._civilAircrafts.getValue().hardClear();
        this._militaryAircrafts.getValue().hardClear();
        this.start();
    }

    public start(): void {
        this.httpService.getAllAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createAllAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._allAircrafts.getValue().add(aircraft);
                }
                this._allAircrafts.next(this._allAircrafts.getValue());
            } else {
                this.informationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            this.informationNotification.createToast("error", "No Connection!", 10000);
        });
        this.httpService.getCivilAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createCivilAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._civilAircrafts.getValue().add(aircraft);
                }
                this._civilAircrafts.next(this._civilAircrafts.getValue());
            } else {
                this.informationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            this.informationNotification.createToast("error", "No Connection!", 10000);
        });
        this.httpService.getMilitaryAircraft().subscribe(data => {
            if (data.body) {
                const aircrafts: Array<Aircraft> = AircraftFactory.createMilitaryAircrafts(data.body);
                for (const aircraft of aircrafts) {
                    this._militaryAircrafts.getValue().add(aircraft);
                }
                this._militaryAircrafts.next(this._militaryAircrafts.getValue());
            } else {
                this.informationNotification.createToast("error", "No Data in Request!", 10000);
            }
        }, error => {
            this.informationNotification.createToast("error", "No Connection!", 10000);
        });
    }


    public allAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._allAircrafts.getValue().getAircraftByID(id);
        this.informationNotification.askForDelete(aircraft).subscribe(data => {
            if (data === true) {
                this.httpService.deleteAircraft(aircraft).subscribe(
                    (success) => {
                        const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                        if (status === true) {
                            if (this._allAircrafts.getValue().remove(aircraft) === false) {
                                this.informationNotification.DeletedFailed(aircraft);
                            }
                            this.informationNotification.Delete(aircraft);
                            if (aircraft instanceof CivilAircraft) {
                                if (this._civilAircrafts.getValue().remove(this._civilAircrafts.getValue().getAircraftByID(id)) === false) {
                                    this.informationNotification.DeletedFailed(aircraft);
                                }
                            } else if (aircraft instanceof MilitaryAircraft) {
                                if (this._militaryAircrafts.getValue().remove(this._militaryAircrafts.getValue().getAircraftByID(id)) === false) {
                                    this.informationNotification.DeletedFailed(aircraft);
                                }
                            }
                            this._allAircrafts.next(this._allAircrafts.getValue());
                            this._civilAircrafts.next(this._civilAircrafts.getValue());
                            this._militaryAircrafts.next(this._militaryAircrafts.getValue());
                        } else {
                            this.informationNotification.deleteFailed(aircraft);
                        }
                    },
                    error => console.error(error)
                );
            }
        });


    }

    public civilAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._civilAircrafts.getValue().getAircraftByID(id);
        this.informationNotification.askForDelete(aircraft).subscribe(data => {
            if (data === true) {
                this.httpService.deleteAircraft(aircraft).subscribe(
                    (success) => {
                        const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                        if (status === true) {

                            if (this._civilAircrafts.getValue().remove(aircraft) === false) {
                                this.informationNotification.DeletedFailed(aircraft);
                            }
                            if (this._allAircrafts.getValue().remove(this._allAircrafts.getValue().getAircraftByID(id)) === false) {
                                this.informationNotification.DeletedFailed(aircraft);
                            }
                            this.informationNotification.Delete(aircraft);
                            this._allAircrafts.next(this._allAircrafts.getValue());
                            this._civilAircrafts.next(this._civilAircrafts.getValue());
                        } else {
                            this.informationNotification.deleteFailed(aircraft);
                        }
                    },
                    error => console.error(error)
                );
            }
        });

    }

    public militaryAircraftDelete(id: number): void {
        const aircraft: Aircraft = this._militaryAircrafts.getValue().getAircraftByID(id);
        this.informationNotification.askForDelete(aircraft).subscribe(data => {
            if (data === true) {
                this.httpService.deleteAircraft(aircraft).subscribe(
                    (success) => {
                        const status: boolean = this.httpService.checkIfNoErrorDelete(success);
                        if (status === true) {
                            this.informationNotification.Delete(aircraft);
                            if (this._militaryAircrafts.getValue().remove(aircraft) === false) {
                                this.informationNotification.DeletedFailed(aircraft);
                            }
                            if (this._allAircrafts.getValue().remove(this._allAircrafts.getValue().getAircraftByID(id)) === false) {
                                this.informationNotification.DeletedFailed(aircraft);
                            }

                            this._allAircrafts.next(this._allAircrafts.getValue());
                            this._militaryAircrafts.next(this._militaryAircrafts.getValue());
                        } else {
                            this.informationNotification.deleteFailed(aircraft);
                        }
                    },
                    error => console.error(error)
                );
            }
        });
    }


    public civilAircraftEdit(oldAircraft: Aircraft, newAircraft: IcivilAircraftData): void {
        this.informationNotification.Change(newAircraft);

        const check1: any = this._allAircrafts.getValue().changeCivil(oldAircraft as CivilAircraft, newAircraft);
        if (check1 === false) {
            this.informationNotification.ChangedFailed(newAircraft);
        }
        const check2: any = this._civilAircrafts.getValue().changeCivil(oldAircraft as CivilAircraft, newAircraft);
        if (check2 === false) {
            this.informationNotification.ChangedFailed(newAircraft);
        }

        this._allAircrafts.next(this._allAircrafts.getValue());
        this._civilAircrafts.next(this._civilAircrafts.getValue());
    }

    public militaryAircraftEdit(oldAircraft: Aircraft, newAircraft: ImilitaryAircraftData): void {
        this.informationNotification.Change(newAircraft);

        const check1: any = this._allAircrafts.getValue().changeMilitary(oldAircraft as MilitaryAircraft, newAircraft);
        if (check1 === false) {
            this.informationNotification.ChangedFailed(newAircraft);
        }
        const check2: any = this._militaryAircrafts.getValue().changeMilitary(oldAircraft as MilitaryAircraft, newAircraft);
        if (check2 === false) {
            this.informationNotification.ChangedFailed(newAircraft);
        }
        this._allAircrafts.next(this._allAircrafts.getValue());
        this._militaryAircrafts.next(this._militaryAircrafts.getValue());
    }

    get civilAircrafts(): Array<CivilAircraft> {
        return this._civilAircrafts.getValue().aircrafts;
    }

    get militaryAircrafts(): Array<MilitaryAircraft> {
        return this._militaryAircrafts.getValue().aircrafts;
    }

    get militaryAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._militaryAircrafts;
    }

    get civilAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._civilAircrafts;
    }

    get allAircraftsBehaviorSubject(): BehaviorSubject<AircraftRepository> {
        return this._allAircrafts;
    }

    set militaryAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._militaryAircrafts = value;
    }

    set civilAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._civilAircrafts = value;
    }

    set allAircraftsBehaviorSubject(value: BehaviorSubject<AircraftRepository>) {
        this._allAircrafts = value;
    }

    public allAircraftsFilter(search: string): Array<Aircraft> {
        return this._allAircrafts.getValue().filter(search);
    }

    public civilAircraftsFilter(search: string): Array<Aircraft> {
        return this._civilAircrafts.getValue().filter(search);
    }

    public militaryAircraftsFilter(search: string): Array<Aircraft> {
        return this._militaryAircrafts.getValue().filter(search);
    }

    public allAircraftsSort(key: string): any {
        return this._allAircrafts.getValue().sortByKey(key);
    }

    public civilAircraftsSort(key: string): any {
        return this._civilAircrafts.getValue().sortByKey(key);
    }

    public militaryAircraftsSort(key: string): any {
        return this._militaryAircrafts.getValue().sortByKey(key);
    }
}
