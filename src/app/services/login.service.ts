import {Inject, Injectable} from "@angular/core";
import {HTTPService} from "./http.service";
// @ts-ignore
import * as CryptoJS from "crypto-js";
import {Observable, Subject, Subscription} from "rxjs";
import {ActivatedRoute, Event, Router, RouterEvent} from "@angular/router";
import {log} from "util";
import {InformationNotificationService} from "./information-notification.service";
import {Dictionary} from "../keys/Dictionary";
import {environment} from "../../environments/environment";
import {
    EMAIL_LOGIN_DB_KEY, EXPIRYTIME_TOKEN_DB_KEY,
    FIRSTNAME_LOGIN_DB_KEY,
    ID_LOGIN_DB_KEY, ROLES_LOGIN_DB_KEY,
    SURNAME_LOGIN_DB_KEY,
    TOKEN_TOKEN_DB_KEY,
    USERNAME_LOGIN_DB_KEY
} from "../keys/LoginKeys";

@Injectable({
    providedIn: "root"
})
export class LoginService {
    // https://stackoverflow.com/questions/41972330/base-64-encode-and-decode-a-string-in-angular-2
    private _email: string;
    private _password: string;
    private _id: number;
    private _firstname: string;

    private _surname: string;
    private _username: string;
    private _login_status: Subject<boolean> = new Subject<boolean>();
    private _token: string;
    private _comeFrom: string = "/all";
    private _expirytime: number;
    private _statusLogin: boolean;
    private _dictionary: Dictionary = new Dictionary();
    private now: any = Math.floor(new Date().getTime() / 1000);
    private _userActive: number = Number(this.now.toString());
    private login_time: number = 0;
    private add_login_time: number = 0;
    private oldUserActive: number = 0;
    private add_loginTime: number = 0;
    private _currentUserRank: Irank = {
        admin: false,
        user: false
    };
    private _currentUserRankSubject: Subject<Irank> = new Subject<Irank>();
    constructor(@Inject(HTTPService) private http: HTTPService,
                @Inject(Router) public router: Router,
                @Inject(ActivatedRoute) private route: ActivatedRoute,
                @Inject(InformationNotificationService) private informationNotificationService: InformationNotificationService,
    ) {
        this._token = localStorage.getItem("login_token");
        this._expirytime = Number(localStorage.getItem("login_expirytime"));
        this.http.sendGetLoginTime().subscribe((getTimeData) => {
            if (getTimeData && getTimeData.body && getTimeData.body.data) {
                this.login_time = getTimeData.body.data["login_time"];
                this.add_login_time = getTimeData.body.data["add_login_time"];
                this.intervalStart();
            }
            this.onesec();
            this.sec30();

        });

        setInterval(() => {
            this.now = Math.floor(new Date().getTime() / 1000);
            this.onesec();
        }, 1000);


        this.startAction();
        this.router.events.subscribe((data: any) => {
            if (data["shouldActivate"] === true) {
                this.renew();
            }

        });
        this.currentUserRankSubject.subscribe((data: Irank) => {
            this.currentUserRank = data;
        });
    }

    private intervalStart(): void {
        setInterval(() => {
            this.sec30();
        }, (this.add_login_time * 0.90) * 1000);
    }

    public renew(): void {
        if (this.add_loginTime < (this.add_login_time * 5)) {
            this.add_loginTime = this.add_loginTime + this.add_login_time;
        }
    }

    private onesec(): void {
        // this.resetTime = this.now - this._expirytime;
    }

    private sec30(): void {
        this.expirytimeCountUp();
    }

    public expirytimeCountUp(): void {
        if (this.login_time !== 0 && this.add_login_time !== 0) {
            this.http.sendLoginRenewToken(this._token, this.add_loginTime).subscribe((renewData) => {
                if (renewData) {
                    if (!renewData.body.error && renewData.body.data) {
                        this._token = renewData.body.data["token"];
                        this._expirytime = Number(renewData.body.data["expirytime"]);
                        this.add_loginTime = 0;
                    }
                }
            });
        }
    }

    private startAction(): void {
        this._login_status.subscribe((data) => {
            this._statusLogin = data;
            if (data === true) {
                this.goToPage();
            }
        });
        this._login_status.next(false);

        this.autoLogin().subscribe((data) => {
            this._login_status.next(data);
            if (data === false) {
                const url: any = this.router.url;
                if ((url.indexOf("login") <= 0) && (url.indexOf("register") <= 0)) {
                    this.comeFrom = url;
                    this.router.navigate(["/login"]);
                }
            }
        });
        this.router.events.subscribe((data: any) => {
            if (data.shouldActivate === true) {
                if ((data.url.indexOf("login") <= 0) && (data.url.indexOf("register") <= 0)) {
                    this.comeFrom = data.url;
                }
            }
        });
    }

    private goToPage(): void {
        this.router.navigate([this.comeFrom]);
    }

    public autoLogin(): Subject<boolean> {
        let result: Subject<boolean> = new Subject<boolean>();
        result = this.checkLogin(result);
        setInterval(() => {
            result = this.checkLogin(result);
        }, 60000);

        return result;
    }

    private checkLogin(result: Subject<boolean>): Subject<boolean> {
        this.http.sendLoginViaToken(this._token).subscribe((data: any) => {
            if (data && data.body && data.body.data) {
                const basis: any = data.body.data;
                if (basis.access === false) {
                    result.next(false);
                } else {
                    if (this._statusLogin !== true) {
                        this._email = atob(basis["userData"][EMAIL_LOGIN_DB_KEY]);
                        this._firstname = atob(basis["userData"][FIRSTNAME_LOGIN_DB_KEY]);
                        this._surname = atob(basis["userData"][SURNAME_LOGIN_DB_KEY]);
                        this._username = atob(basis["userData"][USERNAME_LOGIN_DB_KEY]);
                        this._id = basis["userData"][ID_LOGIN_DB_KEY];
                        this._token = basis["tokenData"][TOKEN_TOKEN_DB_KEY];
                        this._expirytime = basis["tokenData"][EXPIRYTIME_TOKEN_DB_KEY];
                        this.currentUserRank.admin = basis["userData"][ROLES_LOGIN_DB_KEY].indexOf("admin") > -1;
                        this.currentUserRank.user = basis["userData"][ROLES_LOGIN_DB_KEY].indexOf("user") > -1;
                        this.currentUserRankSubject.next(this.currentUserRank);
                        result.next(true);
                    }

                }
            }


        });
        return result;
    }

    public login(email: string, password: string): Subject<boolean> {
        this._email = email;
        this._password = password;
        if (email && password) {
            const newEmail: string = btoa(email);
            const newPassword: string = this.hash(password);
            this.http.sendLogin(newEmail, newPassword).subscribe((data) => {
                if (!data.body.error && data.body.data) {
                    const basis: any = data.body.data;
                    this._email = atob(basis[EMAIL_LOGIN_DB_KEY]);
                    this._firstname = atob(basis[FIRSTNAME_LOGIN_DB_KEY]);
                    this._surname = atob(basis[SURNAME_LOGIN_DB_KEY]);
                    this._username = atob(basis[USERNAME_LOGIN_DB_KEY]);
                    this._id = basis[ID_LOGIN_DB_KEY];
                    this._token = basis[TOKEN_TOKEN_DB_KEY];
                    this._expirytime = basis[EXPIRYTIME_TOKEN_DB_KEY];
                    this.currentUserRank.admin = basis[ROLES_LOGIN_DB_KEY].indexOf("admin") > -1;
                    this.currentUserRank.user = basis[ROLES_LOGIN_DB_KEY].indexOf("user") > -1;
                    // this._currentUserRank;
                    this.currentUserRankSubject.next(this.currentUserRank);
                    localStorage.setItem("login_token", this._token);
                    localStorage.setItem("login_expirytime", this._expirytime.toString());
                    this._login_status.next(true);
                } else {
                    this._login_status.next(false);
                }
            });
        } else {
            this._login_status.next(false);
        }
        return this._login_status;
    }

    public register(email: string, username: string, firstname: string, surname: string, password: string): Observable<any> {
        if (email && password) {
            const newEmail: string = btoa(email);
            const newUsername: string = btoa(username);
            const newFirstname: string = btoa(firstname);
            const newSurname: string = btoa(surname);
            const newPassword: string = this.hash(password);
            return this.http.sendRegister(newEmail, newUsername, newFirstname, newSurname, newPassword);
        }
    }

    private hash(password: string): string {
        return CryptoJS.SHA3("aircraft" + password).toString();
    }

    get email(): string {
        return this._email;
    }

    get password(): string {
        return this._password;
    }

    get username(): string {
        return this._username;
    }

    get surname(): string {
        return this._surname;
    }

    get firstname(): string {
        return this._firstname;
    }

    get id(): number {
        return this._id;
    }

    // https://stackoverflow.com/questions/41038425/how-to-use-cryptojs-with-angular-2-and-typescript-in-webpack-build-environment

    get statusLogin(): boolean {
        return this._statusLogin;
    }

    get comeFrom(): string {
        return this._comeFrom;
    }

    set comeFrom(value: string) {
        if ((value.indexOf("login") <= 0) && (value.indexOf("register") <= 0)) {
            this._comeFrom = value;
        }
    }

    get userActive(): number {
        return this._userActive;
    }


    set userActive(value: number) {
        this._userActive = value;
    }

    get currentUserRank(): Irank {
        return this._currentUserRank;
    }

    set currentUserRank(value: Irank) {
        this._currentUserRank = value;
    }

    get currentUserRankSubject(): Subject<Irank> {
        return this._currentUserRankSubject;
    }

    set currentUserRankSubject(value: Subject<Irank>) {
        this._currentUserRankSubject = value;
    }
}
