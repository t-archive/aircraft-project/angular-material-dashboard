import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs";

@Injectable({
    providedIn: "root"
})
export class StoreService {
    private _filterTextBehaviorSubject: BehaviorSubject<string> = new BehaviorSubject<string>("");
    get filterTextBehaviorSubject(): BehaviorSubject<string> {
        return this._filterTextBehaviorSubject;
    }
}
