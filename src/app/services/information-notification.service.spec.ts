import { TestBed } from "@angular/core/testing";

import { InformationNotificationService } from "./information-notification.service";

describe("InformationNotificationService", () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it("should be created", () => {
    const service: InformationNotificationService = TestBed.get(InformationNotificationService);
    expect(service).toBeTruthy();
  });
});
