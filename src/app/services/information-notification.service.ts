import {Inject, Injectable} from "@angular/core";
import {Observable, Subject, Subscriber} from "rxjs";
import {SweetAlertIconRequirements} from "../keys/Requirements";
import Swal, {SweetAlertArrayOptions} from "sweetalert2";
import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {Dictionary} from "../keys/Dictionary";
import {ItoastArrayParameter, ToastArrayParameter} from "../interfaces/ItoastArrayParameter";
import {ToastParameter} from "../interfaces/ItoastParameter";



@Injectable({
    providedIn: "root"
})
export class InformationNotificationService {


    private _dictionary: Dictionary = new Dictionary();
    private _queue: Array<ItoastArrayParameter> = [];
    private _orderNumber: number = 1;
    private _isActiveToast: number | boolean = false;


    constructor() {
    }

    public createToast(status: SweetAlertIconRequirements, text: string, time: number = 1500): void {
        const toastArrayParameter: ItoastArrayParameter = new ToastArrayParameter({
            orderNumber: this._orderNumber,
            data: new ToastParameter({
                status: status,
                text: text,
                time: time
            }),
            status: "wait"
        });
        this.listTask(toastArrayParameter);
        this._orderNumber++;
    }

    public listTask(data: ItoastArrayParameter): void {
        this._queue.push(data);
        this.makeToast();
    }

    private getFirstItem(): ItoastArrayParameter {
        return this._queue.find(e => !!e && e.status !== "close");
    }

    public makeToast(): void {
        const newData: ItoastArrayParameter = this.getFirstItem();
        this.createToastFunction(newData);
    }

    public createToastFunction(data: ItoastArrayParameter): void {
        if (data) {
            if (this._isActiveToast !== data.orderNumber) {
                const toast: any = Swal.fire({
                    toast: true,
                    position: "top-end",
                    icon: data.data.status,
                    title: data.data.text,
                    showConfirmButton: false,
                    timer: data.data.time,
                    timerProgressBar: false,
                    allowEscapeKey: true,
                    customClass: {container: "swal-container"},
                    onAfterClose: () => {
                        this._isActiveToast = false;
                        data.status = "close";
                        this.makeToast();
                    },
                    preConfirm: () => {
                    },
                    onOpen: () => {
                        this._isActiveToast = data.orderNumber;
                        data.status = "open";
                    }
                });
            }
        }


    }

    public comeFromPopup(): void {
        this._isActiveToast = false;
        this.makeToast();
    }

    public askForDelete(aircraft: CivilAircraft | MilitaryAircraft | Aircraft): Observable<boolean> {
        return new Observable(observer => {
            let return_result: boolean;
            Swal.fire({
                title: this._dictionary.delete + ": " + aircraft.name + "",
                text: this._dictionary.doYouWant + " " + aircraft.name + " " + this._dictionary.reallyDelete,
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: this._dictionary.yes,
                cancelButtonText: this._dictionary.no,
                customClass: {container: "swal-container"}
            }).then((result) => {
                if (result.value) {
                    return_result = true;
                    this.createToast("success", aircraft.name + " " + this._dictionary.wasDeleted, 3000);
                } else {
                    return_result = false;
                    this.createToast("warning", aircraft.name + " " + this._dictionary.wasNotDeleted, 3000);
                }
                this.comeFromPopup();
                observer.next(return_result);
                observer.complete();
            });
        });
    }


    public Delete(aircraft: Aircraft): void {
        this.createToast("warning", aircraft.name + " " + this._dictionary.wasDeleted);
    }

    public DeletedFailed(aircraft: Aircraft): void {
        this.createToast("error", aircraft.name + " " + this._dictionary.wasNotDeletedDeletionError);
    }

    public Change(aircraft: IaircraftData): void {
        this.createToast("success", aircraft.name + " " + this._dictionary.wasEdited);
    }

    public ChangedFailed(aircraft: IaircraftData): void {
        this.createToast("error", aircraft.name + " " + this._dictionary.wasNotEditedErrorWhileEditing);
    }

    public createFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        this.createToast("error", aircraft.name + " " + this._dictionary.createFailed);
    }

    public editFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        this.createToast("error", aircraft.name + " " + this._dictionary.editFailed, 3000);
    }

    public deleteFailed(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): void {
        this.createToast("error", aircraft.name + " " + this._dictionary.deleteFailed, 3000);
    }


}
