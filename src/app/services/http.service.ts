import {Inject, Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";
import {share} from "rxjs/operators";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {AircraftFactory} from "../factories/AircraftFactory";
import {Aircraft} from "../aircrafts/Aircraft";


@Injectable({
    providedIn: "root"
})
export class HTTPService {

    constructor(@Inject(HttpClient) private http: HttpClient) {
    }

    getAllAircraft(): Observable<any> {
        return this.http.get(environment.allAircraftURL, {
            observe: "response"
        });
    }

    getCivilAircraft(): Observable<any> {
        return this.http.get(environment.civilAircraftURL, {
            observe: "response"
        });
    }

    getMilitaryAircraft(): Observable<any> {
        return this.http.get(environment.militaryAircraftURL, {
            observe: "response"
        });
    }

    postCivilAircraft(aircraft: CivilAircraft): Observable<any> {
        const options: any = {observe: "response", headers: "Content-Type: application/json"};
        return this.http.post(environment.civilAircraftURL, JSON.stringify(AircraftFactory.jsonToReadySendPostJSON(aircraft)), options);
    }

    postMilitaryAircraft(aircraft: MilitaryAircraft): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.militaryAircraftURL, JSON.stringify(AircraftFactory.jsonToReadySendPostJSON(aircraft)), options);
    }
    putCivilAircraft(aircraft: CivilAircraft): Observable<any> {
        const options: any = {observe: "response", headers: "Content-Type: application/json"};
        return this.http.put(environment.civilAircraftURL, JSON.stringify(AircraftFactory.jsonToReadySendPostJSON(aircraft)), options);
    }

    putMilitaryAircraft(aircraft: MilitaryAircraft): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.put(environment.militaryAircraftURL, JSON.stringify(AircraftFactory.jsonToReadySendPostJSON(aircraft)), options);
    }
    deleteAircraft(aircraft: Aircraft | CivilAircraft | MilitaryAircraft): Observable<any> {
        return this.http.delete(environment.deleteAircraftURL + aircraft.id);
    }
    sendLoginRenewToken(token: string, time: number): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.loginURL, JSON.stringify({
            renewToken: {
                token: token,
                time: time
            }
        }), options);
    }
    sendGetLoginTime(): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.loginURL, JSON.stringify({
            getTimeData: {}
        }), options);
    }
    sendLoginViaToken(token: string): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.loginURL, JSON.stringify({
            autoLogin: {
                token: token
            }
        }), options);
    }
    sendLogin(email: string, password: string): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.loginURL, JSON.stringify({
            login: {
                email: email,
                password: password
            }
        }), options);
    }
    sendRegister(email: string, username: string, firstname: string, surname: string, password: string): Observable<any> {
        const options: any = {observe: "response", headers: {"Content-Type": "application/json"}};
        return this.http.post(environment.registerURL, JSON.stringify({
            register: {
                email: email,
                username: username,
                firstname: firstname,
                surname: surname,
                password: password
            }
        }), options);
    }
    checkIfNoError(x: any): boolean {
        if (typeof x.body.error !== "undefined") {
            return x.body.error.code === 200;
        } else {
            return true;
        }
    }
    checkIfNoErrorDelete(x: any): boolean {
        if (typeof x.data !== "undefined") {
            return x.data.delete === true;
        } else {
            return true;
        }
    }
}
