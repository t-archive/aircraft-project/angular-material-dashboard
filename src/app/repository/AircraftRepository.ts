import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {AIRREFUELING_KEY, FLARE_KEY, GATELICENSE_KEY} from "../keys/AircraftKeys";
import {HTTPService} from "../services/http.service";
import {Inject} from "@angular/core";
import {InformationNotificationService} from "../services/information-notification.service";


export class AircraftRepository {

    private _version: string;
    private readonly _aircrafts: Array<Aircraft>;
    private readonly _viewAircrafts: Array<Aircraft>;
    private readonly _filterAircrafts: Array<Aircraft>;
    private _key: string;
    private _stat: number;

    constructor() {
        this._version = null;
        this._aircrafts = [];
        this._viewAircrafts = [];
        this._filterAircrafts = [];
        this._stat = 0;
    }

    add(aircraft: Aircraft): void {
        this._aircrafts.push(aircraft);
        this._viewAircrafts.push(aircraft);
        this._filterAircrafts.push(aircraft);
    }

    remove(aircraft: Aircraft): boolean {
        // InformationNotification.Delete(aircraft);
        const deletedAircraft: any = this.removeByInternAircraftsArray(this._viewAircrafts, aircraft);
        if (deletedAircraft === false) {
            // this.informationNotification.DeletedFailed(aircraft);
            return false;
        } else {
            this.removeByInternAircraftsArray(this._filterAircrafts, aircraft);
            return true;
        }

    }


    private removeByInternAircraftsArray(object: Array<object>, aircraft: Aircraft): any {
        const index: number = object.findIndex((otherAircraft) => {
            return otherAircraft === aircraft;
        });
        if (index !== -1) {
            return object.splice(index, 1);
        } else {
            return false;
        }
    }



    clear(): void {
        this._viewAircrafts.length = 0;
        this._aircrafts.length = 0;
    }
    hardClear(): void {
        this._viewAircrafts.length = 0;
        this._aircrafts.length = 0;
        this._filterAircrafts.length = 0;
    }
    clearArray(object: Array<Aircraft>): void {
        if (typeof object === undefined) {
            console.warn("AircraftRepository.clearByArrayObject: Not exists '" + object + "'");
        } else {
            object.length = 0;
        }
    }

    filter(search: any): Array<Aircraft> {
        const result: any = this._filterAircrafts.filter((aircraft) => {
            return aircraft.containsSearchValue(search.toLowerCase());
        });
        this.clearArray(this._viewAircrafts);
        for (const aircraft of result) {
            this._viewAircrafts.push(aircraft);
        }
        return result;
    }

    getAircraftByID(id: number): Aircraft {
        let result: any = null;
        for (const item of this._viewAircrafts) {
            if (item.id === id) {
                result = item;
                break;
            }
        }
        return result;
    }

    getAircraftByIDWithObject(id: number, object: Array<Aircraft>): Aircraft {
        let result: any = null;
        for (const item of object) {
            if (item.id === id) {
                result = item;
                break;
            }
        }
        return result;
    }

    get aircrafts(): Array<any> {
        return this._viewAircrafts;
    }

    get version(): string {
        return this._version;
    }

    set version(value: string) {
        this._version = value;
    }


    public sortByKey(key: any = null): any {
        if (this._key !== undefined) {
            if (key !== this._key && key !== null) {
                this._stat = 0;
            }
        }
        let result: any;
        if (key === null) {
            key = this._key;
        } else {
            this._key = key;
        }

        this.clearArray(this._viewAircrafts);
        if (this._stat === 0) {
            this.setAircraftsInObjects(this._viewAircrafts, this._filterAircrafts);
            result = this.sortAction(key, false);
        } else if (this._stat === 1) {
            this.setAircraftsInObjects(this._viewAircrafts, this._filterAircrafts);

            result = this.sortAction(key, true);
        } else if (this._stat === 2) {
            this._stat = -1;
            this.setAircraftsInObjects(this._viewAircrafts, this._filterAircrafts);
            result = this._viewAircrafts;
        }
        this._stat = this._stat + 1;
        return result;
    }

    sortAction(key: any, asc: any): any {

        return this._viewAircrafts.sort((a, b) => {
            let x: any = a[key];
            let y: any = b[key];

            if (y === undefined) {
                y = -1;
                if (key === GATELICENSE_KEY) {
                    return;
                }

            }
            if (x === undefined) {
                x = -1;

                if (key === AIRREFUELING_KEY) {
                    x = 2;

                }
                if (key === FLARE_KEY) {
                    x = 2;

                }
            }
            let result: any = ((x > y) ? -1 : ((x < y) ? 1 : 0));
            if (asc === false) {
                result = result * -1;
            }

            return result;

        });
    }

    setAircraftsInObjects(object: Array<object>, aircrafts: Array<object>): any {
        for (const aircraft of aircrafts) {
            object.push(aircraft);
        }
    }

    changeCivil(oldAircraft: CivilAircraft, newAircraft: IcivilAircraftData): any {

        const changedAircraft: any = this.changeCivilAircraft(this._viewAircrafts as Array<CivilAircraft>, oldAircraft, newAircraft);
        if (changedAircraft === false) {
            return false;
        } else {
            this.changeCivilAircraft(this._filterAircrafts as Array<CivilAircraft>, oldAircraft, newAircraft);
            this.changeCivilAircraft(this._aircrafts as Array<CivilAircraft>, oldAircraft, newAircraft);
            return true;
        }

    }

    changeMilitary(oldAircraft: MilitaryAircraft, newAircraft: ImilitaryAircraftData): any {

        const changedAircraft: any = this.changeMilitaryAircraft(this._viewAircrafts as Array<MilitaryAircraft>, oldAircraft, newAircraft);
        if (changedAircraft === false) {
            return false;
        } else {
            this.changeMilitaryAircraft(this._filterAircrafts as Array<MilitaryAircraft>, oldAircraft, newAircraft);
            this.changeMilitaryAircraft(this._aircrafts as Array<MilitaryAircraft>, oldAircraft, newAircraft);
            return true;
        }

    }
    private changeMilitaryAircraft(object: Array<MilitaryAircraft>, oldAircraft: MilitaryAircraft, newAircraft: ImilitaryAircraftData): any {
        if (object.length !== 0) {
            const oldAircraftObject: Aircraft = this.getAircraftByIDWithObject(oldAircraft.id, object);
            const index: number = object.findIndex((otherAircraft) => {
                return otherAircraft === oldAircraftObject;
            });
            if (index !== -1) {
                object[index].id = newAircraft.id;
                oldAircraft.id = newAircraft.id;
                object[index].name = newAircraft.name;
                oldAircraft.name = newAircraft.name;
                object[index].distance = newAircraft.distance;
                oldAircraft.distance = newAircraft.distance;
                object[index].seatCount = newAircraft.seatCount;
                oldAircraft.seatCount = newAircraft.seatCount;
                object[index].maxSeatCount = newAircraft.maxSeatCount;
                oldAircraft.maxSeatCount = newAircraft.maxSeatCount;
                object[index].length = newAircraft.length;
                oldAircraft.length = newAircraft.length;
                object[index].wingSpan = newAircraft.wingSpan;
                oldAircraft.wingSpan = newAircraft.wingSpan;
                object[index].height = newAircraft.height;
                oldAircraft.height = newAircraft.height;
                object[index].consumption = newAircraft.consumption;
                oldAircraft.consumption = newAircraft.consumption;
                object[index].flare = newAircraft.flare;
                oldAircraft.flare = newAircraft.flare;
                object[index].airRefueling = newAircraft.airRefueling;
                oldAircraft.airRefueling = newAircraft.airRefueling;

            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private changeCivilAircraft(object: Array<CivilAircraft>, oldAircraft: CivilAircraft, newAircraft: IcivilAircraftData): any {
        if (object.length !== 0) {
            const oldAircraftObject: Aircraft = this.getAircraftByIDWithObject(oldAircraft.id, object);
            const index: number = object.findIndex((otherAircraft) => {
                return otherAircraft === oldAircraftObject;
            });
            if (index !== -1) {
                object[index].id = newAircraft.id;
                oldAircraft.id = newAircraft.id;
                object[index].name = newAircraft.name;
                oldAircraft.name = newAircraft.name;
                object[index].distance = newAircraft.distance;
                oldAircraft.distance = newAircraft.distance;
                object[index].seatCount = newAircraft.seatCount;
                oldAircraft.seatCount = newAircraft.seatCount;
                object[index].maxSeatCount = newAircraft.maxSeatCount;
                oldAircraft.maxSeatCount = newAircraft.maxSeatCount;
                object[index].length = newAircraft.length;
                oldAircraft.length = newAircraft.length;
                object[index].wingSpan = newAircraft.wingSpan;
                oldAircraft.wingSpan = newAircraft.wingSpan;
                object[index].height = newAircraft.height;
                oldAircraft.height = newAircraft.height;
                object[index].consumption = newAircraft.consumption;
                oldAircraft.consumption = newAircraft.consumption;
                object[index].gateLicense = newAircraft.gateLicense;
                oldAircraft.gateLicense = newAircraft.gateLicense;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
