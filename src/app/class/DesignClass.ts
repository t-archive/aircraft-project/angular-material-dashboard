import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {AircraftKeys} from "../keys/AircraftKeys";

export class DesignClass {
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    constructor() {

    }
    public check(aircraft: Aircraft, key: string): any {
        let result: string = "";
        if (key === this._aircraftKeys.gateLicense) {
            if (aircraft instanceof CivilAircraft) {
                result = this.checkIcon(aircraft.gateLicense);
            }
        } else if (key === this._aircraftKeys.flare) {
            if (aircraft instanceof MilitaryAircraft) {
                result = this.checkIcon(aircraft.flare);
            }
        } else if (key === this._aircraftKeys.airRefueling) {
            if (aircraft instanceof MilitaryAircraft) {
                result = this.checkIcon(aircraft.airRefueling);
            }
        } else {
            result = "<i class=\"fas fa-circle has-text-link\"></i>";
        }

        return result;
    }

    private checkIcon(status: boolean): string {
        if (status === true) {
            return "<i class=\"fa fa-check text-icon-green\"></i>";
            // return "<mat-icon>check</mat-icon>";
        } else {
            return "<i class=\"fa fa-times text-icon-red\"></i>";
            // return "<mat-icon>clear</mat-icon>";
        }
    }
    public searchBorderColor(input: Array<object>, searchInput: string): string {
        let classContent: string = "";
        if (input.length === 0) {
            classContent = "is-danger";
        } else {
            if (searchInput !== "") {
                classContent = "is-success";
            } else {
                classContent = "";
            }
        }
        return classContent;
    }
    public getGreenToRed = (percent: number, opacity: number | string = "0.85"): string => {
        const r: number = percent < 50 ? 255 : Math.floor(255 - (percent * 2 - 100) * 255 / 100);
        const g: number = percent > 50 ? 255 : Math.floor((percent * 2) * 255 / 100);
        return "rgba(" + r + "," + g + ",0," + opacity + ")";
    }

}
