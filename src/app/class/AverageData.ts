import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";
import {IaverageDataMinMax} from "../interfaces/iaverage-data-min-max";
import {AircraftKeys} from "../keys/AircraftKeys";

export class AverageData {
    private _maxData: IaverageDataMinMax;
    private _minData: IaverageDataMinMax;
    private _aircrafts: Array<Aircraft | CivilAircraft | MilitaryAircraft>;
    private _distance: number = 0;
    private _seatCount: number = 0;
    private _maxSeatCount: number = 0;
    private _length: number = 0;
    private _wingSpan: number = 0;
    private _height: number = 0;
    private _consumption: number = 0;
    private _distancePercent: number = 0;
    private _seatCountPercent: number = 0;
    private _maxSeatCountPercent: number = 0;
    private _lengthPercent: number = 0;
    private _wingSpanPercent: number = 0;
    private _heightPercent: number = 0;
    private _consumptionPercent: number = 0;
    private _distancePercentReserve: number = 0;
    private _seatCountPercentReserve: number = 0;
    private _maxSeatCountPercentReserve: number = 0;
    private _lengthPercentReserve: number = 0;
    private _wingSpanPercentReserve: number = 0;
    private _heightPercentReserve: number = 0;
    private _consumptionPercentReserve: number = 0;
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    start(aircrafts: Array<Aircraft | CivilAircraft | MilitaryAircraft>): void {
        this.aircrafts = aircrafts;
        let distance: number = 0;
        let seatCount: number = 0;
        let maxSeatCount: number = 0;
        let length: number = 0;
        let wingSpan: number = 0;
        let height: number = 0;
        let consumption: number = 0;
        let count: number = 0;
        let distanceMin: number = Number.POSITIVE_INFINITY;
        let seatCountMin: number = Number.POSITIVE_INFINITY;
        let maxSeatCountMin: number = Number.POSITIVE_INFINITY;
        let lengthMin: number = Number.POSITIVE_INFINITY;
        let wingSpanMin: number = Number.POSITIVE_INFINITY;
        let heightMin: number = Number.POSITIVE_INFINITY;
        let consumptionMin: number = Number.POSITIVE_INFINITY;
        let distanceMax: number = 0;
        let seatCountMax: number = 0;
        let maxSeatCountMax: number = 0;
        let lengthMax: number = 0;
        let wingSpanMax: number = 0;
        let heightMax: number = 0;
        let consumptionMax: number = 0;

        // @ts-ignore
        for (const aircraft of this.aircrafts) {

            count = count + 1;
            distance = distance + aircraft.distance;
            seatCount = seatCount + aircraft.seatCount;
            maxSeatCount = maxSeatCount + aircraft.maxSeatCount;
            length = length + aircraft.length;
            wingSpan = wingSpan + aircraft.wingSpan;
            height = height + aircraft.height;
            consumption = consumption + aircraft.consumption;

            distanceMin = Math.min(distanceMin, aircraft.distance);
            seatCountMin = Math.min(seatCountMin, aircraft.seatCount);
            maxSeatCountMin = Math.min(maxSeatCountMin, aircraft.maxSeatCount);
            lengthMin = Math.min(lengthMin, aircraft.length);
            wingSpanMin = Math.min(wingSpanMin, aircraft.wingSpan);
            heightMin = Math.min(heightMin, aircraft.height);
            consumptionMin = Math.min(consumptionMin, aircraft.consumption);

            distanceMax = Math.max(distanceMax, aircraft.distance);
            seatCountMax = Math.max(seatCountMax, aircraft.seatCount);
            maxSeatCountMax = Math.max(maxSeatCountMax, aircraft.maxSeatCount);
            lengthMax = Math.max(lengthMax, aircraft.length);
            wingSpanMax = Math.max(wingSpanMax, aircraft.wingSpan);
            heightMax = Math.max(heightMax, aircraft.height);
            consumptionMax = Math.max(consumptionMax, aircraft.consumption);

            // @ts-ignore
            for (const otherAircraft of this.aircrafts) {
                if (aircraft !== otherAircraft) {
                    if (otherAircraft.distance > aircraft.distance) {
                        aircraft.distanceProperty.counterBetter++;
                    } else if (otherAircraft.distance === aircraft.distance) {
                        aircraft.distanceProperty.counterEqual++;
                    } else if (otherAircraft.distance < aircraft.distance) {
                        aircraft.distanceProperty.counterWorse++;
                    }

                    if (otherAircraft.seatCount > aircraft.seatCount) {
                        aircraft.seatCountProperty.counterBetter++;
                    } else if (otherAircraft.seatCount === aircraft.seatCount) {
                        aircraft.seatCountProperty.counterEqual++;
                    } else if (otherAircraft.seatCount < aircraft.seatCount) {
                        aircraft.seatCountProperty.counterWorse++;
                    }

                    if (otherAircraft.maxSeatCount > aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterBetter++;
                    } else if (otherAircraft.maxSeatCount === aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterEqual++;
                    } else if (otherAircraft.maxSeatCount < aircraft.maxSeatCount) {
                        aircraft.maxSeatCountProperty.counterWorse++;
                    }

                    if (otherAircraft.length > aircraft.length) {
                        aircraft.lengthProperty.counterBetter++;
                    } else if (otherAircraft.length === aircraft.length) {
                        aircraft.lengthProperty.counterEqual++;
                    } else if (otherAircraft.length < aircraft.length) {
                        aircraft.lengthProperty.counterWorse++;
                    }

                    if (otherAircraft.wingSpan > aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterBetter++;
                    } else if (otherAircraft.wingSpan === aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterEqual++;
                    } else if (otherAircraft.wingSpan < aircraft.wingSpan) {
                        aircraft.wingSpanProperty.counterWorse++;
                    }

                    if (otherAircraft.height > aircraft.height) {
                        aircraft.heightProperty.counterBetter++;
                    } else if (otherAircraft.height === aircraft.height) {
                        aircraft.heightProperty.counterEqual++;
                    } else if (otherAircraft.height < aircraft.height) {
                        aircraft.heightProperty.counterWorse++;
                    }

                    if (otherAircraft.consumption > aircraft.consumption) {
                        aircraft.consumptionProperty.counterBetter++;
                    } else if (otherAircraft.consumption === aircraft.consumption) {
                        aircraft.consumptionProperty.counterEqual++;
                    } else if (otherAircraft.consumption < aircraft.consumption) {
                        aircraft.consumptionProperty.counterWorse++;
                    }
                }
            }
        }
        this.minData = {
            [this._aircraftKeys.distance]: distanceMin,
            [this._aircraftKeys.seatCount]: seatCountMin,
            [this._aircraftKeys.maxSeatCount]: maxSeatCountMin,
            [this._aircraftKeys.length]: lengthMin,
            [this._aircraftKeys.wingSpan]: wingSpanMin,
            [this._aircraftKeys.height]: heightMin,
            [this._aircraftKeys.consumption]: consumptionMin
        };
        this.maxData = {
            [this._aircraftKeys.distance]: distanceMax,
            [this._aircraftKeys.seatCount]: seatCountMax,
            [this._aircraftKeys.maxSeatCount]: maxSeatCountMax,
            [this._aircraftKeys.length]: lengthMax,
            [this._aircraftKeys.wingSpan]: wingSpanMax,
            [this._aircraftKeys.height]: heightMax,
            [this._aircraftKeys.consumption]: consumptionMax
        };

        this.distance = Math.round(distance / count);
        this.seatCount = Math.round(seatCount / count);
        this.maxSeatCount = Math.round(maxSeatCount / count);
        this.length = Math.round((length / count) * 100) / 100;
        this.wingSpan = Math.round((wingSpan / count) * 100) / 100;
        this.height = Math.round((height / count) * 100) / 100;
        this.consumption = Math.round((consumption / count) * 100) / 100;
        this.distancePercent = (100 * (this.distance - this.minData.distance)) / (this.maxData.distance - this.minData.distance);
        this.seatCountPercent = (100 * (this.seatCount - this.minData.seatCount)) / (this.maxData.seatCount - this.minData.seatCount);
        this.maxSeatCountPercent = (100 * (this.maxSeatCount - this.minData.maxSeatCount)) / (this.maxData.maxSeatCount - this.minData.maxSeatCount);
        this.lengthPercent = (100 * (this.length - this.minData.length)) / (this.maxData.length - this.minData.length);
        this.wingSpanPercent = (100 * (this.wingSpan - this.minData.wingSpan)) / (this.maxData.wingSpan - this.minData.wingSpan);
        this.heightPercent = (100 * (this.height - this.minData.height)) / (this.maxData.height - this.minData.height);
        this.consumptionPercent = (100 * (this.consumption - this.minData.consumption)) / (this.maxData.consumption - this.minData.consumption);

        this.distancePercentReserve = 100 - this.distancePercent;
        this.seatCountPercentReserve = 100 - this.seatCountPercent;
        this.maxSeatCountPercentReserve = 100 - this.maxSeatCountPercent;
        this.lengthPercentReserve = 100 - this.lengthPercent;
        this.wingSpanPercentReserve = 100 - this.wingSpanPercent;
        this.heightPercentReserve = 100 - this.heightPercent;
        this.consumptionPercentReserve = 100 - this.consumptionPercent;
    }

    get aircrafts(): Array<Aircraft | CivilAircraft | MilitaryAircraft> {
        return this._aircrafts;
    }


    set aircrafts(value: Array<Aircraft | CivilAircraft | MilitaryAircraft>) {
        this._aircrafts = value;
    }

    get consumptionPercentReserve(): number {
        return this._consumptionPercentReserve;
    }

    set consumptionPercentReserve(value: number) {
        this._consumptionPercentReserve = value;
    }

    get heightPercentReserve(): number {
        return this._heightPercentReserve;
    }

    set heightPercentReserve(value: number) {
        this._heightPercentReserve = value;
    }

    get wingSpanPercentReserve(): number {
        return this._wingSpanPercentReserve;
    }

    set wingSpanPercentReserve(value: number) {
        this._wingSpanPercentReserve = value;
    }

    get lengthPercentReserve(): number {
        return this._lengthPercentReserve;
    }

    set lengthPercentReserve(value: number) {
        this._lengthPercentReserve = value;
    }

    get maxSeatCountPercentReserve(): number {
        return this._maxSeatCountPercentReserve;
    }

    set maxSeatCountPercentReserve(value: number) {
        this._maxSeatCountPercentReserve = value;
    }

    get seatCountPercentReserve(): number {
        return this._seatCountPercentReserve;
    }

    set seatCountPercentReserve(value: number) {
        this._seatCountPercentReserve = value;
    }

    get distancePercentReserve(): number {
        return this._distancePercentReserve;
    }

    set distancePercentReserve(value: number) {
        this._distancePercentReserve = value;
    }

    get consumptionPercent(): number {
        return this._consumptionPercent;
    }

    set consumptionPercent(value: number) {
        this._consumptionPercent = value;
    }

    get heightPercent(): number {
        return this._heightPercent;
    }

    set heightPercent(value: number) {
        this._heightPercent = value;
    }

    get wingSpanPercent(): number {
        return this._wingSpanPercent;
    }

    set wingSpanPercent(value: number) {
        this._wingSpanPercent = value;
    }

    get lengthPercent(): number {
        return this._lengthPercent;
    }

    set lengthPercent(value: number) {
        this._lengthPercent = value;
    }

    get maxSeatCountPercent(): number {
        return this._maxSeatCountPercent;
    }

    set maxSeatCountPercent(value: number) {
        this._maxSeatCountPercent = value;
    }

    get seatCountPercent(): number {
        return this._seatCountPercent;
    }

    set seatCountPercent(value: number) {
        this._seatCountPercent = value;
    }

    get distancePercent(): number {
        return this._distancePercent;
    }

    set distancePercent(value: number) {
        this._distancePercent = value;
    }

    get consumption(): number {
        return this._consumption;
    }

    set consumption(value: number) {
        this._consumption = value;
    }

    get height(): number {
        return this._height;
    }

    set height(value: number) {
        this._height = value;
    }

    get wingSpan(): number {
        return this._wingSpan;
    }

    set wingSpan(value: number) {
        this._wingSpan = value;
    }

    get length(): number {
        return this._length;
    }

    set length(value: number) {
        this._length = value;
    }

    get maxSeatCount(): number {
        return this._maxSeatCount;
    }

    set maxSeatCount(value: number) {
        this._maxSeatCount = value;
    }

    get seatCount(): number {
        return this._seatCount;
    }

    set seatCount(value: number) {
        this._seatCount = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get minData(): IaverageDataMinMax {
        return this._minData;
    }

    set minData(value: IaverageDataMinMax) {
        this._minData = value;
    }

    get maxData(): IaverageDataMinMax {
        return this._maxData;
    }

    set maxData(value: IaverageDataMinMax) {
        this._maxData = value;
    }
}
