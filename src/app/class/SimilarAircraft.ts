import {Aircraft} from "../aircrafts/Aircraft";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";

export class SimilarAircraft {

    constructor(private aircrafts: Array<Aircraft | CivilAircraft | MilitaryAircraft>, private withCurrentAircraft: boolean = false) {
    }

    public range(): any {
        return {
            "distance": 1000,
            "seatCount": 100,
            "maxSeatCount": 25,
            "length": 2.5,
            "wingSpan": 2.5,
            "height": 2.5,
            "consumption": 0.75,
        };
    }

    public inCalculationUse(): any {
        return {
            "distance": true,
            "seatCount": true,
            "maxSeatCount": true,
            "length": false,
            "wingSpan": false,
            "height": false,
            "consumption": false,
        };
    }

    public inRange(aircraft: number, minmax: number, statAircraft: number): any | number {
        const min: number = statAircraft - minmax;
        const max: number = statAircraft + minmax;
        return ((aircraft - min) * (aircraft - max) <= 0);
    }

    public getSimilarAircraft(aircraft: Aircraft): Array<Aircraft | CivilAircraft | MilitaryAircraft> {
        const result: Array<Aircraft | CivilAircraft | MilitaryAircraft> = [];
        if (this.withCurrentAircraft === true) {
            result.push(aircraft);
        }
        for (const item of this.aircrafts) {
            if (item.id === aircraft.id) {
                continue;
            }
            if (
                ((this.inCalculationUse().distance) ? this.inRange(item.distance, this.range().distance, aircraft.distance) : true) &&
                ((this.inCalculationUse().seatCount) ? this.inRange(item.seatCount, this.range().seatCount, aircraft.seatCount) : true) &&
                ((this.inCalculationUse().maxSeatCount) ? this.inRange(item.maxSeatCount, this.range().maxSeatCount, aircraft.maxSeatCount) : true) &&
                ((this.inCalculationUse().length) ? this.inRange(item.length, this.range().length, aircraft.length) : true) &&
                ((this.inCalculationUse().wingSpan) ? this.inRange(item.wingSpan, this.range().wingSpan, aircraft.wingSpan) : true) &&
                ((this.inCalculationUse().height) ? this.inRange(item.height, this.range().height, aircraft.height) : true) &&
                ((this.inCalculationUse().consumption) ? this.inRange(item.consumption, this.range().consumption, aircraft.consumption) : true)
            ) {
                result.push(item);
            }
        }

        return result;
    }
}
