import {Aircraft} from "../aircrafts/Aircraft";
import {Chart} from "chart.js";
import {IchartDialogData} from "../interfaces/Ichart-dialog-data";
import {UnitExtension} from "../keys/UnitExtension";
import {AircraftTitle} from "../keys/AircraftTitle";
import {AircraftKeys} from "../keys/AircraftKeys";
import {Dictionary} from "../keys/Dictionary";
import {DesignClass} from "./DesignClass";
import {Inject} from "@angular/core";
import {ChartService} from "../services/chart.service";

export class CreateChart {
    set chart(value: Chart) {
        this._chart = value;
    }
    get chart(): Chart {
        return this._chart;
    }
    private data: IchartDialogData;


    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();

    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private designClass: DesignClass = new DesignClass();
    private _chart: Chart;

    constructor(private chartService: ChartService) {

    }

    public make(data: any, thisdata: IchartDialogData, HtmlId: any = "canvas", clean: boolean = false): Chart {
        this.data = thisdata;
        const averageData: any = data.averageData;
        const percent: any = data.percent;
        const aircraft: Aircraft = percent.aircraft;
        const aircraftByID: Aircraft = this.data.behaviorSubject.getValue().getAircraftByID(this.data.aircraft.id);
        const nameToData: object = {
            [this.aircraftTitle.distance]: aircraftByID.distance.toLocaleString() + " " + this._unitExtension.distance,
            [this.aircraftTitle.seatCount]: aircraftByID.seatCount.toLocaleString() + " " + this._unitExtension.seatCount,
            [this.aircraftTitle.maxSeatCount]: aircraftByID.maxSeatCount.toLocaleString() + " " + this._unitExtension.maxSeatCount,
            [this.aircraftTitle.length]: aircraftByID.length.toLocaleString() + " " + this._unitExtension.length,
            [this.aircraftTitle.wingSpan]: aircraftByID.wingSpan.toLocaleString() + " " + this._unitExtension.wingSpan,
            [this.aircraftTitle.height]: aircraftByID.height.toLocaleString() + " " + this._unitExtension.height,
            [this.aircraftTitle.consumption]: aircraftByID.consumption.toLocaleString() + " " + this._unitExtension.consumption
        };

        const averageWorse: object = {
            [this.aircraftTitle.distance]: aircraftByID.distanceProperty.counterWorse,
            [this.aircraftTitle.seatCount]: aircraftByID.seatCountProperty.counterWorse,
            [this.aircraftTitle.maxSeatCount]: aircraftByID.maxSeatCountProperty.counterWorse,
            [this.aircraftTitle.length]: aircraftByID.lengthProperty.counterWorse,
            [this.aircraftTitle.wingSpan]: aircraftByID.wingSpanProperty.counterWorse,
            [this.aircraftTitle.height]: aircraftByID.heightProperty.counterWorse,
            [this.aircraftTitle.consumption]: aircraftByID.consumptionProperty.counterBetter
        };
        const averageBetter: object = {
            [this.aircraftTitle.distance]: aircraftByID.distanceProperty.counterBetter,
            [this.aircraftTitle.seatCount]: aircraftByID.seatCountProperty.counterBetter,
            [this.aircraftTitle.maxSeatCount]: aircraftByID.maxSeatCountProperty.counterBetter,
            [this.aircraftTitle.length]: aircraftByID.lengthProperty.counterBetter,
            [this.aircraftTitle.wingSpan]: aircraftByID.wingSpanProperty.counterBetter,
            [this.aircraftTitle.height]: aircraftByID.heightProperty.counterBetter,
            [this.aircraftTitle.consumption]: aircraftByID.consumptionProperty.counterWorse
        };
        const averageEqual: object = {
            [this.aircraftTitle.distance]: aircraftByID.distanceProperty.counterEqual,
            [this.aircraftTitle.seatCount]: aircraftByID.seatCountProperty.counterEqual,
            [this.aircraftTitle.maxSeatCount]: aircraftByID.maxSeatCountProperty.counterEqual,
            [this.aircraftTitle.length]: aircraftByID.lengthProperty.counterEqual,
            [this.aircraftTitle.wingSpan]: aircraftByID.wingSpanProperty.counterEqual,
            [this.aircraftTitle.height]: aircraftByID.heightProperty.counterEqual,
            [this.aircraftTitle.consumption]: aircraftByID.consumptionProperty.counterEqual
        };
        const averageToData: object = {
            [this.aircraftTitle.distance]: averageData.distance.toLocaleString() + " " + this._unitExtension.distance,
            [this.aircraftTitle.seatCount]: averageData.seatCount.toLocaleString() + " " + this._unitExtension.seatCount,
            [this.aircraftTitle.maxSeatCount]: averageData.maxSeatCount.toLocaleString() + " " + this._unitExtension.maxSeatCount,
            [this.aircraftTitle.length]: averageData.length.toLocaleString() + " " + this._unitExtension.length,
            [this.aircraftTitle.wingSpan]: averageData.wingSpan.toLocaleString() + " " + this._unitExtension.wingSpan,
            [this.aircraftTitle.height]: averageData.height.toLocaleString() + " " + this._unitExtension.height,
            [this.aircraftTitle.consumption]: averageData.consumption.toLocaleString() + " " + this._unitExtension.consumption
        };


        this._chart = new Chart(HtmlId, {
            type: "polarArea",
            data: {
                labels: this.chartService.chartLabels,
                datasets: [
                    {
                        label: this.dictionary.aircraft,
                        data: [
                            percent.aircraft.distance,
                            percent.aircraft.seatCount,
                            percent.aircraft.maxSeatCount,
                            percent.aircraft.length,
                            percent.aircraft.wingSpan,
                            percent.aircraft.height,
                            percent.aircraftReserve.consumption
                        ],
                        fill: false,
                        backgroundColor: [
                            this.designClass.getGreenToRed(percent.aircraft.distance),
                            this.designClass.getGreenToRed(percent.aircraft.seatCount),
                            this.designClass.getGreenToRed(percent.aircraft.maxSeatCount),
                            this.designClass.getGreenToRed(percent.aircraft.length),
                            this.designClass.getGreenToRed(percent.aircraft.wingSpan),
                            this.designClass.getGreenToRed(percent.aircraft.height),
                            this.designClass.getGreenToRed(percent.aircraftReserve.consumption)
                        ],
                        borderColor: [
                            this.designClass.getGreenToRed(percent.aircraft.distance, "1"),
                            this.designClass.getGreenToRed(percent.aircraft.seatCount, "1"),
                            this.designClass.getGreenToRed(percent.aircraft.maxSeatCount, "1"),
                            this.designClass.getGreenToRed(percent.aircraft.length, "1"),
                            this.designClass.getGreenToRed(percent.aircraft.wingSpan, "1"),
                            this.designClass.getGreenToRed(percent.aircraft.height, "1"),
                            this.designClass.getGreenToRed(percent.aircraftReserve.consumption, "1")
                        ],
                    },
                    {
                        label: this.dictionary.average,
                        data: [
                            averageData.distancePercent,
                            averageData.seatCountPercent,
                            averageData.maxSeatCountPercent,
                            averageData.lengthPercent,
                            averageData.wingSpanPercent,
                            averageData.heightPercent,
                            averageData.consumptionPercentReserve
                        ],
                        fill: false,
                        backgroundColor: [
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)",
                            "rgba(0, 0, 0, 0.5)"
                        ],
                        borderColor: [
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)",
                            "rgba(0, 0, 0, 0)"
                        ],
                        borderWidth: 1,
                    },
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: !clean,
                    text: this.data.aircraft.name,
                    position: "top",
                    fontSize: 16,
                    fontColor: "#111",
                    padding: 20
                },
                scale: {

                    ticks: {
                        display: clean,
                        min: 0,
                        max: 100
                    }
                },
                tooltips: {
                    callbacks: {
                        // tslint:disable-next-line:no-shadowed-variable
                        title: (tooltipItem: any, data: any) => {
                            return data["labels"][tooltipItem[0]["index"]];
                        },
                        // tslint:disable-next-line:no-shadowed-variable
                        label: (tooltipItem: any, data: any) => {
                            // return data['datasets'][0]['data'][tooltipItem['index']];

                            return "Aktuell: " + nameToData[data["labels"][tooltipItem["index"]]];

                        },
                        // tslint:disable-next-line:no-shadowed-variable
                        afterLabel: (tooltipItem: any, data: any) => {

                            return "\nDurchschnitt: " + averageToData[data["labels"][tooltipItem["index"]]] + "" +
                                "\nSchlechter: " + averageWorse[data["labels"][tooltipItem["index"]]] + "" +
                                "\nGleichwertig: " + averageEqual[data["labels"][tooltipItem["index"]]] + "" +
                                "\nBesser: " + averageBetter[data["labels"][tooltipItem["index"]]] + "";


                        }
                    },
                    backgroundColor: "#FFF",
                    titleFontSize: 16,
                    titleFontColor: "#0066ff",
                    bodyFontColor: "#000",
                    bodyFontSize: 14,
                    displayColors: false
                },
                legend: {
                    display: !clean,
                    position: "bottom",
                    labels: {
                        boxWidth: 20,
                        fontColor: "#111",
                        padding: 15
                    }
                },
                plugins: {
                    datalabels: {
                        color: "#000",
                        textAlign: "center",
                        font: {
                            lineHeight: 1.6
                        }
                    },
                    afterRender: (chart, options) => {

                    }
                }
            }
        });
        return this._chart;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }
}
