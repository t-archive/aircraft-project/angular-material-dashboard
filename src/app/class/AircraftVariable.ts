import {AircraftKeys} from "../keys/AircraftKeys";
import {errorObject} from "rxjs/internal-compatibility";

export class AircraftVariable {


    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private readonly startValue: string = "nothing";
    private nothing: string = "nothing";
    private danger: string = "is-danger";
    private ready: string = "ready";

    constructor(startPoint: string) {
        if (startPoint === "create") {
            this.startValue = this.nothing;
        } else if (startPoint === "edit") {
            this.startValue = this.ready;
        }
        this.clear();
    }

    private _name: string = this.startValue;
    private _distance: string = this.startValue;
    private _seatCount: string = this.startValue;
    private _maxSeatCount: string = this.startValue;
    private _length: string = this.startValue;
    private _wingSpan: string = this.startValue;
    private _height: string = this.startValue;
    private _consumption: string = this.startValue;
    private _airRefueling: string = this.startValue;
    private _flare: string = this.startValue;
    private _gateLicense: string = this.startValue;


    private _active: boolean = false;
    private _select: string;

    setStatus(select: any): void {
        this.select = select;
        if (this.select === this.aircraftKeys.all) {
            this.active = false;
            this.clear();
        } else {
            this.active = true;
            this.clear();
        }
    }

    clear(): void {
        this.name = this.startValue;
        this.distance = this.startValue;
        this.seatCount = this.startValue;
        this.maxSeatCount = this.startValue;
        this.length = this.startValue;
        this.wingSpan = this.startValue;
        this.height = this.startValue;
        this.consumption = this.startValue;
        this.airRefueling = this.startValue;
        this.flare = this.startValue;
        this.gateLicense = this.startValue;
    }

    submitRelease(): boolean {
        let error: boolean = false;
        if (this.active === false) {
            error = true;
        } else {

            if (this.name !== "ready") {
                error = true;
            }
            if (this.distance !== "ready") {
                error = true;
            }
            if (this.seatCount !== "ready") {
                error = true;
            }
            if (this.maxSeatCount !== "ready") {
                error = true;
            }
            if (this.length !== "ready") {
                error = true;
            }
            if (this.wingSpan !== "ready") {
                error = true;
            }
            if (this.height !== "ready") {
                error = true;
            }
            if (this.consumption !== "ready") {
                error = true;
            }
            if (this.select === this.aircraftKeys.military && this.airRefueling !== "ready") {
                error = true;
            }
            if (this.select === this.aircraftKeys.military && this.flare !== "ready") {
                error = true;
            }
            if (this.select === this.aircraftKeys.civil && this.gateLicense !== "ready") {
                error = true;
            }
        }
        return error === false;
    }

    check(value: any, functionName: string): any {

        if (this.active === false) {
            value = "";
        }
        const type: string = typeof value;
        if (value) {
            this[functionName] = "ready";

        } else {
            if (type === "number" && value === 0) {
                this[functionName] = "ready";
            } else {
                this[functionName] = "is-danger";
            }
        }
    }

    get select(): string {
        return this._select;
    }

    set select(value: string) {
        this._select = value;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get gateLicense(): string {
        return this._gateLicense;
    }

    set gateLicense(value: string) {
        this._gateLicense = value;
    }

    get flare(): string {
        return this._flare;
    }

    set flare(value: string) {
        this._flare = value;
    }

    get airRefueling(): string {
        return this._airRefueling;
    }

    set airRefueling(value: string) {
        this._airRefueling = value;
    }

    get consumption(): string {
        return this._consumption;
    }

    set consumption(value: string) {
        this._consumption = value;
    }

    get height(): string {
        return this._height;
    }

    set height(value: string) {
        this._height = value;
    }

    get wingSpan(): string {
        return this._wingSpan;
    }

    set wingSpan(value: string) {
        this._wingSpan = value;
    }

    get length(): string {
        return this._length;
    }

    set length(value: string) {
        this._length = value;
    }

    get maxSeatCount(): string {
        return this._maxSeatCount;
    }

    set maxSeatCount(value: string) {
        this._maxSeatCount = value;
    }

    get seatCount(): string {
        return this._seatCount;
    }

    set seatCount(value: string) {
        this._seatCount = value;
    }

    get distance(): string {
        return this._distance;
    }

    set distance(value: string) {
        this._distance = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }


    get active(): boolean {
        return this._active;
    }

    set active(value: boolean) {
        this._active = value;
    }

}
