export const ID_LOGIN_DB_KEY: string = "id";
export const EMAIL_LOGIN_DB_KEY: string = "email";
export const FIRSTNAME_LOGIN_DB_KEY: string = "firstname";
export const SURNAME_LOGIN_DB_KEY: string = "surname";
export const USERNAME_LOGIN_DB_KEY: string = "username";
export const PASSWORD_LOGIN_DB_KEY: string = "password";
export const ACTIVE_LOGIN_DB_KEY: string = "active";
export const ROLES_LOGIN_DB_KEY: string = "roles";
export const ID_TOKEN_DB_KEY: string = "id";
export const USER_ID_TOKEN_DB_KEY: string = "userID";
export const TOKEN_TOKEN_DB_KEY: string = "token";
export const EXPIRYTIME_TOKEN_DB_KEY: string = "expirytime";

export class LoginKeys {
    private _idLogin: string = ID_LOGIN_DB_KEY;
    private _emailLogin: string = EMAIL_LOGIN_DB_KEY;
    private _firstnameLogin: string = FIRSTNAME_LOGIN_DB_KEY;
    private _surnameLogin: string = SURNAME_LOGIN_DB_KEY;
    private _usernameLogin: string = USERNAME_LOGIN_DB_KEY;
    private _passwordLogin: string = PASSWORD_LOGIN_DB_KEY;
    private _activeLogin: string = ACTIVE_LOGIN_DB_KEY;
    private _rolesLogin: string = ROLES_LOGIN_DB_KEY;
    private _idToken: string = ID_TOKEN_DB_KEY;
    private _userIdToken: string = USER_ID_TOKEN_DB_KEY;
    private _tokenToken: string = TOKEN_TOKEN_DB_KEY;
    private _expirytimeToken: string = EXPIRYTIME_TOKEN_DB_KEY;

    constructor() {
    }

    get expirytimeToken(): string {
        return this._expirytimeToken;
    }

    get tokenToken(): string {
        return this._tokenToken;
    }

    get userIdToken(): string {
        return this._userIdToken;
    }

    get idToken(): string {
        return this._idToken;
    }

    get rolesLogin(): string {
        return this._rolesLogin;
    }

    get activeLogin(): string {
        return this._activeLogin;
    }

    get passwordLogin(): string {
        return this._passwordLogin;
    }

    get usernameLogin(): string {
        return this._usernameLogin;
    }

    get surnameLogin(): string {
        return this._surnameLogin;
    }

    get firstnameLogin(): string {
        return this._firstnameLogin;
    }

    get emailLogin(): string {
        return this._emailLogin;
    }

    get idLogin(): string {
        return this._idLogin;
    }

}
