export type SweetAlertIconRequirements = "success" | "error" | "warning" | "info" | "question";
export type ToastStatus = "wait" | "open" | "close";

