import {environment} from "../../environments/environment";
import {LANGUAGE_KEY_ENGLISH} from "./config";

export class AircraftTitle {
    constructor(lang: string = environment.language) {
        this.lookForLanguage(lang);
    }

    private _aircrafts: string = "Flugzeuge";
    private _aircraft: string = "Flugzeug";
    private _id: string = "ID";
    private _name: string = "Name";
    private _distance: string = "Reichweite";
    private _seatCount: string = "Typische Sitzanzahl";
    private _maxSeatCount: string = "Maximale Sitzanzahl";
    private _length: string = "Länge";
    private _wingSpan: string = "Flügelspannweite";
    private _height: string = "Höhe";
    private _consumption: string = "Verbrauch";
    private _airRefueling: string = "Luftbetankung";
    private _flare: string = "Flare";
    private _gateLicense: string = "Gate Lizenz";


    private lookForLanguage(lang: string): void {
        if (lang === LANGUAGE_KEY_ENGLISH) {
            this._aircrafts = "Aircrafts";
            this._aircraft = "Aircraft";
            this._id = "ID";
            this._name = "Name";
            this._distance = "Distance";
            this._seatCount = "Typical Seats";
            this._maxSeatCount = "Maximum Seats";
            this._length = "Length";
            this._wingSpan = "Wingspan";
            this._height = "Height";
            this._consumption = "Consumption";
            this._airRefueling = "Air Refueling";
            this._flare = "Flare";
            this._gateLicense = "Gate License";
        }
    }

    get aircraft(): string {
        return this._aircraft;
    }

    get gateLicense(): string {
        return this._gateLicense;
    }

    get flare(): string {
        return this._flare;
    }

    get airRefueling(): string {
        return this._airRefueling;
    }

    get consumption(): string {
        return this._consumption;
    }

    get height(): string {
        return this._height;
    }

    get wingSpan(): string {
        return this._wingSpan;
    }

    get length(): string {
        return this._length;
    }

    get maxSeatCount(): string {
        return this._maxSeatCount;
    }

    get seatCount(): string {
        return this._seatCount;
    }

    get distance(): string {
        return this._distance;
    }

    get name(): string {
        return this._name;
    }

    get id(): string {
        return this._id;
    }

    get aircrafts(): string {
        return this._aircrafts;
    }
}
