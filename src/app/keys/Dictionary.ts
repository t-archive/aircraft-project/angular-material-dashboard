import {environment} from "../../environments/environment";
import {LANGUAGE_KEY_ENGLISH} from "./config";

export class Dictionary {
    get owner(): string {
        return this._owner;
    }
    get unknown(): string {
        return this._unknown;
    }
    get youWillBeLoggedOutRightAway(): string {
        return this._youWillBeLoggedOutRightAway;
    }
    get accountCreatedSuccessfully(): string {
        return this._accountCreatedSuccessfully;
    }
    get registrationFailed(): string {
        return this._registrationFailed;
    }

    get loginFailed(): string {
        return this._loginFailed;
    }

    get accountAlreadyExists(): string {
        return this._accountAlreadyExists;
    }

    get passwords(): string {
        return this._passwords;
    }

    get agree(): string {
        return this._agree;
    }

    constructor(lang: string = environment.language) {
        this.lookForLanguage(lang);
    }

    private _saveChange: string = "Speichern";

    private _cancel: string = "Schließen";

    private _homePage: string = "Startseite";
    private _allAircrafts: string = "Alle Flugzeuge";
    private _civilAircrafts: string = "Zivile Flugzeuge";
    private _militaryAircrafts: string = "Militärflugzeuge";
    private _civilAircraft: string = "Ziviles Flugzeug";
    private _militaryAircraft: string = "Militärflugzeug";
    private _aircrafts: string = "Flugzeuge";
    private _aircraft: string = "Flugzeug";
    private _search: string = "Suche";
    private _search1: string = "Suchen";
    private _create: string = "Erstellen";
    private _edit: string = "Bearbeiten";
    private _delete: string = "Löschen";
    private _yes: string = "Ja";
    private _no: string = "Nein";
    private _pleaseCloseThisWindowModal: string = "Bitte schließen Sie dieses Fenster";
    private _wasDeleted: string = "wurde Gelöscht!";
    private _wasNotDeletedDeletionError: string = "wurde nicht Gelöscht! Fehler beim Löschen!";
    private _wasNotDeleted: string = "wurde nicht Gelöscht!";
    private _wasEdited: string = "wurde Bearbeitet!";
    private _wasNotEditedErrorWhileEditing: string = "wurde nicht Bearbeitet! Fehler beim Bearbeiten!";
    private _aircraftTypeSelect: string = "Flugzeug Typ";
    private _pleaseSelect: string = "Bitte auswählen";
    private _createFailed: string = "Konnte nicht Erstellt werden!";
    private _editFailed: string = "Konnte nicht Bearbeitet werden!";
    private _deleteFailed: string = "Konnte nicht Gelöscht werden!";
    private _itemsPerPageLabel: string = "Einträge pro Seite:";
    private _firstPageLabel: string = "Erste Seite";
    private _lastPageLabel: string = "Letzte Seite";
    private _nextPageLabel: string = "Nächste Seite";
    private _previousPageLabel: string = "Bisherige Seite";
    private _doYouWant: string = "Wollen sie";
    private _reallyDelete: string = "wirklich löschen?";
    private _average: string = "Durchschnitt";
    private _current_AC: string = "Aktuelles";
    private _look: string = "Ansehen";
    private _similar: string = "Ähnliche";
    private _competing: string = "Konkurrierende";
    private _ranking: string = "Rang:";
    private _noDataAvailable: string = "Keine Daten verfügbar";
    private _seatCount: string = "Sitzanzahl";
    private _from: string = "von";
    private _currentlyInTheAir: string = "Aktuell in der Luft";
    private _latitude: string = "Breitengrad";
    private _longitude: string = "Längengrad";
    private _track: string = "Strecke";
    private _calibratedAltitude: string = "Kalibrierte Höhe";
    private _groundSpeed: string = "Fluggeschwindigkeit";
    private _squawk: string = "Squawk";
    private _registration: string = "Registrierung";
    private _departureAirport: string = "Abflug Flughafen";
    private _arrivalAirport: string = "Ankunft Flughafen";
    private _flightNumber: string = "Flugnummer";
    private _verticalSpeed: string = "Vertikale Geschwindigkeit";
    private _flightRoute: string = "Flugroute";
    private _airline: string = "Fluggesellschaft";
    private _status: string = "Status";
    private _landingIn: string = "Landung in";
    private _landed: string = "Gelandet";
    private _fly2: string = "geflogen";
    private _flight: string = "Flug";
    private _generalData: string = "Allgemeine Daten";
    private _image: string = "Bild";
    private _carriedOutBy: string = "durchgeführt von";
    private _with: string = "mit";
    private _operator: string = "Betreiber";
    private _owner: string = "Besitzer";
    private _terminal: string = "Terminal";
    private _gate: string = "Gate";
    private _baggageReclaim: string = "Gepäckausgabe";
    private _baggage: string = "Gepäck";
    private _location: string = "Standort";
    private _localtime: string = "Ortszeit";
    private _website: string = "Webseite";
    private _arrival: string = "Ankunft";
    private _departure: string = "Abflug";
    private _toOpenIn: string = "Öffnen in";
    private _toOpen: string = "Öffnen";
    private _graph: string = "Diagramm";
    private _noCallsign: string = "Kein Rufzeichen";
    private _areNotAllowed: string = "sind nicht erlaubt";
    private _Taxing_Standby: string = "Taxing/Standby";
    private _Start_Landing: string = "Start/Landung";
    private _fly: string = "Fliegt";
    private _height: string = "Höhe";
    private _login: string = "Login";
    private _log_in: string = "Einloggen";
    private _register: string = "Registrieren";
    private _onThisWebsite: string = "auf dieser Webseite";
    private _eMailAddress: string = "E-Mail Adresse";
    private _password: string = "Passwort";
    private _passwords: string = "Passwörter";
    private _repeatPassword: string = "Passwort wiederholen";
    private _firstName: string = "Vorname";
    private _surname: string = "Nachname";
    private _username: string = "Username";
    private _thePasswordsDoNotMatch: string = "Die Passwörter stimmen nicht überein";
    private _pleaseEnterAValidEmailAddress: string = "Bitte geben Sie eine gültige E-Mail-Adresse ein";
    private _emailIsRequired: string = "E-Mail ist erforderlich";
    private _isRequired: string = "ist erforderlich";
    private _agree: string = "stimmen überein";
    private _accountAlreadyExists: string = "Konto existiert bereits";
    private _loginFailed: string = "Anmeldung Fehlgeschlagen";
    private _registrationFailed: string = "Registrierung Fehlgeschlagen";
    private _accountCreatedSuccessfully: string = "Account erfolgreich erstellt";
    private _youWillBeLoggedOutRightAway: string = "Sie werden gleich ausgeloggt!";
    private _unknown: string = "Unbekannt";

    private lookForLanguage(lang: string): void {
        if (lang === LANGUAGE_KEY_ENGLISH) {
            this._saveChange = "Save Change";
            this._cancel = "Cancel";
            this._homePage = "Homepage";
            this._allAircrafts = "All Aircrafts";
            this._civilAircrafts = "Civil Aircrafts";
            this._militaryAircrafts = "Military Aircrafts";
            this._civilAircraft = "Civil Airplane";
            this._militaryAircraft = "Military Aircraft";
            this._aircrafts = "Aircrafts";
            this._aircraft = "Aircraft";
            this._search = "search";
            this._search1 = "Search";
            this._create = "Create";
            this._edit = "Edit";
            this._delete = "Delete";
            this._yes = "Yes";
            this._no = "No";
            this._pleaseCloseThisWindowModal = "Please close this window";
            this._wasDeleted = "has been deleted!";
            this._wasNotDeletedDeletionError = "was not deleted! Deletion error!";
            this._wasNotDeleted = "was not deleted!";
            this._wasEdited = "was edited!";
            this._wasNotEditedErrorWhileEditing = "was not edited! Editing error!";
            this._aircraftTypeSelect = "Aircraft type";
            this._pleaseSelect = "Please Select";
            this._createFailed = "Couldn't be created!";
            this._editFailed = "Couldn't be edited!";
            this._deleteFailed = "Could not be deleted!";
            this._itemsPerPageLabel = "Entries per page:";
            this._firstPageLabel = "First page";
            this._lastPageLabel = "Last page";
            this._nextPageLabel = "Next page";
            this._previousPageLabel = "Previous page";
            this._doYouWant = "Do you want";
            this._reallyDelete = "really delete?";
            this._average = "Average";
            this._current_AC = "Current";
            this._look = "Look at";
            this._similar = "Similar";
            this._competing = "Competing";
            this._ranking = "Rank:";
            this._noDataAvailable = "No data available";
            this._seatCount = "Number of seats";
            this._from = "from";
            this._currentlyInTheAir = "Currently in the air";
            this._latitude = "Latitude";
            this._longitude = "Longitude";
            this._track = "Track";
            this._calibratedAltitude = "Calibrated Altitude";
            this._groundSpeed = "Ground Speed";
            this._squawk = "Squawk";
            this._registration = "Registration";
            this._departureAirport = "Departure Airport";
            this._arrivalAirport = "Arrival Airport";
            this._flightNumber = "Flight Number";
            this._verticalSpeed = "Vertical Speed";
            this._flightRoute = "Flight Route";
            this._airline = "Airline";
            this._status = "Status";
            this._landingIn = "Landing in";
            this._landed = "Landed";
            this._fly2 = "Flown";
            this._flight = "Flight";
            this._generalData = "General Data";
            this._image = "Image";
            this._carriedOutBy = "carried out by";
            this._with = "with";
            this._operator = "Operator";
            this._terminal = "Terminal";
            this._gate = "Gate";
            this._baggageReclaim = "Baggage";
            this._baggage = "Baggage";
            this._location = "Location";
            this._localtime = "Localtime";
            this._website = "Website";
            this._arrival = "Arrival";
            this._departure = "Departure";
            this._toOpenIn = "Opening in";
            this._toOpen = "Open";
            this._graph = "Diagram";
            this._noCallsign = "No Callsign";
            this._areNotAllowed = "are not allowed";
            this._Taxing_Standby = "Taxing/Standby";
            this._Start_Landing = "Start/Landing";
            this._fly = "Fly";
            this._height = "Height";
            this._login = "Login";
            this._log_in = "Login";
            this._register = "Register";
            this._onThisWebsite = "on this Website";
            this._eMailAddress = "E-Mail Address";
            this._password = "Password";
            this._passwords = "Passwords";
            this._repeatPassword = "Repeat Password";
            this._firstName = "First Name";
            this._surname = "Surname";
            this._username = "Username";
            this._thePasswordsDoNotMatch = "The passwords do not match";
            this._pleaseEnterAValidEmailAddress = "Please enter a valid email address";
            this._emailIsRequired = "Email is required";
            this._isRequired = "is required";
            this._agree = "agree";
            this._accountAlreadyExists = "Account already exists";
            this._loginFailed = "Login failed";
            this._registrationFailed = "Registration failed";
            this._accountCreatedSuccessfully = "Account created successfully";
            this._youWillBeLoggedOutRightAway = "You will be logged out right away!";
            this._unknown = "Unknown";
            this._owner = "Owner";
        }
    }

    get doYouWant(): string {
        return this._doYouWant;
    }

    get reallyDelete(): string {
        return this._reallyDelete;
    }


    get previousPageLabel(): string {
        return this._previousPageLabel;
    }


    get nextPageLabel(): string {
        return this._nextPageLabel;
    }

    get lastPageLabel(): string {
        return this._lastPageLabel;
    }

    get firstPageLabel(): string {
        return this._firstPageLabel;
    }

    get itemsPerPageLabel(): string {
        return this._itemsPerPageLabel;
    }

    get deleteFailed(): string {
        return this._deleteFailed;
    }

    get editFailed(): string {
        return this._editFailed;
    }

    get pleaseSelect(): string {
        return this._pleaseSelect;
    }

    get aircraftTypeSelect(): string {
        return this._aircraftTypeSelect;
    }

    get create(): string {
        return this._create;
    }

    get wasNotEditedErrorWhileEditing(): string {
        return this._wasNotEditedErrorWhileEditing;
    }

    get wasEdited(): string {
        return this._wasEdited;
    }

    get wasNotDeleted(): string {
        return this._wasNotDeleted;
    }

    get wasNotDeletedDeletionError(): string {
        return this._wasNotDeletedDeletionError;
    }

    get wasDeleted(): string {
        return this._wasDeleted;
    }


    get cancel(): string {
        return this._cancel;
    }

    get pleaseCloseThisWindowModal(): string {
        return this._pleaseCloseThisWindowModal;
    }

    get no(): string {
        return this._no;
    }

    get yes(): string {
        return this._yes;
    }

    get delete(): string {
        return this._delete;
    }

    get edit(): string {
        return this._edit;
    }

    get search(): string {
        return this._search;
    }

    get aircraft(): string {
        return this._aircraft;
    }

    get aircrafts(): string {
        return this._aircrafts;
    }

    get militaryAircrafts(): string {
        return this._militaryAircrafts;
    }

    get civilAircrafts(): string {
        return this._civilAircrafts;
    }

    get allAircrafts(): string {
        return this._allAircrafts;
    }

    get homePage(): string {
        return this._homePage;
    }

    get saveChange(): string {
        return this._saveChange;
    }

    get createFailed(): string {
        return this._createFailed;
    }

    get average(): string {
        return this._average;
    }

    get militaryAircraft(): string {
        return this._militaryAircraft;
    }

    get civilAircraft(): string {
        return this._civilAircraft;
    }

    get current_AC(): string {
        return this._current_AC;
    }

    get look(): string {
        return this._look;
    }

    get similar(): string {
        return this._similar;
    }

    get competing(): string {
        return this._competing;
    }

    get ranking(): string {
        return this._ranking;
    }

    get noDataAvailable(): string {
        return this._noDataAvailable;
    }

    get seatCount(): string {
        return this._seatCount;
    }

    get from(): string {
        return this._from;
    }

    get status(): string {
        return this._status;
    }

    get airline(): string {
        return this._airline;
    }

    get flightRoute(): string {
        return this._flightRoute;
    }

    get verticalSpeed(): string {
        return this._verticalSpeed;
    }

    get flightNumber(): string {
        return this._flightNumber;
    }

    get arrivalAirport(): string {
        return this._arrivalAirport;
    }

    get departureAirport(): string {
        return this._departureAirport;
    }

    get registration(): string {
        return this._registration;
    }

    get squawk(): string {
        return this._squawk;
    }

    get groundSpeed(): string {
        return this._groundSpeed;
    }

    get calibratedAltitude(): string {
        return this._calibratedAltitude;
    }

    get track(): string {
        return this._track;
    }

    get longitude(): string {
        return this._longitude;
    }

    get latitude(): string {
        return this._latitude;
    }

    get currentlyInTheAir(): string {
        return this._currentlyInTheAir;
    }

    get landingIn(): string {
        return this._landingIn;
    }

    get landed(): string {
        return this._landed;
    }

    get generalData(): string {
        return this._generalData;
    }

    get fly2(): string {
        return this._fly2;
    }

    get height(): string {
        return this._height;
    }

    get fly(): string {
        return this._fly;
    }

    get Taxing_Standby(): string {
        return this._Taxing_Standby;
    }

    get Start_Landing(): string {
        return this._Start_Landing;
    }

    get toOpen(): string {
        return this._toOpen;
    }

    get areNotAllowed(): string {
        return this._areNotAllowed;
    }

    get noCallsign(): string {
        return this._noCallsign;
    }

    get graph(): string {
        return this._graph;
    }

    get toOpenIn(): string {
        return this._toOpenIn;
    }

    get departure(): string {
        return this._departure;
    }

    get arrival(): string {
        return this._arrival;
    }

    get website(): string {
        return this._website;
    }

    get localtime(): string {
        return this._localtime;
    }

    get location(): string {
        return this._location;
    }

    get baggage(): string {
        return this._baggage;
    }

    get baggageReclaim(): string {
        return this._baggageReclaim;
    }

    get gate(): string {
        return this._gate;
    }

    get terminal(): string {
        return this._terminal;
    }

    get operator(): string {
        return this._operator;
    }

    get with(): string {
        return this._with;
    }

    get carriedOutBy(): string {
        return this._carriedOutBy;
    }

    get search1(): string {
        return this._search1;
    }

    get flight(): string {
        return this._flight;
    }

    get image(): string {
        return this._image;
    }

    get password(): string {
        return this._password;
    }

    get eMailAddress(): string {
        return this._eMailAddress;
    }

    get log_in(): string {
        return this._log_in;
    }

    get onThisWebsite(): string {
        return this._onThisWebsite;
    }

    get register(): string {
        return this._register;
    }

    get login(): string {
        return this._login;
    }

    get repeatPassword(): string {
        return this._repeatPassword;
    }

    get firstName(): string {
        return this._firstName;
    }

    get surname(): string {
        return this._surname;
    }

    get username(): string {
        return this._username;
    }

    get isRequired(): string {
        return this._isRequired;
    }

    get emailIsRequired(): string {
        return this._emailIsRequired;
    }

    get pleaseEnterAValidEmailAddress(): string {
        return this._pleaseEnterAValidEmailAddress;
    }

    get thePasswordsDoNotMatch(): string {
        return this._thePasswordsDoNotMatch;
    }
}
