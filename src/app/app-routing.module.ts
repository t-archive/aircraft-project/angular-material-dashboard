import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./components/home/home.component";

import {AllAircraftComponent} from "./components/all-aircraft/all-aircraft.component";
import {CivilAircraftComponent} from "./components/civil-aircraft/civil-aircraft.component";
import {MilitaryAircraftComponent} from "./components/military-aircraft/military-aircraft.component";
import {FlightDataComponent} from "./components/flight-data/flight-data.component";
import {ChartModalComponent} from "./components/chart-modal/chart-modal.component";
import {DetailPageComponent} from "./components/detail-page/detail-page.component";
import {RegisterComponent} from "./components/register/register.component";
import {LoginComponent} from "./components/login/login.component";


export const routes: Routes = [
    {path: "home", component: HomeComponent},
    {path: "all", component: AllAircraftComponent},
    {path: "civil", component: CivilAircraftComponent},
    {path: "military", component: MilitaryAircraftComponent},
    {path: "login", component: LoginComponent},
    {path: "register", component: RegisterComponent},
    {path: "detail/:id", redirectTo: "detail/base/all/:id"},
    {path: "detail/base/:typ/aircraft/:id", component: DetailPageComponent},
    {path: "", component: HomeComponent},
    {path: "**", redirectTo: "/all"}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
    public static getPath(): any {
        return RouterModule.forRoot(routes);
    }
}
