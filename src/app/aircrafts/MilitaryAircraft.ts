import {Aircraft} from "./Aircraft";
import {AircraftKeys} from "../keys/AircraftKeys";

export class MilitaryAircraft extends Aircraft {
    private aircraftKeys: AircraftKeys = new AircraftKeys();
    private _flare: boolean;
    private _airRefueling: boolean;

    get flare(): boolean {
        return this._flare;
    }

    set flare(value: boolean) {
        this._flare = value;
    }

    get airRefueling(): boolean {
        return this._airRefueling;
    }

    set airRefueling(value: boolean) {
        this._airRefueling = value;
    }

    getMilitaryAircraft(): any {

        const array: object = {
            [this.aircraftKeys.id]: this.id,
            [this.aircraftKeys.name]: this.name,
            [this.aircraftKeys.distance]: this.distance,
            [this.aircraftKeys.seatCount]: this.seatCount,
            [this.aircraftKeys.maxSeatCount]: this.maxSeatCount,
            [this.aircraftKeys.length]: this.length,
            [this.aircraftKeys.wingSpan]: this.wingSpan,
            [this.aircraftKeys.height]: this.height,
            [this.aircraftKeys.consumption]: this.consumption,
            [this.aircraftKeys.flare]: this._flare,
            [this.aircraftKeys.airRefueling]: this._airRefueling
        };
        // tslint:disable-next-line:radix
        if (isNaN(parseInt(String(this.id)))) {
            delete array["id"];
        }
        return array;
    }

    toJSON(): object {

        return {
            ...super.toJSON(), ...{[this.aircraftKeys.flare]: this._flare, [this.aircraftKeys.airRefueling]: this._airRefueling}
        };
    }
}
