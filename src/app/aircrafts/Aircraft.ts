import {
    CONSUMPTION_KEY,
    DISTANCE_KEY,
    HEIGHT_KEY, ID_KEY,
    LENGTH_KEY,
    MAXSEATCOUNT_KEY,
    NAME_KEY,
    SEATCOUNT_KEY,
    WINGSPAN_KEY
} from "../keys/AircraftKeys";
import {PropertyComparison} from "../class/PropertyComparison";

export abstract class Aircraft {


    private _distanceProperty: PropertyComparison = new PropertyComparison();
    private _seatCountProperty: PropertyComparison = new PropertyComparison();
    private _maxSeatCountProperty: PropertyComparison = new PropertyComparison();
    private _lengthProperty: PropertyComparison = new PropertyComparison();
    private _wingSpanProperty: PropertyComparison = new PropertyComparison();
    private _heightProperty: PropertyComparison = new PropertyComparison();
    private _consumptionProperty: PropertyComparison = new PropertyComparison();

    private _id: number;
    private _name: string;
    private _distance: number;
    private _seatCount: number;
    private _maxSeatCount: number;
    private _length: number;
    private _wingSpan: number;
    private _height: number;
    private _consumption: number;

    constructor(name: string, distance: number, seatCount: number, maxSeatCount: number, length: number, wingSpan: number, height: number, consumption: number) {
        this._name = name;
        this._distance = distance;
        this._seatCount = seatCount;
        this._maxSeatCount = maxSeatCount;
        this._length = length;
        this._wingSpan = wingSpan;
        this._height = height;
        this._consumption = consumption;
    }

    public containsSearchValue(value: string): boolean {
        return this._name.toLowerCase().indexOf(value) > -1
            || this._distance.toString().toLowerCase().indexOf(value) > -1
            || this._seatCount.toString().toLowerCase().indexOf(value) > -1
            || this._maxSeatCount.toString().toLowerCase().indexOf(value) > -1
            || this._length.toString().toLowerCase().indexOf(value) > -1
            || this._wingSpan.toString().toLowerCase().indexOf(value) > -1
            || this._height.toString().toLowerCase().indexOf(value) > -1
            || this._consumption.toString().toLowerCase().indexOf(value) > -1;
    }
    public toJSON(): object {
        const result: object = {};

        if (typeof this._id === "number") {
            result[ID_KEY] = this._id;
        }
        if (typeof this._name === "string") {
            result[NAME_KEY] = this._name;
        }
        if (typeof this._distance === "number") {
            result[DISTANCE_KEY] = this._distance;
        }
        if (typeof this._seatCount === "number") {
            result[SEATCOUNT_KEY] = this._seatCount;
        }
        if (typeof this._maxSeatCount === "number") {
            result[MAXSEATCOUNT_KEY] = this._maxSeatCount;
        }
        if (typeof this._length === "number") {
            result[LENGTH_KEY] = parseFloat(this._length.toFixed(2));
        }
        if (typeof this._wingSpan === "number") {
            result[WINGSPAN_KEY] = parseFloat(this._wingSpan.toFixed(2));
        }
        if (typeof this._height === "number") {
            result[HEIGHT_KEY] = parseFloat(this._height.toFixed(2));
        }
        if (typeof this._consumption === "number") {
            result[CONSUMPTION_KEY] = parseFloat(this._consumption.toFixed(0));
        }

        return result;
    }

    set id(value: number) {
        this._id = value;
    }

    get consumption(): number {
        return this._consumption;
    }

    set consumption(value: number) {
        this._consumption = value;
    }

    get height(): number {
        return this._height;
    }

    set height(value: number) {
        this._height = value;
    }

    get wingSpan(): number {
        return this._wingSpan;
    }

    set wingSpan(value: number) {
        this._wingSpan = value;
    }

    get length(): number {
        return this._length;
    }

    set length(value: number) {
        this._length = value;
    }

    get maxSeatCount(): number {
        return this._maxSeatCount;
    }

    set maxSeatCount(value: number) {
        this._maxSeatCount = value;
    }

    get seatCount(): number {
        return this._seatCount;
    }

    set seatCount(value: number) {
        this._seatCount = value;
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this._distance = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get id(): number {
        return this._id;
    }



    get consumptionProperty(): PropertyComparison {
        return this._consumptionProperty;
    }

    set consumptionProperty(value: PropertyComparison) {
        this._consumptionProperty = value;
    }
    get heightProperty(): PropertyComparison {
        return this._heightProperty;
    }

    set heightProperty(value: PropertyComparison) {
        this._heightProperty = value;
    }
    get wingSpanProperty(): PropertyComparison {
        return this._wingSpanProperty;
    }

    set wingSpanProperty(value: PropertyComparison) {
        this._wingSpanProperty = value;
    }
    get lengthProperty(): PropertyComparison {
        return this._lengthProperty;
    }

    set lengthProperty(value: PropertyComparison) {
        this._lengthProperty = value;
    }
    get maxSeatCountProperty(): PropertyComparison {
        return this._maxSeatCountProperty;
    }

    set maxSeatCountProperty(value: PropertyComparison) {
        this._maxSeatCountProperty = value;
    }
    get seatCountProperty(): PropertyComparison {
        return this._seatCountProperty;
    }

    set seatCountProperty(value: PropertyComparison) {
        this._seatCountProperty = value;
    }
    get distanceProperty(): PropertyComparison {
        return this._distanceProperty;
    }

    set distanceProperty(value: PropertyComparison) {
        this._distanceProperty = value;
    }

}

