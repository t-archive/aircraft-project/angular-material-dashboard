import {Component, OnInit} from "@angular/core";
import {AircraftFactory} from "../../factories/AircraftFactory";

@Component({
    selector: "app-home",
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {

    constructor() {

    }

    ngOnInit(): any {
    }

}
