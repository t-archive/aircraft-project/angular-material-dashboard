import {AfterViewInit, Component, Inject, Input, OnInit, ViewChild} from "@angular/core";
import {Aircraft} from "../../aircrafts/Aircraft";
import {AircraftService} from "../../services/AircraftService";
import {DesignClass} from "../../class/DesignClass";
import {StoreService} from "../../services/store.service";
import {UnitExtension} from "../../keys/UnitExtension";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {MatDialog, MatInput, MatTable} from "@angular/material";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {Dictionary} from "../../keys/Dictionary";
import {CreateModalComponent} from "../create-modal/create-modal.component";

import {MatSort} from "@angular/material/sort";
import {MatTableDataSource} from "@angular/material/table";
import {DataSource} from "@angular/cdk/collections";
import {MatPaginator} from "@angular/material/paginator";
import {ChartModalComponent} from "../chart-modal/chart-modal.component";
import {ChartDialogDataInterface} from "../../interfaces/Ichart-dialog-data";
import {ChartService} from "../../services/chart.service";
import {MatInputModule} from "@angular/material/typings/input";
import {LoginService} from "../../services/login.service";

@Component({
    selector: "app-all-aircraft",
    templateUrl: "./all-aircraft.component.html",
    styleUrls: ["./all-aircraft.component.css"]
})
export class AllAircraftComponent implements OnInit, AfterViewInit {

    private _allAircrafts: Array<Aircraft>;


    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _designClass: DesignClass = new DesignClass();
    // private _pageSizeOptions: Array<number> = [5, 10, 15, 20, 25, 50];
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    private searchFilter: any;
    displayedColumns: string[] = [
        this._aircraftKeys.id,
        this._aircraftKeys.name,
        this._aircraftKeys.distance,
        this._aircraftKeys.seatCount,
        this._aircraftKeys.maxSeatCount,
        this._aircraftKeys.length,
        this._aircraftKeys.wingSpan,
        this._aircraftKeys.height,
        this._aircraftKeys.consumption,
        this._aircraftKeys.gateLicense,
        this._aircraftKeys.flare,
        this._aircraftKeys.airRefueling,
        "buttonAction"
    ];
    private _dataSource: any;

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService,
                @Inject(StoreService) private store: StoreService,
                @Inject(MatDialog) private dialog: MatDialog,
                @Inject(LoginService) private loginService: LoginService,
                @Inject(ChartService) private chartService: ChartService) {

        this._aircraftService.allAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {

                this._allAircrafts = aircraftRepository.aircrafts;
                this.dataSource = new MatTableDataSource(aircraftRepository.aircrafts);
                this.setParms();
            }
        });

    }

    ngOnInit(): void {
        this.setParms();
        this.store.filterTextBehaviorSubject.subscribe(message => {
            this._aircraftService.allAircraftsFilter(message);
        });

    }

    ngAfterViewInit(): void {

    }

    public applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    private checkFilter(): void {
        if (this.searchFilter) {
            if (this.dataSource.filter !== this.searchFilter) {
                this.applyFilter(this.searchFilter);
            }
        }

    }

    private setParms(): void {
        this.checkFilter();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        if (this.dataSource.paginator) {
            this.dataSource.paginator._intl.firstPageLabel = this.dictionary.firstPageLabel;
            this.dataSource.paginator._intl.lastPageLabel = this.dictionary.lastPageLabel;
            this.dataSource.paginator._intl.nextPageLabel = this.dictionary.nextPageLabel;
            this.dataSource.paginator._intl.previousPageLabel = this.dictionary.previousPageLabel;
            this.dataSource.paginator._intl.itemsPerPageLabel = this.dictionary.itemsPerPageLabel;

            let lengthData: number = this.dataSource.data.length;
            if (this.dataSource.data.length !== this.dataSource.filteredData.length) {
                lengthData = this.dataSource.filteredData.length;
            }
            this.dataSource.paginator._length = lengthData;
            this.dataSource.paginator._pageSizeOptions = [5, 10, 15, 20, 25, 50, lengthData];
            // this.pageSizeOptions = [5, 10, 15, 20, 25, 50, lengthData];
        }
    }

    public openEditModal(aircraft: Aircraft): void {
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: aircraft,
            autoFocus: false
        });

    }

    public openChartModal(aircraft: Aircraft): void {
        const dialogRef: any = this.dialog.open(ChartModalComponent, {
            data: new ChartDialogDataInterface({
                aircraft: aircraft,
                behaviorSubject: this._aircraftService.allAircraftsBehaviorSubject,
                type: this.aircraftKeys.all
            }),
            autoFocus: false
        });
    }

    public openCreateModal(): void {
        const dialogRef: any = this.dialog.open(CreateModalComponent, {
            data: {
                behaviorSubject: this._aircraftService.allAircraftsBehaviorSubject,
                type: this.aircraftKeys.all
            },
            autoFocus: false
        });
    }

    // sort(key: string): void {
    //     this._aircraftService.allAircraftsSort(key);
    // }

    public delete(id: number): void {
        this._aircraftService.allAircraftDelete(id);
    }

    get allAircrafts(): Array<Aircraft> {
        return this._allAircrafts;
    }


    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get designClass(): DesignClass {
        return this._designClass;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get dataSource(): any {
        return this._dataSource;
    }

    set dataSource(value: any) {
        this._dataSource = value;
    }

    // get pageSizeOptions(): Array<number> {
    //     return this._pageSizeOptions;
    // }
    //
    // set pageSizeOptions(value: Array<number>) {
    //     this._pageSizeOptions = value;
    // }

}

