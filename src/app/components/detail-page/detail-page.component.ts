import {AfterViewInit, Component, ContentChildren, Inject, OnDestroy, OnInit, QueryList, ViewChild, ViewContainerRef} from "@angular/core";
import {MAT_DIALOG_DATA, MatSort, MatTab, MatBadge} from "@angular/material";
import {Aircraft} from "../../aircrafts/Aircraft";
import {ChartService} from "../../services/chart.service";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {ChartDialogDataInterface, IchartDialogData} from "../../interfaces/Ichart-dialog-data";
import {Chart} from "chart.js";
import {DesignClass} from "../../class/DesignClass";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {UnitExtension} from "../../keys/UnitExtension";
import {CreateChart} from "../../class/CreateChart";
import {FlightDataService} from "../../services/flight-data.service";
import {ActivatedRoute, Router, UrlTree} from "@angular/router";
import {AircraftService} from "../../services/AircraftService";
import {FlightDataComponent} from "../flight-data/flight-data.component";
import {HttpClient} from "@angular/common/http";
import {AppRoutingModule, routes} from "../../app-routing.module";

@Component({
    selector: "app-detail-page",
    templateUrl: "./detail-page.component.html",
    styleUrls: ["./detail-page.component.scss"]
})
export class DetailPageComponent implements OnInit {

    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private designClass: DesignClass = new DesignClass();
    private _linkAircraftID: any;
    private _linkBase: any;
    private _linkFlightNumber: string;
    private _chart: Chart;
    private _clickButton: boolean = false;
    private _wait: boolean = true;
    private data: IchartDialogData;
    private flightData: FlightDataComponent;
    private _similarAircrafts: Array<Aircraft | CivilAircraft | MilitaryAircraft> = [];
    @ViewChild("canvasAircraftCharts", {static: false}) private canvasAircraftCharts: any;
    private infoDataSource: any = [];

    constructor(@Inject(AircraftService) private aircraftService: AircraftService,
                @Inject(ChartService) private chartService: ChartService,
                @Inject(Router) private router: Router,
                @Inject(ActivatedRoute) private route: ActivatedRoute,
                @Inject(FlightDataService) private _flightData: FlightDataService) {
        this.checkGenerateTable();
        this.route.paramMap.subscribe(() => {
            this.infoDataSource.length = 0;
            // this.wait = true;
            this.init();
            this.checkGenerateTable();
            // this.chartService.chart
            if (this._createChart && this._createChart.chart) {
                this._createChart.chart.clear();
            }
        });
    }

    private _createChart: CreateChart;

    private checkGenerateTable(): void {
        const interval: any = setInterval(() => {
            if (this.flightData && this.flightData.icaoCode && this.flightData.icaoCode !== "") {
                this.wait = false;
                this.generateTable();
                clearInterval(interval);
            }
        }, 100);
    }

    private init(): void {
        this._linkAircraftID = this.route.snapshot.paramMap.get("id");
        this._linkBase = this.route.snapshot.paramMap.get("typ");
        this._linkFlightNumber = this.route.snapshot.paramMap.get("flightNumber");
        if (this._linkBase === this._aircraftKeys.all) {
            this.aircraftService.allAircraftsBehaviorSubject.subscribe((aircraftRepository: AircraftRepository) => {
                this.data = new ChartDialogDataInterface({
                    // aircraft: aircraftRepository.aircrafts.filter()
                    aircraft: aircraftRepository.aircrafts.filter((aircraft: Aircraft | CivilAircraft | MilitaryAircraft) => {
                        if (aircraft.id.toString().indexOf(this._linkAircraftID) >= 0) {
                            return aircraft;
                        }
                    })[0],
                    behaviorSubject: this.aircraftService.allAircraftsBehaviorSubject,
                    type: this.aircraftKeys.all
                });
            });
        } else if (this._linkBase === this._aircraftKeys.civil) {
            this.aircraftService.civilAircraftsBehaviorSubject.subscribe((aircraftRepository: AircraftRepository) => {
                this.data = new ChartDialogDataInterface({
                    // aircraft: aircraftRepository.aircrafts.filter()
                    aircraft: aircraftRepository.aircrafts.filter((aircraft: Aircraft | CivilAircraft) => {
                        if (aircraft.id.toString().indexOf(this._linkAircraftID) >= 0) {
                            return aircraft;
                        }
                    })[0],
                    behaviorSubject: this.aircraftService.civilAircraftsBehaviorSubject,
                    type: this.aircraftKeys.civil
                });
                // this.startClick();
            });
        } else if (this._linkBase === this._aircraftKeys.military) {
            this.aircraftService.militaryAircraftsBehaviorSubject.subscribe((aircraftRepository: AircraftRepository) => {
                this.data = new ChartDialogDataInterface({
                    // aircraft: aircraftRepository.aircrafts.filter()
                    aircraft: aircraftRepository.aircrafts.filter((aircraft: Aircraft | MilitaryAircraft) => {
                        if (aircraft.id.toString().indexOf(this._linkAircraftID) >= 0) {
                            return aircraft;
                        }
                    })[0],
                    behaviorSubject: this.aircraftService.militaryAircraftsBehaviorSubject,
                    type: this.aircraftKeys.military
                });
                // this.startClick();
            });

        }
        if (this._linkBase === this._aircraftKeys.all) {
            this.aircraftService.allAircraftsBehaviorSubject.subscribe((data) => {
                if (data.aircrafts) {
                    this.startClick();
                }

            });

        } else if (this._linkBase === this._aircraftKeys.civil) {
            this.aircraftService.civilAircraftsBehaviorSubject.subscribe((data) => {
                if (data.aircrafts) {
                    this.startClick();
                }
            });
        } else if (this._linkBase === this._aircraftKeys.military) {
            this.aircraftService.militaryAircraftsBehaviorSubject.subscribe((data) => {
                if (data.aircrafts) {
                    this.startClick();
                }
            });

        }
    }

    ngOnInit(): any {

    }


    public receiveInputFlightData($event: FlightDataComponent): void {
        if ($event) {
            this.flightData = $event;
        }
    }


    public start(): void {
        this._createChart = new CreateChart(this.chartService);
        const data: any = this.chartService.setData(this.data, "detail-page");
        this._similarAircrafts = data.similarAircrafts;
        this.makeChart(data);
    }

    private startClick(): void {
        setTimeout(() => this.start(), 1);
    }

    public makeChart(data: any, HtmlId: any = "canvas"): void {
        (new Chart(HtmlId, {}).clear());
        this._createChart.make(data, this.data, HtmlId, false);
    }

    private generateTable(): void {
        this.generateTableFunction();

    }

    private generateTableFunction(): void {
        let name: string;
        if (this._flightData.aircraftListData[this.flightData.icaoCode].result.name.indexOf(this.data.aircraft.name) <= 0) {
            name = this.data.aircraft.name + " (" + this._flightData.aircraftListData[this.flightData.icaoCode].result.name + ")";
        } else {
            name = this._flightData.aircraftListData[this.flightData.icaoCode].result.name;
        }
        this.infoDataSource = [
            {
                key: this.aircraftTitle.name,
                value: name
            },
            {
                key: "ICAO-Code / IATA-Code",
                value: this._flightData.aircraftListData[this.flightData.icaoCode].result.icao + " / " + this._flightData.aircraftListData[this.flightData.icaoCode].result.iata
            },
            {
                key: this._dictionary.currentlyInTheAir,
                value: this._flightData.aircraftListData[this.flightData.icaoCode].dataSource.data.length + " " + this._dictionary.aircrafts
            },
            {
                key: this._aircraftTitle.distance,
                value: this.data.aircraft.distance.toLocaleString() + " " + this._unitExtension.distance
            },
            {
                key: this.aircraftTitle.seatCount + " / " + this._aircraftTitle.maxSeatCount,
                value: this.data.aircraft.seatCount.toLocaleString() + " " + this._unitExtension.seatCount + " / "
                    + this.data.aircraft.maxSeatCount.toLocaleString() + " " + this._unitExtension.maxSeatCount
            },
            {
                key: this._aircraftTitle.length + " / " + this._aircraftTitle.wingSpan + " / " + this._aircraftTitle.height,
                value: this.data.aircraft.length.toLocaleString() + " " + this._unitExtension.length + " / " +
                    this.data.aircraft.wingSpan.toLocaleString() + " " + this._unitExtension.wingSpan + " / " +
                    this.data.aircraft.height.toLocaleString() + " " + this._unitExtension.height
            },
            {
                key: this._aircraftTitle.consumption,
                value: this.data.aircraft.consumption.toLocaleString() + " " + this._unitExtension.consumption
            }
        ];
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get wait(): boolean {
        return this._wait;
    }

    set wait(value: boolean) {
        this._wait = value;
    }

}
