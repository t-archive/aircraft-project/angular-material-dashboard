import {Component, Inject, OnInit} from "@angular/core";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {MAT_DIALOG_DATA} from "@angular/material";
import {BehaviorSubject} from "rxjs";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {HTTPService} from "../../services/http.service";
import {AircraftVariable} from "../../class/AircraftVariable";
import {Aircraft} from "../../aircrafts/Aircraft";
import {AircraftService} from "../../services/AircraftService";
import {InformationNotificationService} from "../../services/information-notification.service";

@Component({
    selector: "app-create-modal",
    templateUrl: "./create-modal.component.html",
    styleUrls: ["./create-modal.component.css"]
})
export class CreateModalComponent implements OnInit {


    private _aircraftVariableStore: AircraftVariable = new AircraftVariable("create");
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _type: string = "";
    private _typeDisabled: boolean = false;

    private _name: string;
    private _distance: number;
    private _seatCount: number;
    private _maxSeatCount: number;
    private _length: number;
    private _wingSpan: number;
    private _height: number;
    private _consumption: number;
    private _gateLicense: boolean;
    private _flare: boolean;
    private _airRefueling: boolean;
    private _selectAircraftTag: string;
    private _submitButton: boolean;
    private behaviorSubject: BehaviorSubject<AircraftRepository>;


    constructor(@Inject(MAT_DIALOG_DATA) private data: any, @Inject(AircraftService) private aircraftService: AircraftService, @Inject(HTTPService) private httpService: HTTPService, @Inject(InformationNotificationService) private informationNotification: InformationNotificationService) {
        if (this.data.type === this.aircraftKeys.all) {
            this.typeDisabled = true;
        }
        this.type = this.data.type;
        this.behaviorSubject = this.data.behaviorSubject as BehaviorSubject<AircraftRepository>;
    }

    ngOnInit(): void {

    }

    update(): void {
        if (this.type === this.aircraftKeys.military) {
            this.selectAircraftTag = "is-success";
            this.submitButton = true;
        } else if (this.type === this.aircraftKeys.civil) {
            this.selectAircraftTag = "is-success";
            this.submitButton = true;
        } else {
            this.selectAircraftTag = "is-danger";
            this.submitButton = false;
        }
        this.submitButton = this.aircraftVariableStore.submitRelease();
    }

    save(): void {
        if (this.type === this.aircraftKeys.military) {
            const newAircraft: MilitaryAircraft = new MilitaryAircraft(this.name, this.distance, this.seatCount, this.maxSeatCount, this.length, this.wingSpan, this.height, this.consumption);
            newAircraft.flare = this.flare;
            newAircraft.airRefueling = this.airRefueling;
            this.postBySave(newAircraft);
        } else if (this.type === this.aircraftKeys.civil) {
            const newAircraft: CivilAircraft = new CivilAircraft(this.name, this.distance, this.seatCount, this.maxSeatCount, this.length, this.wingSpan, this.height, this.consumption);
            newAircraft.gateLicense = this.gateLicense;
            this.postBySave(newAircraft);
        }

    }

    private postBySave(newAircraft: CivilAircraft | MilitaryAircraft): void {

        if (newAircraft) {
            if (this.type === this.aircraftKeys.civil) {
                this.httpService.postCivilAircraft(newAircraft as CivilAircraft).subscribe(
                    (success) => {
                        const status: boolean = this.httpService.checkIfNoError(success);
                        if (status === true) {
                                this.aircraftService.allAircraftsBehaviorSubject.getValue().add(newAircraft);
                                this.aircraftService.civilAircraftsBehaviorSubject.getValue().add(newAircraft);
                                this.aircraftService.allAircraftsBehaviorSubject.next(this.aircraftService.allAircraftsBehaviorSubject.getValue());
                                this.aircraftService.civilAircraftsBehaviorSubject.next(this.aircraftService.civilAircraftsBehaviorSubject.getValue());
                                this.aircraftService.reload();
                            // this.behaviorSubject.getValue().add(newAircraft);
                        } else {
                            this.informationNotification.createFailed(newAircraft);
                        }
                    },
                    error => console.error(error)
                );
            } else if (this.type === this.aircraftKeys.military) {
                this.httpService.postMilitaryAircraft(newAircraft as MilitaryAircraft).subscribe(
                    (success) => {
                        const status: boolean = this.httpService.checkIfNoError(success);
                        if (status === true) {
                            // this.behaviorSubject.getValue().add(newAircraft);
                            this.aircraftService.allAircraftsBehaviorSubject.getValue().add(newAircraft);
                            this.aircraftService.militaryAircraftsBehaviorSubject.getValue().add(newAircraft);
                            this.aircraftService.allAircraftsBehaviorSubject.next(this.aircraftService.allAircraftsBehaviorSubject.getValue());
                            this.aircraftService.militaryAircraftsBehaviorSubject.next(this.aircraftService.militaryAircraftsBehaviorSubject.getValue());
                            this.aircraftService.reload();
                        } else {
                            this.informationNotification.createFailed(newAircraft);
                        }
                    },
                    error => console.error(error)
                );
            }
        } else {
            console.warn("Failed");
        }
    }

    maxTitleLength(text: string = "", max: number): string {
        if (max) {
            if (text.length > max) {
                return text.substring(0, (max - 4)).trim() + "...";
            } else {
                return text.trim();
            }
        } else {
            console.warn("edit-modal.component.ts - maxTitleLength(): No Text or Max");
            return text.trim();
        }
    }

    set type(value: string) {
        this.aircraftVariableStore.setStatus(value);
        this._type = value;
        this.selectAircraftTag = "";
        this.submitButton = false;
    }

    get type(): string {
        return this._type;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftVariableStore(): AircraftVariable {
        return this._aircraftVariableStore;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }


    get airRefueling(): boolean {
        return this._airRefueling;
    }

    set airRefueling(value: boolean) {
        this.aircraftVariableStore.check(value, "airRefueling");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._airRefueling = false;
        } else if (oldValue === "true") {
            this._airRefueling = true;
        }
        this.update();
    }

    get flare(): boolean {
        return this._flare;
    }

    set flare(value: boolean) {
        this.aircraftVariableStore.check(value, "flare");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._flare = false;
        } else if (oldValue === "true") {
            this._flare = true;
        }
        this.update();
    }

    get gateLicense(): boolean {
        return this._gateLicense;
    }

    set gateLicense(value: boolean) {
        this.aircraftVariableStore.check(value, "gateLicense");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._gateLicense = false;
        } else if (oldValue === "true") {
            this._gateLicense = true;
        }
        this.update();
    }

    get consumption(): number {
        return this._consumption;
    }

    set consumption(value: number) {
        this.aircraftVariableStore.check(value, "consumption");
        this._consumption = value;
        this.update();
    }

    get height(): number {
        return this._height;
    }

    set height(value: number) {
        this.aircraftVariableStore.check(value, "height");
        this._height = value;
        this.update();
    }

    get wingSpan(): number {
        return this._wingSpan;
    }

    set wingSpan(value: number) {
        this.aircraftVariableStore.check(value, "wingSpan");
        this._wingSpan = value;
        this.update();
    }

    get length(): number {
        return this._length;
    }

    set length(value: number) {
        this.aircraftVariableStore.check(value, "length");
        this._length = value;
        this.update();
    }

    get maxSeatCount(): number {
        return this._maxSeatCount;
    }

    set maxSeatCount(value: number) {
        this.aircraftVariableStore.check(value, "maxSeatCount");
        this._maxSeatCount = value;
        this.update();
    }

    get seatCount(): number {
        return this._seatCount;
    }

    set seatCount(value: number) {
        this.aircraftVariableStore.check(value, "seatCount");
        this._seatCount = value;
        this.update();
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this.aircraftVariableStore.check(value, "distance");
        this._distance = value;
        this.update();
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this.aircraftVariableStore.check(value, "name");
        this._name = value;
        this.update();
    }

    get selectAircraftTag(): string {
        return this._selectAircraftTag;
    }

    set selectAircraftTag(value: string) {
        this._selectAircraftTag = value;
    }

    get submitButton(): boolean {
        return this._submitButton;
    }

    set submitButton(value: boolean) {
        this._submitButton = value;
    }

    get typeDisabled(): boolean {
        return this._typeDisabled;
    }

    set typeDisabled(value: boolean) {
        this._typeDisabled = value;
    }

}
