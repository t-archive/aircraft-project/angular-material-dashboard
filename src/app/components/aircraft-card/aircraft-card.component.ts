import {AfterViewInit, Component, Inject, Input, OnInit, Output, ViewChild} from "@angular/core";
import {Aircraft} from "../../aircrafts/Aircraft";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {DesignClass} from "../../class/DesignClass";
import {ChartDialogDataInterface, IchartDialogData} from "../../interfaces/Ichart-dialog-data";
import {AircraftService} from "../../services/AircraftService";
import {BehaviorSubject, Subject} from "rxjs";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {ChartModalComponent} from "../chart-modal/chart-modal.component";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material";
import {ChartService} from "../../services/chart.service";
import {CreateChart} from "../../class/CreateChart";
import {environment} from "../../../environments/environment";
import * as Chart from "chart.js";
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
    selector: "app-aircraft-card",
    templateUrl: "./aircraft-card.component.html",
    styleUrls: ["./aircraft-card.component.scss"],
    animations: [
        trigger("fadeInOut", [
            transition(":enter", [   // :enter is alias to 'void => *'
                style({opacity: 0}),
                animate(500, style({opacity: 1}))
            ]),
            transition(":leave", [   // :leave is alias to '* => void'
                animate(0, style({opacity: 0}))
            ])
        ])
    ]
})
export class AircraftCardComponent implements OnInit, AfterViewInit {
    private _aircraft: Aircraft | CivilAircraft | MilitaryAircraft;
    private _aircraftSubject: Subject<Aircraft | CivilAircraft | MilitaryAircraft> = new Subject<Aircraft | CivilAircraft | MilitaryAircraft>();
    private _dataFromCurrentAircraft: IchartDialogData;
    private _dataFromCurrentAircraftSubject: Subject<IchartDialogData> = new Subject<IchartDialogData>();
    private _currentAircraftSubject: Subject<Aircraft | CivilAircraft | MilitaryAircraft> = new Subject<Aircraft|CivilAircraft|MilitaryAircraft>();
    private _currentAircraft: Aircraft | CivilAircraft | MilitaryAircraft;
    private _buttonDisabledSubject: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    private _buttonDisabled: boolean = false;
    @Input()
    set aircraftObject(aircraftObject: Aircraft | CivilAircraft | MilitaryAircraft) {
        this._aircraftSubject.next(aircraftObject);
        this._aircraft = aircraftObject;
    }


    @Input()
    set dataFromCurrentAircraft(dataFromCurrentAircraft: IchartDialogData) {
        this._dataFromCurrentAircraftSubject.next(dataFromCurrentAircraft);
        this._dataFromCurrentAircraft = dataFromCurrentAircraft;
    }
    @Input()
    set currentAircraft(currentAircraft: Aircraft | CivilAircraft | MilitaryAircraft) {
        this._currentAircraftSubject.next(currentAircraft);
        this._currentAircraft = currentAircraft;
    }
    @Input()
    set buttonDisabled(buttonDisabled: boolean) {
        this._buttonDisabledSubject.next(buttonDisabled);
        this._buttonDisabled = buttonDisabled;
    }
    // @Output() readyRendering: boolean = false;
    private _unitExtension: UnitExtension = new UnitExtension();

    private _aircraftTitle: AircraftTitle = new AircraftTitle();

    private _aircraftKeys: AircraftKeys = new AircraftKeys();

    private _dictionary: Dictionary = new Dictionary();
    private designClass: DesignClass = new DesignClass();
    private _aircraftType: string = "";
    private _selectedType: string;
    private _behaviorSubject: BehaviorSubject<AircraftRepository>;
    private data: IchartDialogData;
    private isThatCurrentAircraft: boolean = false;
    private _isSwitch: boolean = false;
    private _displayedColumns: string[] = ["name", "value"];
    private _dataSource: Array<object>;
    @ViewChild("canvasElementAircraft", {static: true}) private canvasElementAircraft: any;

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService, @Inject(ChartService) private chartService: ChartService,
                @Inject(MatDialog) private dialog: MatDialog) {
        this._buttonDisabledSubject.subscribe((info: boolean) => {
            this._buttonDisabled = info;
        });
    }

    private _createChart: CreateChart = new CreateChart(this.chartService);

    private chart: Chart;

    private rankingPercent: number = 0;

    ngOnInit(): void {
        this.isThatCurrentAircraft = this._aircraft === this._dataFromCurrentAircraft.aircraft;
        this.init();
        this._aircraftSubject.subscribe((data: Aircraft | CivilAircraft | MilitaryAircraft) => {
            this._aircraft = data;
            this._dataFromCurrentAircraftSubject.subscribe((dataFromCurrentAircraftSubject: IchartDialogData) => {
                this._dataFromCurrentAircraft = dataFromCurrentAircraftSubject;
                this.isThatCurrentAircraft = false;
                this.init();
            });
        });
    }

    private init(): void {

        this._currentAircraftSubject.subscribe((aircraft: Aircraft | CivilAircraft | MilitaryAircraft) => {
            this.isThatCurrentAircraft = this._aircraft === aircraft;
        });

        if (this.aircraft instanceof CivilAircraft) {
            this._aircraftType = this._dictionary.civilAircraft;
        } else if (this.aircraft instanceof MilitaryAircraft) {
            this._aircraftType = this._dictionary.militaryAircraft;
        }
    }

    ngAfterViewInit(): void {
        setTimeout(() => this.switchContentInTable(), 100);
        this.start();
    }

    public start(): void {

        if (this._dataFromCurrentAircraft.type === this._aircraftKeys.all) {
            this._selectedType = this._aircraftKeys.all;
            this._behaviorSubject = this._aircraftService.allAircraftsBehaviorSubject;
        } else if (this._dataFromCurrentAircraft.type === this._aircraftKeys.civil) {
            this._selectedType = this._aircraftKeys.civil;
            this._behaviorSubject = this._aircraftService.civilAircraftsBehaviorSubject;
        } else if (this._dataFromCurrentAircraft.type === this._aircraftKeys.military) {
            this._selectedType = this._aircraftKeys.military;
            this._behaviorSubject = this._aircraftService.militaryAircraftsBehaviorSubject;
        }
        if (this.aircraft instanceof CivilAircraft) {
            this._aircraftType = this._dictionary.civilAircraft;
        } else if (this.aircraft instanceof MilitaryAircraft) {
            this._aircraftType = this._dictionary.militaryAircraft;
        }
        this.data = new ChartDialogDataInterface({
            aircraft: this.aircraft,
            behaviorSubject: this._behaviorSubject,
            type: this._aircraftKeys.all
        });

        const data: any = this.chartService.setData(this.data, "aircraft-card");
        const HtmlId: any = "canvasAircraft" + this._aircraft.id;
        if (data) {
            const tmpData: any = ((data.percent.aircraft.distance + data.percent.aircraft.seatCount + data.percent.aircraft.maxSeatCount +
                data.percent.aircraft.length + data.percent.aircraft.wingSpan + data.percent.aircraft.height + data.percent.aircraft.consumption));
            // this.rankingPercent = Math.round((tmpData / data.percent.aircraft.length) * 100) / 100;
            this.rankingPercent = (((tmpData / 7) * 100));
            this.rankingPercent = (Math.round(this.rankingPercent) / 100);
            this.rankingPercent = parseFloat(this.rankingPercent.toFixed(2));
        }

        // "aircraft": {
        //         "distance": distance,
        //         "seatCount": seatCount,
        //         "maxSeatCount": maxSeatCount,
        //         "length": length,
        //         "wingSpan": wingSpan,
        //         "height": height,
        //         "consumption": consumption
        // },

        this._createChart.make(data, this.data, HtmlId, true);
    }

    public switchContentInTable(): void {
        this.dataSource = [
            {name: this._dictionary.ranking, value: this.rankingPercent.toLocaleString() + " " + this._unitExtension.percent},
            {name: "", value: ""},
            {name: this._aircraftTitle.distance, value: this._aircraft.distance.toLocaleString() + " " + this._unitExtension.distance},
            {name: this._aircraftTitle.seatCount, value: this._aircraft.seatCount.toLocaleString() + " " + this._unitExtension.seatCount},
            {
                name: this._aircraftTitle.maxSeatCount,
                value: this._aircraft.maxSeatCount.toLocaleString() + " " + this._unitExtension.maxSeatCount
            },
            {name: this._aircraftTitle.length, value: this._aircraft.length.toLocaleString() + " " + this._unitExtension.length},
            {name: this._aircraftTitle.wingSpan, value: this._aircraft.wingSpan.toLocaleString() + " " + this._unitExtension.wingSpan},
            {name: this._aircraftTitle.height, value: this._aircraft.height.toLocaleString() + " " + this._unitExtension.height},
            {
                name: this._aircraftTitle.consumption,
                value: this._aircraft.consumption.toLocaleString() + " " + this._unitExtension.consumption
            }
        ];
    }

    public openChartModal(aircraft: Aircraft): void {
        const dialogRef: any = this.dialog.open(ChartModalComponent, {
            data: new ChartDialogDataInterface({
                aircraft: aircraft,
                behaviorSubject: this._aircraftService.allAircraftsBehaviorSubject,
                type: this._aircraftKeys.all
            }),
            autoFocus: false
        });
    }

    get aircraft(): Aircraft | CivilAircraft | MilitaryAircraft {
        return this._aircraft;
    }

    set aircraft(value: Aircraft | CivilAircraft | MilitaryAircraft) {
        this._aircraft = value;
    }

    get aircraftType(): string {
        return this._aircraftType;
    }

    set aircraftType(value: string) {
        this._aircraftType = value;
    }

    get isSwitch(): boolean {
        return this._isSwitch;
    }

    set isSwitch(value: boolean) {
        if (value === false) {
            setTimeout(() => this.start(), 1);
        } else {

        }
        this._isSwitch = value;
    }

    get displayedColumns(): string[] {
        return this._displayedColumns;
    }

    set displayedColumns(value: string[]) {
        this._displayedColumns = value;
    }

    get dataSource(): Array<object> {
        return this._dataSource;
    }

    set dataSource(value: Array<object>) {
        this._dataSource = value;
    }

}
