import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { SearchAircraftsComponent } from "./search-aircrafts.component";

describe("SearchAircraftsComponent", () => {
  let component: SearchAircraftsComponent;
  let fixture: ComponentFixture<SearchAircraftsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchAircraftsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchAircraftsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
