import {Component, Inject, OnInit} from "@angular/core";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {LoginService} from "../../services/login.service";
import {InformationNotificationService} from "../../services/information-notification.service";
import {FormControl, ValidationErrors, Validators} from "@angular/forms";
import {Observable, ObservableInput} from "rxjs";

@Component({
    selector: "app-register",
    templateUrl: "./register.component.html",
    styleUrls: ["./register.component.scss"]
})
export class RegisterComponent implements OnInit {
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _email: string = "";
    private _password: string = "";
    private _repeatPassword: string = "";
    private _username: string = "";
    private _firstname: string = "";
    private _surname: string = "";
    private _emailControl: FormControl = new FormControl("", [Validators.required, Validators.email]);
    private _usernameControl: FormControl = new FormControl("", [Validators.required]);
    private _firstnameControl: FormControl = new FormControl("", [Validators.required]);
    private _surnameControl: FormControl = new FormControl("", [Validators.required]);
    private _passwordControl: FormControl = new FormControl("", [Validators.required]);
    private _repeatPasswordControl: FormControl = new FormControl("", [Validators.required]);
    private _status: boolean | string = false;
    private _statusSuccess: boolean | string = false;

    constructor(@Inject(LoginService) private _loginService: LoginService, @Inject(InformationNotificationService) private informationNotificationService: InformationNotificationService) {

        this.emailControl.valueChanges.subscribe((data: any) => {
            this.emailSync = data;
        });
        this.usernameControl.valueChanges.subscribe((data: any) => {
            this.username = data;
        });
        this.firstnameControl.valueChanges.subscribe((data: any) => {
            this.firstname = data;
        });
        this.surnameControl.valueChanges.subscribe((data: any) => {
            this.surname = data;
        });
        this.passwordControl.valueChanges.subscribe((data: any) => {
            this.password = data;
        });
        this.repeatPasswordControl.valueChanges.subscribe((data: any) => {
            this.repeatPassword = data;
            if (this.password !== this.repeatPassword) {
                this.repeatPasswordControl.setErrors({"isNotTheSamePassword": true});
            }
        });
    }

    ngOnInit(): void {
    }

    private register(): void {
        if (this.password === this.repeatPassword) {
            this._loginService.register(this.email, this.username, this.firstname, this.surname, this.password).subscribe((data) => {
                if (data && data.body && data.body.data) {
                    if (data.body.data.code) {
                        this._status = this.dictionary.accountAlreadyExists + "!";
                        this._statusSuccess = false;
                    } else {
                        this._status = false;
                        this._statusSuccess = this.dictionary.accountCreatedSuccessfully + "!";
                    }
                }
            });
        } else {
            this._status = this.dictionary.registrationFailed + "!";
            this._statusSuccess = false;
        }
    }

    private keyDownFunction: any = (event: KeyboardEvent): any => event.code === "Enter" || event.code === "NumpadEnter" ? this.register() : this.doNothing();

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    set emailSync(value: string) {
        this._email = value.trim();
    }

    get emailSync(): string {
        return this._email.trim().toLowerCase();
    }

    get password(): string {
        return this._password;
    }

    set password(value: string) {
        this._password = value;
    }

    get repeatPassword(): string {
        return this._repeatPassword;
    }

    set repeatPassword(value: string) {
        this._repeatPassword = value;
    }

    private doNothing: any = function(data: any = null): any {
        if (data) {
            // console.debug(data);
        }
    };


    log(event: any): void {
        // console.debug(event);
    }

    get surname(): string {
        return this._surname.trim();
    }

    set surname(value: string) {
        this._surname = value.trim();
    }

    get firstname(): string {
        return this._firstname.trim();
    }

    set firstname(value: string) {
        this._firstname = value.trim();
    }

    get username(): string {
        return this._username.trim().replace(" ", "_");
    }

    set username(value: string) {
        this._username = value.trim().replace(" ", "_");
    }

    get passwordControl(): FormControl {
        return this._passwordControl;
    }

    set passwordControl(value: FormControl) {
        this._passwordControl = value;
    }

    set repeatPasswordControl(value: FormControl) {
        this._repeatPasswordControl = value;
    }

    get repeatPasswordControl(): FormControl {
        return this._repeatPasswordControl;
    }

    get surnameControl(): FormControl {
        return this._surnameControl;
    }

    set surnameControl(value: FormControl) {
        this._surnameControl = value;
    }

    get firstnameControl(): FormControl {
        return this._firstnameControl;
    }

    set firstnameControl(value: FormControl) {
        this._firstnameControl = value;
    }

    get usernameControl(): FormControl {
        return this._usernameControl;
    }

    set usernameControl(value: FormControl) {
        this._usernameControl = value;
    }

    get emailControl(): FormControl {
        return this._emailControl;
    }

    set emailControl(value: FormControl) {
        this._emailControl = value;
    }

}
