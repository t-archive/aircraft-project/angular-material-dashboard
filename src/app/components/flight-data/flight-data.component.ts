import {AfterViewInit, Component, EventEmitter, Inject, Input, OnDestroy, OnInit, Output, ViewChild} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Aircraft} from "../../aircrafts/Aircraft";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {BehaviorSubject, Observable, ObservableInput, Subject, Subscription} from "rxjs";
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Dictionary} from "../../keys/Dictionary";
import {FlightDataService} from "../../services/flight-data.service";
import set = Reflect.set;
import {ActivatedRoute, ParamMap, Route, Router} from "@angular/router";
import {switchMap} from "rxjs/operators";
import {log} from "util";
import {ChartModalComponent} from "../chart-modal/chart-modal.component";
import {ChartDialogDataInterface} from "../../interfaces/Ichart-dialog-data";
import {FlightDetailsModalComponent} from "../flight-details-modal/flight-details-modal.component";
import DateTimeFormat = Intl.DateTimeFormat;
import {FlightDetailsModalData} from "../../interfaces/Iflight-details-modal-data";
import {FlightDetails, IflightDetails} from "../../interfaces/iflight-details";
import {FlightTimeSpanData} from "../../interfaces/Iflight-time-span-data";
import {FlightDataTime} from "../../interfaces/Iflight-data-time";
import {IflightBasicData} from "../../interfaces/Iflight-basic-data";
import {environment} from "../../../environments/environment";
import {ModalLoadingComponent} from "../modal-loading/modal-loading.component";

@Component({
    selector: "app-flight-data",
    templateUrl: "./flight-data.component.html",
    styleUrls: ["./flight-data.component.scss"]
})
export class FlightDataComponent implements OnInit, AfterViewInit, OnDestroy {
    private _aircraft: Aircraft | CivilAircraft | MilitaryAircraft;
    private _aircraftSubject: Subject<Aircraft | CivilAircraft | MilitaryAircraft> = new Subject<Aircraft | CivilAircraft | MilitaryAircraft>();
    private startVar: Subscription;
    private _showInfoData: boolean;
    private _linkFlightNumber: string;
    private _flightDetails: IflightDetails;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    @ViewChild(MatSort, {static: true}) private sort: MatSort;
    @Output() inputFlightData: EventEmitter<any> = new EventEmitter();
    private _linkAircraftID: string;
    private _linkBase: string;

    @Input()
    set aircraftObject(aircraftObject: Aircraft | CivilAircraft | MilitaryAircraft) {
        this._aircraftSubject.next(aircraftObject);
        this._aircraft = aircraftObject;
    }

    @Input()
    set showInfoData(showInfoData: boolean) {
        this._showInfoData = showInfoData;
    }

    @Input()
    set linkFlightNumber(linkFlightNumber: string) {
        this._linkFlightNumber = linkFlightNumber;
    }

    private _data: any;

    private _picture: any;

    private _icaoCode: string;

    private _iataCode: string;
    private _dataSource: any = new MatTableDataSource([]);
    private _dictionary: Dictionary = new Dictionary();
    public readonly displayedColumnsInfo: string[] = ["key", "value"];
    public itemSwitch: boolean = false;
    private _detailsCloseClick: boolean = false;
    private _startTimePoint: any;
    private _endTimePoint: any;
    private _detailsCurrentAircraft: IflightBasicData | boolean;
    private _displayedColumns: string[] = ["8", "9", "11", "12", "13", "status", "airline", "button"];
    private _loadingSpinnerSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private _loadingSpinner: any = null;

    constructor(@Inject(HttpClient) private http: HttpClient,
                @Inject(FlightDataService) private flightData: FlightDataService,
                @Inject(MatDialog) private dialog: MatDialog,
                @Inject(ActivatedRoute) private route: ActivatedRoute) {
        this._loadingSpinnerSubject.subscribe((data: any) => {
            this._loadingSpinner = data;
        });
    }

    ngOnInit(): any {
        this.start();
        this._aircraftSubject.subscribe((data) => {
            this._aircraft = data;
            this.start();
            this.beforeStart();
        });
    }

    private start(): void {
        this._linkAircraftID = this.route.snapshot.paramMap.get("id");
        this._linkBase = this.route.snapshot.paramMap.get("typ");
        this._linkFlightNumber = this.route.snapshot.paramMap.get("flightNumber");
        this.dataSource.sort = this.sort;
        this.startVar = this.flightData.start(this._aircraft).subscribe((data) => {
            this.picture = this.flightData.aircraftListData[data.icao].picture;
            this.data = this.flightData.aircraftListData[data.icao].dataSource.data;
            this.dataSource = this.flightData.aircraftListData[data.icao].dataSource;
            this.iataCode = this.flightData.aircraftListData[data.icao].result.iata;
            this.icaoCode = this.flightData.aircraftListData[data.icao].result.icao;
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
            this.inputFlightData.emit(this);
            // console.log(this.dataSource.data);
        });
        if (this.dataSource.paginator) {
            this.dataSource.paginator._intl.firstPageLabel = this.dictionary.firstPageLabel;
            this.dataSource.paginator._intl.lastPageLabel = this.dictionary.lastPageLabel;
            this.dataSource.paginator._intl.nextPageLabel = this.dictionary.nextPageLabel;
            this.dataSource.paginator._intl.previousPageLabel = this.dictionary.previousPageLabel;
            this.dataSource.paginator._intl.itemsPerPageLabel = this.dictionary.itemsPerPageLabel;

            let lengthData: number = this.dataSource.data.length;
            if (this.dataSource.data.length !== this.dataSource.filteredData.length) {
                lengthData = this.dataSource.filteredData.length;
            }
            this.dataSource.paginator._length = lengthData;
            this.dataSource.paginator._pageSizeOptions = [5, 10, 15, 20, 25, 50, lengthData];
            // this.pageSizeOptions = [5, 10, 15, 20, 25, 50, lengthData];
        }

    }

    private beforeStart(): void {
        this.picture = null;
        this.data = null;
        this.dataSource = null;
        this.iataCode = null;
        this.icaoCode = null;
    }

    ngOnDestroy(): void {
        this.flightData.destroy();
        this.startVar.unsubscribe();
    }

    ngAfterViewInit(): void {
        this.dataSource.sort = this.sort;
        this.inputFlightData.emit(this);
    }

    private showDetails(element: IflightBasicData): boolean {
        this._loadingSpinnerSubject.next(false);
        // const dialogLoad: MatDialogRef<any> = this.dialog.open(ModalLoadingComponent, {
        //     data: null,
        //     autoFocus: false
        // });
        this.flightDetails = new FlightDetails({
            action: true,
            data: element,
            previewPicture: null,
            airlineLogo: null,
            details: {},
            age: "NaN",
            time: {}
        });

        this.flightData.getMoreDetails(element.id).subscribe((moredata: any) => {
            this.flightDetails["details"] = moredata.data;
            this.flightDetails["time"] = new FlightDataTime({
                departure: null,
                arrival: null,
                departureTime: null,
                arrivalTime: null,
                flightTime: null,
                flightTimeCalc: null,
                flightTimePercent: null,
                flightTimeData: null
            });
            if (this.flightDetails.details["airline"].code && this.flightDetails.details["airline"].code["icao"]) {
                this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/airlineLogo/iata/" + this.flightDetails.details["airline"].code["iata"] + "/icao/" + this.flightDetails.details["airline"].code["icao"] + "").subscribe((data: any) => {
                    this.flightDetails.airlineLogo = data.data.link;
                });
            } else {
                this.http.get(environment.baseApiEndpoint + "/aircrafts/flightData/airlineLogo/iata/null/icao/NoAirlineLogo").subscribe((data: any) => {
                    this.flightDetails.airlineLogo = data.data.link;
                });
            }
            if (this.flightDetails["details"].aircraft.images.sideview) {
                this.flightDetails["previewPicture"] = this.flightDetails["details"].aircraft.images.sideview;
            } else if (this.flightDetails["details"].aircraft.images.large.length > 0) {
                this.flightDetails["previewPicture"] = this.flightDetails["details"].aircraft.images.large[0].src;
            }
            if (this.flightDetails["aircraft"] && this.flightDetails["aircraft"]["age"]["date"]) {
                this.flightDetails["age"] = this.flightDetails["aircraft"]["age"]["date"];
            }
            if (this.flightDetails["details"]["time"]) {
                if (this.flightDetails["details"]["time"]["real"]["departure"] !== null) {
                    this.flightDetails["time"]["departure"] = this.flightDetails["details"]["time"]["real"]["departure"];
                } else if (this.flightDetails["details"]["time"]["estimated"]["departure"] !== null) {
                    this.flightDetails["time"]["departure"] = this.flightDetails["details"]["time"]["estimated"]["departure"];
                } else if (this.flightDetails["details"]["time"]["scheduled"]["departure"] !== null) {
                    this.flightDetails["time"]["departure"] = this.flightDetails["details"]["time"]["scheduled"]["departure"];
                } else {
                    this.flightDetails["time"]["departure"] = -1;
                    console.warn("no-departure");
                }
                if (this.flightDetails["details"]["time"]["real"]["arrival"] !== null) {
                    this.flightDetails["time"]["arrival"] = this.flightDetails["details"]["time"]["real"]["arrival"];
                } else if (this.flightDetails["details"]["time"]["estimated"]["arrival"] !== null) {
                    this.flightDetails["time"]["arrival"] = this.flightDetails["details"]["time"]["estimated"]["arrival"];
                } else if (this.flightDetails["details"]["time"]["scheduled"]["arrival"] !== null) {
                    this.flightDetails["time"]["arrival"] = this.flightDetails["details"]["time"]["scheduled"]["arrival"];
                } else {
                    this.flightDetails["time"]["arrival"] = -1;
                    console.warn("no-arrival");
                }
                if (this.flightDetails["time"]["arrival"] >= 0 && this.flightDetails["details"]["airport"].destination) {
                    // this.flightDetails["time"]["arrivalTime"] = new Date(this.flightDetails["time"]["arrival"] * 1000).toLocaleString("de-DE", {timeZone: this.flightDetails["details"]["airport"].destination.timezone.name});
                    this.flightDetails["time"]["arrivalTime"] = this.date(this.flightDetails["time"]["arrival"], this.flightDetails["details"]["airport"].destination.timezone.name);
                }
                if (this.flightDetails["time"]["departure"] >= 0 && this.flightDetails["details"]["airport"].origin) {
                    // this.flightDetails["time"]["departureTime"] = new Date(this.flightDetails["time"]["departure"] * 1000).toLocaleString("de-DE", {timeZone: this.flightDetails["details"]["airport"].origin.timezone.name});
                    this.flightDetails["time"]["departureTime"] = this.date(this.flightDetails["time"]["departure"], this.flightDetails["details"]["airport"].origin.timezone.name);
                }
                if (this.flightDetails["details"]["time"]["historical"] && this.flightDetails["details"]["time"]["historical"]["delay"] !== null && this.flightDetails["details"]["time"]["historical"]["flighttime"] !== null) {


                    this.flightDetails["time"]["flightTime"] = Number(this.flightDetails["details"]["time"]["historical"]["flighttime"]);
                    const departureTimezone: string = this.flightDetails["details"]["airport"].origin.timezone.name;
                    let arrivalTimezone: string = departureTimezone;
                    if (this.flightDetails["details"]["airport"].destination) {
                        arrivalTimezone = this.flightDetails["details"]["airport"].destination.timezone.name;
                    }
                    this._startTimePoint = new Date().toLocaleString("de-DE", {timeZone: departureTimezone});
                    this._endTimePoint = new Date().toLocaleString("de-DE", {timeZone: arrivalTimezone});
                    const date: Date = new Date();
                    let arrival: number = Number((((this.flightDetails["time"]["arrival"] * 1000) - (date.getTime())) / 1000).toFixed(0)); // bis zur Landung
                    this.flightDetails["time"]["flightTimeCalc"] = this.flightDetails["time"]["arrival"] - this.flightDetails["time"]["departure"];
                    let alreadyFlown: number = this.flightDetails["time"]["flightTimeCalc"] - arrival; // Schon geflogen
                    this.flightDetails["time"]["flightTimePercent"] = ((alreadyFlown * 100) / this.flightDetails["time"]["flightTimeCalc"]);
                    if (arrival < 0) {
                        arrival = 0;
                    }
                    if (alreadyFlown < 0) {
                        alreadyFlown = 0;
                    }
                    this.flightDetails["time"]["flightTimeData"] = new FlightTimeSpanData({
                        alreadyFlown: alreadyFlown,
                        arrival: arrival,
                        alreadyFlownDate: this.formatSecondsToHHMM(alreadyFlown),
                        arrivalDate: this.formatSecondsToHHMM(arrival)
                    });
                }
                // console.log(this.flightDetails);
            }
            const timeReloadInterval: any = setInterval(() => {
                if (this.flightDetails && this.flightDetails["details"] && this.flightDetails["details"]["airport"]) {
                    if (this.flightDetails.action === false || !this.flightDetails["details"]["airport"].destination) {
                        clearInterval(timeReloadInterval);
                    } else {
                        this._startTimePoint = new Date().toLocaleString("de-DE", {timeZone: this.flightDetails["details"]["airport"].origin.timezone.name});
                        this._endTimePoint = new Date().toLocaleString("de-DE", {timeZone: this.flightDetails["details"]["airport"].destination.timezone.name});

                        const date: Date = new Date();
                        let arrival: number = Number((((this.flightDetails["time"]["arrival"] * 1000) - (date.getTime())) / 1000).toFixed(0)); // bis zur Landung
                        this.flightDetails["time"]["flightTimeCalc"] = this.flightDetails["time"]["arrival"] - this.flightDetails["time"]["departure"];
                        // if (this.flightDetails.details && this.flightDetails.details["time"] && this.flightDetails.details["time"]["historical"] && Number(this.flightDetails.details["time"]["historical"]["delay"]) < 0) {
                        // this.flightDetails["time"]["flightTimeCalc"] = this.flightDetails["time"]["flightTimeCalc"] - Number(this.flightDetails.details["time"]["historical"]["delay"]);
                        // }
                        let alreadyFlown: number = this.flightDetails["time"]["flightTimeCalc"] - arrival; // Schon geflogen
                        this.flightDetails["time"]["flightTimePercent"] = Number(((alreadyFlown * 100) / this.flightDetails["time"]["flightTimeCalc"]).toFixed(2));
                        if (arrival < 0) {
                            arrival = 0;
                        }
                        if (alreadyFlown < 0) {
                            alreadyFlown = 0;
                        }
                        if (arrival <= 60 && arrival > 0) {
                            this.flightDetails["time"]["flightTimeData"] = new FlightTimeSpanData({
                                alreadyFlown: alreadyFlown,
                                arrival: arrival,
                                alreadyFlownDate: this.formatSecondsToHHMM(alreadyFlown),
                                arrivalDate: this.formatSeconds(arrival)
                            });
                        } else {
                            this.flightDetails["time"]["flightTimeData"] = new FlightTimeSpanData({
                                alreadyFlown: alreadyFlown,
                                arrival: arrival,
                                alreadyFlownDate: this.formatSecondsToHHMM(alreadyFlown),
                                arrivalDate: this.formatSecondsToHHMM(arrival)
                            });
                        }

                    }
                }
            }, 1000);

            // const dialogRef: MatDialogRef<any> = this.dialog.open(FlightDetailsModalComponent, {
            //     data: new FlightDetailsModalData({
            //         aircraft: element,
            //         flightDetails: this.flightDetails,
            //         self: this
            //     }),
            //     autoFocus: false
            // });
            const dialogRef: MatDialogRef<any> = this.dialog.open(FlightDetailsModalComponent, {
                data: new FlightDetailsModalData({
                    aircraft: element,
                    flightDetails: this.flightDetails,
                    self: this
                }),
                autoFocus: false
            });
            // console.log(this.flightDetails);
            // dialogRef.componentInstance.data = new FlightDetailsModalData({
            //     aircraft: element,
            //     flightDetails: this.flightDetails,
            //     self: this
            // });
            this._loadingSpinnerSubject.next(true);
            dialogRef.afterClosed().subscribe(() => {
                this._loadingSpinnerSubject.next(null);
            });
        });

        this._detailsCurrentAircraft = element;
        return true;
    }

    private date(timeStamp: number, timezone: string = "Europe/Berlin"): any {
        return new Date(timeStamp * 1000).toLocaleString("de-DE", {timeZone: timezone});
        // return new Date(timeStamp * 1000).toLocaleString("de-DE", {timeZone: "Europe/Berlin"});
    }

    private formatSeconds(seconds: number): any {
        const date: Date = new Date(1970, 0, 1);
        date.setSeconds(seconds);
        return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
    }

    private formatSecondsToHHMM(seconds: number): any {
        const date: Date = new Date(1970, 0, 1);
        date.setSeconds(seconds);
        // return date.toTimeString() + ":" + date.getMinutes();
        return date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1").substring(0, 5);
    }

    public applyFilter(event: Event): void {
        const filterValue: any = (event.target as HTMLInputElement).value;
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    get icaoCode(): string {
        return this._icaoCode;
    }


    set icaoCode(value: string) {
        this._icaoCode = value;
    }

    get iataCode(): string {
        return this._iataCode;
    }

    set iataCode(value: string) {
        this._iataCode = value;
    }

    get displayedColumns(): string[] {
        return this._displayedColumns;
    }

    set displayedColumns(value: string[]) {
        this._displayedColumns = value;
    }

    get dataSource(): any {
        return this._dataSource;
    }

    set dataSource(value: any) {
        this._dataSource = value;
    }


    get picture(): any {
        return this._picture;
    }


    set picture(value: any) {
        this._picture = value;
    }

    get flightDetails(): IflightDetails {
        return this._flightDetails;
    }

    set flightDetails(value: IflightDetails) {
        this._flightDetails = value;
    }

    get data(): any {
        return this._data;
    }


    set data(value: any) {
        this._data = value;
    }

    get detailsCloseClick(): boolean {
        return this._detailsCloseClick;
    }

    set detailsCloseClick(value: boolean) {
        this._detailsCloseClick = value;
    }

    get startTimePoint(): any {
        return this._startTimePoint;
    }

    set startTimePoint(value: any) {
        this._startTimePoint = value;
    }

    get endTimePoint(): any {
        return this._endTimePoint;
    }

    set endTimePoint(value: any) {
        this._endTimePoint = value;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftSubject(): Subject<Aircraft | CivilAircraft | MilitaryAircraft> {
        return this._aircraftSubject;
    }

    /**
     0  = ICAO 24-bit Address
     1  = Latitude
     2  = Longitude
     3  = Track
     4  = Calibrated altitude
     5  = Ground speed
     6  = Squawk
     7  =
     8  = Aircraft type (ICAO Code)
     9  = Registration
     10 =
     11 = Abflug Flughafen
     12 = Ziel Flughafen
     13 = Flugnummer
     14 =
     15 = Vertical speed
     16 = Flug Route
     17 =
     18 = Airline Kürzel
     */

    get aircraft(): Aircraft | CivilAircraft | MilitaryAircraft {
        return this._aircraft;
    }

    get loadingSpinnerSubject(): BehaviorSubject<boolean> {
        return this._loadingSpinnerSubject;
    }

    set loadingSpinnerSubject(value: BehaviorSubject<boolean>) {
        this._loadingSpinnerSubject = value;
    }

    get loadingSpinner(): boolean {
        return this._loadingSpinner;
    }

    set loadingSpinner(value: boolean) {
        this._loadingSpinner = value;
    }

}
