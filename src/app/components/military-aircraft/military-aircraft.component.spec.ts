import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { MilitaryAircraftComponent } from "./military-aircraft.component";

describe("MilitaryAircraftComponent", () => {
  let component: MilitaryAircraftComponent;
  let fixture: ComponentFixture<MilitaryAircraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MilitaryAircraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MilitaryAircraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
