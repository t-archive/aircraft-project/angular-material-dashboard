import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { CivilAircraftComponent } from "./civil-aircraft.component";

describe("CivilAircraftComponent", () => {
  let component: CivilAircraftComponent;
  let fixture: ComponentFixture<CivilAircraftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CivilAircraftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CivilAircraftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
