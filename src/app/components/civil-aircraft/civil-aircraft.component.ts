import {Component, Inject, OnInit, ViewChild} from "@angular/core";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {AircraftService} from "../../services/AircraftService";
import {DesignClass} from "../../class/DesignClass";
import {StoreService} from "../../services/store.service";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Aircraft} from "../../aircrafts/Aircraft";
import {EditModalComponent} from "../edit-modal/edit-modal.component";
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from "@angular/material";
import {Dictionary} from "../../keys/Dictionary";
import {CreateModalComponent} from "../create-modal/create-modal.component";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {LoginService} from "../../services/login.service";

@Component({
    selector: "app-civil-aircraft",
    templateUrl: "./civil-aircraft.component.html",
    styleUrls: ["./civil-aircraft.component.css"]
})
export class CivilAircraftComponent implements OnInit {


    private _civilAircrafts: Array<CivilAircraft>;
    private _designClass: DesignClass = new DesignClass();
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _dictionary: Dictionary = new Dictionary();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
    private searchFilter: any;
    displayedColumns: string[] = [
        this._aircraftKeys.id,
        this._aircraftKeys.name,
        this._aircraftKeys.distance,
        this._aircraftKeys.seatCount,
        this._aircraftKeys.maxSeatCount,
        this._aircraftKeys.length,
        this._aircraftKeys.wingSpan,
        this._aircraftKeys.height,
        this._aircraftKeys.consumption,
        this._aircraftKeys.gateLicense,
        "buttonAction"
    ];
    private _dataSource: any;

    constructor(@Inject(AircraftService) private _aircraftService: AircraftService,
                @Inject(StoreService) private store: StoreService,
                @Inject(LoginService) private loginService: LoginService,
                @Inject(MatDialog) private dialog: MatDialog) {
        this._civilAircrafts = this._aircraftService.civilAircrafts;
        this._aircraftService.civilAircraftsBehaviorSubject.subscribe(aircraftRepository => {
            if (aircraftRepository instanceof AircraftRepository) {

                this._civilAircrafts = aircraftRepository.aircrafts;
                this.dataSource = new MatTableDataSource(aircraftRepository.aircrafts);
                this.setParms();
            }
        });
    }

    ngOnInit(): void {
        this.setParms();
        this.store.filterTextBehaviorSubject.subscribe(message => {
            this._aircraftService.civilAircraftsFilter(message);
        });

    }

    public applyFilter(filterValue: string): void {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    private checkFilter(): void {
        if (this.searchFilter) {
            if (this.dataSource.filter !== this.searchFilter) {
                this.applyFilter(this.searchFilter);
            }
        }

    }

    private setParms(): void {
        this.checkFilter();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        if (this.dataSource.paginator) {
            this.dataSource.paginator._intl.firstPageLabel = this.dictionary.firstPageLabel;
            this.dataSource.paginator._intl.lastPageLabel = this.dictionary.lastPageLabel;
            this.dataSource.paginator._intl.nextPageLabel = this.dictionary.nextPageLabel;
            this.dataSource.paginator._intl.previousPageLabel = this.dictionary.previousPageLabel;
            this.dataSource.paginator._intl.itemsPerPageLabel = this.dictionary.itemsPerPageLabel;

            let lengthData: number = this.dataSource.data.length;
            if (this.dataSource.data.length !== this.dataSource.filteredData.length) {
                lengthData = this.dataSource.filteredData.length;
            }
            this.dataSource.paginator._length = lengthData;
            this.dataSource.paginator._pageSizeOptions = [5, 10, 15, 20, 25, 50, lengthData];
        }
    }

    public openEditModal(aircraft: Aircraft): void {
        // aircraft.name = aircraft.name + "TEST";
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: aircraft,
            autoFocus: false
        });
    }

    public openCreateModal(): void {
        const dialogRef: any = this.dialog.open(CreateModalComponent, {
            data: {
                behaviorSubject: this._aircraftService.civilAircraftsBehaviorSubject,
                type: this.aircraftKeys.civil,
            },
            autoFocus: false
        });
    }

    // sort(key: string): void {
    //     this._aircraftService.civilAircraftsSort(key);
    // }
    public delete(id: number): void {
        this._aircraftService.civilAircraftDelete(id);
    }

    get civilAircrafts(): Array<CivilAircraft> {
        return this._civilAircrafts;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get designClass(): DesignClass {
        return this._designClass;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get dataSource(): any {
        return this._dataSource;
    }

    set dataSource(value: any) {
        this._dataSource = value;
    }
}
