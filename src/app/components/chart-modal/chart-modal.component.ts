import {AfterViewInit, Component, ContentChildren, Inject, OnInit, QueryList, ViewChild, ViewContainerRef} from "@angular/core";
import {MAT_DIALOG_DATA, MatSort, MatTab, MatBadge} from "@angular/material";
import {Aircraft} from "../../aircrafts/Aircraft";
import {ChartService} from "../../services/chart.service";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {IchartDialogData} from "../../interfaces/Ichart-dialog-data";
import {Chart} from "chart.js";
import {DesignClass} from "../../class/DesignClass";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {AircraftRepository} from "../../repository/AircraftRepository";
import {UnitExtension} from "../../keys/UnitExtension";
import {CreateChart} from "../../class/CreateChart";
import {FlightDataService} from "../../services/flight-data.service";

@Component({
    selector: "app-chart-modal",
    templateUrl: "./chart-modal.component.html",
    styleUrls: ["./chart-modal.component.scss"]
})
export class ChartModalComponent implements OnInit, AfterViewInit {
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();

    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private designClass: DesignClass = new DesignClass();
    private _chart: Chart;
    private _clickButton: boolean = false;
    private _wait: boolean = true;
    private _similarAircrafts: Array<Aircraft | CivilAircraft | MilitaryAircraft> = [];
    @ViewChild("canvasAircraftCharts", {static: false}) private canvasAircraftCharts: any;

    constructor(@Inject(MAT_DIALOG_DATA) private data: IchartDialogData,
                @Inject(ChartService) private chartService: ChartService) {
    }

    private _createChart: CreateChart = new CreateChart(this.chartService);

    ngOnInit(): any {

    }

    ngAfterViewInit(): void {
        this.start();
    }

    public next(event: any): any {
    }

    public start(): void {
        const data: any = this.chartService.setData(this.data, "chart-modal");
        this._similarAircrafts = data.similarAircrafts;
        this.makeChart(data);
    }

    private startClick(): void {
        setTimeout(() => this.start(), 1);
    }

    public makeChart(data: any, HtmlId: any = "canvas"): void {
        this._createChart.make(data, this.data, HtmlId, false);
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get clickButton(): boolean {
        return this._clickButton;
    }

    set clickButton(value: boolean) {
        this._clickButton = value;
    }

    get wait(): boolean {
        return this._wait;
    }

    set wait(value: boolean) {
        this._wait = value;
    }


    log($event: any): void {
    }
}
