import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material";
import {Aircraft} from "../../aircrafts/Aircraft";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {CivilAircraft} from "../../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../../aircrafts/MilitaryAircraft";
import {Dictionary} from "../../keys/Dictionary";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {AircraftService} from "../../services/AircraftService";
import {HTTPService} from "../../services/http.service";
import {AircraftVariable} from "../../class/AircraftVariable";
import {InformationNotificationService} from "../../services/information-notification.service";

@Component({
    selector: "app-edit-modal",
    templateUrl: "./edit-modal.component.html",
    styleUrls: ["./edit-modal.component.css"]
})
export class EditModalComponent implements OnInit {


    private _aircraftVariable: AircraftVariable = new AircraftVariable("edit");
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _type: string = "";


    private _id: number;
    private _name: string;
    private _distance: number;
    private _seatCount: number;
    private _maxSeatCount: number;
    private _length: number;
    private _wingSpan: number;
    private _height: number;
    private _consumption: number;
    private _gateLicense: boolean;
    private _flare: boolean;
    private _airRefueling: boolean;
    private _dataStore: object;
    private _submitButton: boolean;
    private _submitButtonDisabled: string = "disabled";

    constructor(@Inject(MAT_DIALOG_DATA) private data: Aircraft, @Inject(AircraftService) private _aircraftService: AircraftService, @Inject(HTTPService) private httpService: HTTPService, @Inject(InformationNotificationService) private informationNotification: InformationNotificationService) {
        this._id = this.data.id;
        this._name = this.data.name;
        this._distance = this.data.distance;
        this._seatCount = this.data.seatCount;
        this._maxSeatCount = this.data.maxSeatCount;
        this._length = this.data.length;
        this._wingSpan = this.data.wingSpan;
        this._height = this.data.height;
        this._consumption = this.data.consumption;

        if (this.data instanceof CivilAircraft) {
            this._dataStore = this.data as CivilAircraft;
            this.type = this.aircraftKeys.civil;
            this._gateLicense = this.data.gateLicense;
        } else if (this.data instanceof MilitaryAircraft) {
            this._dataStore = this.data as MilitaryAircraft;
            this.type = this.aircraftKeys.military;
            this._flare = this.data.flare;
            this._airRefueling = this.data.airRefueling;
        } else {
            this.informationNotification.createToast("error", this._dictionary.pleaseCloseThisWindowModal);
            console.error("DEV: This Aircrafts is not valid!");
        }
    }

    ngOnInit(): void {

    }

    update(): void {
        if (this.type === this.aircraftKeys.military) {
            this.submitButton = true;
        } else if (this.type === this.aircraftKeys.civil) {
            this.submitButton = true;
        }
        this.submitButton = this.aircraftVariable.submitRelease();
    }

    save(): void {

        if (this.type === this.aircraftKeys.military) {
            const newAircraft: MilitaryAircraft = new MilitaryAircraft(this.name, this.distance, this.seatCount, this.maxSeatCount, this.length, this.wingSpan, this.height, this.consumption);
            newAircraft.id = this.id;
            newAircraft.flare = this.flare;
            newAircraft.airRefueling = this.airRefueling;
            this.postBySave(newAircraft);

        } else if (this.type === this.aircraftKeys.civil) {
            const newAircraft: CivilAircraft = new CivilAircraft(this.name, this.distance, this.seatCount, this.maxSeatCount, this.length, this.wingSpan, this.height, this.consumption);
            newAircraft.gateLicense = this.gateLicense;
            newAircraft.id = this.id;
            this.postBySave(newAircraft);

        }
    }

    postBySave(newAircraftObject: Aircraft | CivilAircraft | MilitaryAircraft): void {
        if (this.type === this.aircraftKeys.civil) {

            this.httpService.putCivilAircraft(newAircraftObject as CivilAircraft).subscribe(
                (success) => {
                    const status: boolean = this.httpService.checkIfNoError(success);
                    if (status === true) {
                        const newAircraft: IcivilAircraftData = {
                            id: this.id,
                            name: this.name,
                            distance: this.distance,
                            seatCount: this.seatCount,
                            maxSeatCount: this.maxSeatCount,
                            length: this.length,
                            wingSpan: this.wingSpan,
                            height: this.height,
                            consumption: this.consumption,
                            gateLicense: this.gateLicense
                        };
                        this._aircraftService.civilAircraftEdit(this.data, newAircraft);
                    } else {
                        this.informationNotification.editFailed(newAircraftObject);
                    }
                },
                error => console.error(error)
            );
        } else if (this.type === this.aircraftKeys.military) {

            this.httpService.putMilitaryAircraft(newAircraftObject as MilitaryAircraft).subscribe(
                (success) => {
                    const status: boolean = this.httpService.checkIfNoError(success);
                    if (status === true) {
                        const newAircraft: ImilitaryAircraftData = {
                            id: this.id,
                            name: this.name,
                            distance: this.distance,
                            seatCount: this.seatCount,
                            maxSeatCount: this.maxSeatCount,
                            length: this.length,
                            wingSpan: this.wingSpan,
                            height: this.height,
                            consumption: this.consumption,
                            airRefueling: this.airRefueling,
                            flare: this.flare
                        };
                        this._aircraftService.militaryAircraftEdit(this.data, newAircraft);
                    } else {
                        this.informationNotification.editFailed(newAircraftObject);
                    }
                },
                error => console.error(error)
            );
        }
    }

    maxTitleLength(text: string, max: number): string {
        if (text && max) {
            if (text.length > max) {
                return text.substring(0, (max - 4)).trim() + "...";
            } else {
                return text.trim();
            }
        } else {
            console.warn("edit-modal.component.ts - maxTitleLength(): No Text or Max");
            return text.trim();
        }
    }

    set type(value: string) {
        this.aircraftVariable.setStatus(value);
        this._type = value;
    }

    get type(): string {
        return this._type;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }


    get airRefueling(): boolean {
        return this._airRefueling;
    }

    set airRefueling(value: boolean) {
        this.aircraftVariable.check(value, "airRefueling");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._airRefueling = false;
        } else if (oldValue === "true") {
            this._airRefueling = true;
        }
        this.update();
    }

    get flare(): boolean {
        return this._flare;
    }

    set flare(value: boolean) {
        this.aircraftVariable.check(value, "flare");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._flare = false;
        } else if (oldValue === "true") {
            this._flare = true;
        }
        this.update();
    }

    get gateLicense(): boolean {
        return this._gateLicense;
    }

    set gateLicense(value: boolean) {
        this.aircraftVariable.check(value, "gateLicense");
        // @ts-ignore
        const oldValue: string = value;
        if (oldValue === "false") {
            this._gateLicense = false;
        } else if (oldValue === "true") {
            this._gateLicense = true;
        }
        this.update();
    }

    get consumption(): number {
        return this._consumption;
    }

    set consumption(value: number) {
        this.aircraftVariable.check(value, "consumption");
        this._consumption = value;
        this.update();
    }

    get height(): number {
        return this._height;
    }

    set height(value: number) {
        this.aircraftVariable.check(value, "height");
        this._height = value;
        this.update();
    }

    get wingSpan(): number {
        return this._wingSpan;
    }

    set wingSpan(value: number) {
        this.aircraftVariable.check(value, "wingSpan");
        this._wingSpan = value;
        this.update();
    }

    get length(): number {
        return this._length;
    }

    set length(value: number) {
        this.aircraftVariable.check(value, "length");
        this._length = value;
        this.update();
    }

    get maxSeatCount(): number {
        return this._maxSeatCount;
    }

    set maxSeatCount(value: number) {
        this.aircraftVariable.check(value, "maxSeatCount");
        this._maxSeatCount = value;
        this.update();
    }

    get seatCount(): number {
        return this._seatCount;
    }

    set seatCount(value: number) {
        this.aircraftVariable.check(value, "seatCount");
        this._seatCount = value;
        this.update();
    }

    get distance(): number {
        return this._distance;
    }

    set distance(value: number) {
        this.aircraftVariable.check(value, "distance");
        this._distance = value;
        this.update();
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this.aircraftVariable.check(value, "name");
        this._name = value;
        this.update();
    }


    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
        this.update();
    }

    get aircraftVariable(): AircraftVariable {
        return this._aircraftVariable;
    }

    set aircraftVariable(value: AircraftVariable) {
        this._aircraftVariable = value;
    }

    get submitButton(): boolean {
        return this._submitButton;
    }

    set submitButton(value: boolean) {

        this._submitButton = value;
    }


}
