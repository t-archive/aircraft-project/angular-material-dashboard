import {AfterViewInit, Component, ElementRef, HostListener, Inject, OnDestroy, OnInit, PipeTransform, ViewChild} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material";
import {IchartDialogData} from "../../interfaces/Ichart-dialog-data";
import {Dictionary} from "../../keys/Dictionary";
import {IflightDetailsModalData} from "../../interfaces/Iflight-details-modal-data";
import {IflightDetails} from "../../interfaces/Iflight-details";
import {ActivatedRoute} from "@angular/router";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {FlightDataService} from "../../services/flight-data.service";
import * as Chart from "chart.js";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {LoginService} from "../../services/login.service";

@Component({
    selector: "app-modal-loading",
    templateUrl: "./modal-loading.component.html",
    styleUrls: ["./modal-loading.component.scss"]
})
export class ModalLoadingComponent implements OnInit, OnDestroy {


    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
    }

    ngOnDestroy(): void {

    }

    ngOnInit(): void {

    }
}
