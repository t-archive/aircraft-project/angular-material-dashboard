import {Component, Inject, OnInit} from "@angular/core";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {Dictionary} from "../../keys/Dictionary";
import {LoginService} from "../../services/login.service";
import {falseIfMissing} from "protractor/built/util";
import {FormControl, Validators} from "@angular/forms";
import {Subject} from "rxjs";

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _dictionary: Dictionary = new Dictionary();
    private _email: string = "";
    private _password: string = "";
    private _status: boolean = true;

    constructor(@Inject(LoginService) private _loginService: LoginService) {
    }

    ngOnInit(): void {
    }

    private login(): void {
        const subject: Subject<boolean> = this._loginService.login(this.email, this.password);
        // subject.subscribe((data: boolean) => {
        //     this._status = data;
        //     if (this._status === true) {
        //         this._loginService.router.navigate(["/all"]);
        //     }
        // });
    }

    private keyDownFunction: any = (event: KeyboardEvent): any => event.code === "Enter" || event.code === "NumpadEnter" ? this.login() : this.doNothing();

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    set emailSync(value: string) {
        this._email = value.trim();
    }

    get emailSync(): string {
        return this._email.trim().toLowerCase();
    }

    get password(): string {
        return this._password;
    }

    set password(value: string) {
        this._password = value;
    }
    private doNothing: any = function(data: any = null): any {
        if (data) {
            console.log(data);
        }
    };

    log(event: any): void {
        console.log(event);
    }
}
