import {AfterViewInit, Component, ElementRef, HostListener, Inject, OnDestroy, OnInit, PipeTransform, ViewChild} from "@angular/core";
import {MAT_DIALOG_DATA} from "@angular/material";
import {IchartDialogData} from "../../interfaces/Ichart-dialog-data";
import {Dictionary} from "../../keys/Dictionary";
import {IflightDetailsModalData} from "../../interfaces/Iflight-details-modal-data";
import {IflightDetails} from "../../interfaces/Iflight-details";
import {ActivatedRoute} from "@angular/router";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {FlightDataService} from "../../services/flight-data.service";
import * as Chart from "chart.js";
import {UnitExtension} from "../../keys/UnitExtension";
import {AircraftTitle} from "../../keys/AircraftTitle";
import {AircraftKeys} from "../../keys/AircraftKeys";
import {LoginService} from "../../services/login.service";

@Component({
    selector: "app-flight-details-modal",
    templateUrl: "./flight-details-modal.component.html",
    styleUrls: ["./flight-details-modal.component.scss"]
})
export class FlightDetailsModalComponent implements OnInit, OnDestroy, AfterViewInit {
    private _flightDetails: IflightDetails;
    private wordSelectAorB: string;
    private _dictionary: Dictionary = new Dictionary();
    private _unitExtension: UnitExtension = new UnitExtension();
    private _aircraftTitle: AircraftTitle = new AircraftTitle();
    private _aircraftKeys: AircraftKeys = new AircraftKeys();
    private _slideIndex: number = 1;
    private _iframeLink: string = "./";
    private myLineChart: Chart;
    // private _previewPictureHeight: number;
    @ViewChild("previewPicture", {static: true}) elementView: ElementRef;
    arrivalHTML: string;

    @HostListener("window:resize", ["$event"])
    onResize($event: any): void {
        const innerWidth: any = $event.target.innerWidth;
        this.resizePreviewButtonFunction();
    }

    constructor(@Inject(MAT_DIALOG_DATA) private data: IflightDetailsModalData, @Inject(DomSanitizer) private _sanitizer: DomSanitizer,
                @Inject(FlightDataService) private flightData: FlightDataService,
                @Inject(LoginService) private loginService: LoginService) {
        this.data.self.loadingSpinnerSubject.subscribe((getData: boolean) => {
            if (getData === true) {
                this.flightDetails = this.data.flightDetails;
                this.wordSelectAorB = this.flightDetails.data.result.name.includes("Airbus") ? "dem" : this.flightDetails.data.result.name.includes("Boeing") ? "der" : "der/dem";
            }

        });

    }

    ngOnInit(): void {
        this.loginService.renew();
    }

    ngAfterViewInit(): void {
        this.data.self.loadingSpinnerSubject.subscribe((getData: boolean) => {
            if (getData === true) {
                this.showDivs(1);
                this.resizePreviewButtonFunction();
                this.chart();
            }
        });
        this.showDivs(1);
        this.resizePreviewButtonFunction();
        this.chart();
    }

    private date(timeStamp: number, timezone: string = "Europe/Berlin"): any {
        return new Date(timeStamp * 1000).toLocaleString("de-DE", {timeZone: timezone});
        // return new Date(timeStamp * 1000).toLocaleString("de-DE", {timeZone: "Europe/Berlin"});
    }

    private chart(): void {
        const alt: Array<number> = [0];
        const spd: Array<number> = [0];
        const altPercent: Array<number> = [];
        const spdPercent: Array<number> = [];
        let altMax: number = 0;
        let spdMax: number = 0;
        const time: Array<any> = [];
        for (const value of this.flightDetails.details["trail"]) {
            if (value.alt !== 0) {
                alt.push(value.alt);
                if (value.alt > altMax) {
                    altMax = value.alt;
                }
                spd.push(value.spd);
                if (value.spd > spdMax) {
                    spdMax = value.spd;
                }
                // time.push(this.date(value.ts));
                time.push("");
            }
        }
        alt.reverse();
        spd.reverse();
        time.reverse();
        for (const value of this.flightDetails.details["trail"]) {
            if (value.alt !== 0) {
                altPercent.push(((value.alt * 100) / altMax) * 2.5);
                // spdPercent.push(value.spd);
                spdPercent.push(((value.spd * 100) / spdMax) * 2.5);
            }
        }
        altPercent.reverse();
        spdPercent.reverse();
        this.myLineChart = new Chart("canvasAircraftHistory", {
            type: "line",
            data: {
                labels: time,
                datasets: [{
                    data: spdPercent,
                    label: this.dictionary.groundSpeed,
                    borderColor: "#8e5ea2",
                    fill: true,

                }, {
                    data: altPercent,
                    label: this.dictionary.height,
                    borderColor: "#3e95cd",
                    fill: true
                }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                tooltips: {
                    enabled: false,
                    callbacks: {
                        // title: (tooltipItem: any, data: any) => {
                        //     return tooltipItem[0]["value"].toLocaleString();
                        // }
                    },
                },
                title: {
                    display: false,
                    text: ""
                },
                legend: {
                    display: true,
                    position: "bottom",
                    labels: {
                        boxWidth: 20,
                        fontColor: "#111",
                        padding: 15
                    }
                },
            }
        });
    }

    private plusDivs(n: number): void {
        this.showDivs(this.slideIndex += n);
    }

    private showDivs(n: number): boolean {
        let i: number;
        const x: any = document.getElementsByClassName("mySlides");
        if (n > x.length) {
            this.slideIndex = 1;
        }
        if (n < 1) {
            this.slideIndex = x.length;
        }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[this.slideIndex - 1].style.display = "block";
        return false;
    }

    private resizePreviewButton(): void {
        setTimeout(() => {
            this.resizePreviewButtonFunction();
        }, 250);
    }

    private resizePreviewButtonFunction(): void {
        if (this.data.self.loadingSpinner) {
            const htmlElement: any = document.getElementsByClassName("preview-image-button");

            for (let i: number = 0; i < htmlElement.length; i++) {
                const e: any = htmlElement[i];
                if (e instanceof HTMLElement) {
                    if (this.elementView.nativeElement.offsetHeight > 0) {
                        e.style.height = (Math.round(this.elementView.nativeElement.offsetHeight) + 2.7).toString() + "px";
                    } else {
                        this.resizePreviewButton();
                    }
                }
            }
        } else {
            this.data.self.loadingSpinnerSubject.subscribe((data: boolean) => {
                if (data === true) {
                    this.resizePreviewButtonFunction();
                }
            });
        }

    }

    get flightDetails(): IflightDetails {
        return this._flightDetails;
    }

    set flightDetails(value: IflightDetails) {
        this._flightDetails = value;
    }

    get dictionary(): Dictionary {
        return this._dictionary;
    }

    private getSafeUrl(url: string): any {
        return this._sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    ngOnDestroy(): void {
        this.flightDetails.action = false;
    }

    get slideIndex(): number {
        return this._slideIndex;
    }

    set slideIndex(value: number) {
        this._slideIndex = value;
    }

    private activateIframe(): void {
        if (this._iframeLink === "./") {
            // this._iframeLink = this.getSafeUrl("https://www.flightradar24.com/simple?callsign=" + this.flightDetails.data["16"] + "&solo=1&airports=1&z=8&size=auto");
            this._iframeLink = this.getSafeUrl("https://www.openstreetmap.org/#map=6/51.300/5.000");
            this._iframeLink = this.getSafeUrl("https://www.openstreetmap.org/export/embed.html?bbox=-16.083984375000004%2C44.60611274517393%2C20.610351562500004%2C57.136239319177434&amp;layer=mapnik");
        } else {
            this._iframeLink = "./";
        }
    }


    get aircraftKeys(): AircraftKeys {
        return this._aircraftKeys;
    }

    get aircraftTitle(): AircraftTitle {
        return this._aircraftTitle;
    }

    get unitExtension(): UnitExtension {
        return this._unitExtension;
    }

}
