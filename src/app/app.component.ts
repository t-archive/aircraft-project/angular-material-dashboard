import {Component, EventEmitter, Inject, OnInit, Output} from "@angular/core";
import {DOCUMENT} from "@angular/common";
import {StoreService} from "./services/store.service";
import {MatDialog} from "@angular/material";
import {EditModalComponent} from "./components/edit-modal/edit-modal.component";
import {Dictionary} from "./keys/Dictionary";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LoginService} from "./services/login.service";



@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})

export class AppComponent implements OnInit {
    private _dictionary: Dictionary = new Dictionary();
    private _title: string = "aircraftAngularFrontend";

    private _isActive: string = "";
    private _isActiveHome: string = "";
    private _isActiveAll: string = "";
    private _isActiveLogin: string = "";
    private _isActiveCivil: string = "";
    private _isActiveMilitary: string = "";
    private _isActiveCounter: number = 0;
    // https://creazilla.com/nl/nodes/3015-airbus-a350-silhouet
    private _logoColor: any = {
        "lightblue": "4bb7ee",
        "blue": "2993fc",
        "black": "000000",
        "orange": "fd8c25",
        "red": "f44545",
        "lightgreen": "4edd69",
        "yellow": "fed530",
        "purple": "ac61da",
        "green": "09630b",
        "outline": "outline"
    };
    private _logoColorHex: string = "";


    constructor(@Inject(DOCUMENT) private document: Document,
                @Inject(StoreService) private store: StoreService,
                @Inject(MatDialog) private dialog: MatDialog,
                @Inject(Router) private router: Router,
                @Inject(ActivatedRoute) private route: ActivatedRoute,
                @Inject(LoginService) private loginService: LoginService,
    ) {
        this._logoColorHex = this._logoColor.orange;
        this.linkActiveNav(this.document.location.pathname.substr(1));

    }
    ngOnInit(): void {

    }

    public receiveInputSearch($event: string): void {
        this.store.filterTextBehaviorSubject.next($event);
    }

    public alertMessageTS(): void {
        if (this._isActive === "is-active") {
            this._isActive = "";
        } else {
            this._isActive = "is-active";
        }

    }


    public openEditModal(): void {
        const dialogRef: any = this.dialog.open(EditModalComponent, {
            data: {name: "austin"},
            autoFocus: false
        });
    }


    public linkActiveNav(page: string): void {
        if (this._isActiveCounter !== 0) {
            this.alertMessageTS();
        }
        this._isActiveCounter++;
        let now: string;
        if (page) {
            now = "/" + page;
        } else {
            now = this.document.location.pathname;
        }

        this.linkActiveNavClear();
        if (now === "/home") {
            if (this._isActiveHome === "is-active") {
                this._isActiveHome = "";
            } else {
                this._isActiveHome = "is-active";
            }
        } else if (now === "/civil") {
            if (this._isActiveCivil === "is-active") {
                this._isActiveCivil = "";
            } else {
                this._isActiveCivil = "is-active";
            }
        } else if (now === "/military") {
            if (this._isActiveMilitary === "is-active") {
                this._isActiveMilitary = "";
            } else {
                this._isActiveMilitary = "is-active";
            }
        } else if (now === "/all") {
            if (this._isActiveAll === "is-active") {
                this._isActiveAll = "";
            } else {
                this._isActiveAll = "is-active";
            }
        } else if (now === "/login") {
            if (this._isActiveLogin === "is-active") {
                this._isActiveLogin = "";
            } else {
                this._isActiveLogin = "is-active";
            }
        } else {
            this.linkActiveNavClear();
        }
    }


    private linkActiveNavClear(): void {
        this._isActiveHome = "";
        this._isActiveAll = "";
        this._isActiveMilitary = "";
        this._isActiveCivil = "";
    }

    get isActiveCounter(): number {
        return this._isActiveCounter;
    }

    set isActiveCounter(value: number) {
        this._isActiveCounter = value;
    }

    get isActiveMilitary(): string {
        return this._isActiveMilitary;
    }

    set isActiveMilitary(value: string) {
        this._isActiveMilitary = value;
    }

    get isActiveCivil(): string {
        return this._isActiveCivil;
    }

    set isActiveCivil(value: string) {
        this._isActiveCivil = value;
    }

    get isActiveAll(): string {
        return this._isActiveAll;
    }

    set isActiveAll(value: string) {
        this._isActiveAll = value;
    }

    get isActiveHome(): string {
        return this._isActiveHome;
    }

    set isActiveHome(value: string) {
        this._isActiveHome = value;
    }

    get isActive(): string {
        return this._isActive;
    }

    set isActive(value: string) {
        this._isActive = value;
    }

    get title(): string {
        return this._title;
    }

    set title(value: string) {
        this._title = value;
    }

    get logoColorHex(): string {
        return this._logoColorHex;
    }

    set logoColorHex(value: string) {
        this._logoColorHex = value;
    }

    get isActiveLogin(): string {
        return this._isActiveLogin;
    }

    set isActiveLogin(value: string) {
        this._isActiveLogin = value;
    }
}

