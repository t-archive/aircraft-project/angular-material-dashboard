import {
    AIRCRAFTS_KEY, AIRREFUELING_KEY,
    CIVIL_KEY,
    CONSUMPTION_KEY,
    DATA_KEY, DISTANCE_KEY, FLARE_KEY, GATELICENSE_KEY,
    HEIGHT_KEY,
    ID_KEY, JSON_AIRCRAFT_KEY,
    LENGTH_KEY, MAXSEATCOUNT_KEY, MILITARY_KEY,
    NAME_KEY, SEATCOUNT_KEY,
    WINGSPAN_KEY
} from "../keys/AircraftKeys";
import {CivilAircraft} from "../aircrafts/CivilAircraft";
import {MilitaryAircraft} from "../aircrafts/MilitaryAircraft";

import {Aircraft} from "../aircrafts/Aircraft";
import {checkIfBoolExists, checkIfNumberExists, checkIfObjectExists, checkIfStringExists} from "./FactoryHelper";

export abstract class AircraftFactory {

    /**
     * @param rawData
     */
    public static createAllAircrafts(rawData: any): Array<Aircraft> {
        const allAircrafts: Array<Aircraft> = [];
        if (typeof rawData === "object"
            && checkIfObjectExists(rawData, DATA_KEY)
            && checkIfObjectExists(rawData[DATA_KEY], CIVIL_KEY)
            && checkIfObjectExists(rawData[DATA_KEY], MILITARY_KEY)) {

            for (const rawForAircraft of rawData[DATA_KEY][CIVIL_KEY][AIRCRAFTS_KEY]) {
                const civilAircraft: CivilAircraft = this.createCivilAircraft(rawForAircraft);
                if (civilAircraft) {
                    allAircrafts.push(civilAircraft);
                }
            }
            for (const rawForAircraft of rawData[DATA_KEY][MILITARY_KEY][AIRCRAFTS_KEY]) {
                const militaryAircraft: MilitaryAircraft = this.createMilitaryAircraft(rawForAircraft);
                if (militaryAircraft) {
                    allAircrafts.push(militaryAircraft);
                }
            }
        }
        return allAircrafts;
    }

    /**
     * @param rawData
     */
    public static createCivilAircrafts(rawData: any): Array<CivilAircraft> {
        const civilAircrafts: Array<CivilAircraft> = [];
        if (typeof rawData === "object"
            && checkIfObjectExists(rawData, DATA_KEY)
            && checkIfObjectExists(rawData[DATA_KEY], CIVIL_KEY)) {

            for (const rawForAircraft of rawData[DATA_KEY][CIVIL_KEY][AIRCRAFTS_KEY]) {
                const civilAircraft: CivilAircraft = this.createCivilAircraft(rawForAircraft);
                if (civilAircraft) {
                    civilAircrafts.push(civilAircraft);
                }
            }
        }
        return civilAircrafts;
    }


    public static createMilitaryAircrafts(rawData: any): Array<MilitaryAircraft> {
        const resultAircrafts: Array<MilitaryAircraft> = [];
        if (typeof rawData === "object"
            && checkIfObjectExists(rawData, DATA_KEY)
            && checkIfObjectExists(rawData[DATA_KEY], MILITARY_KEY)) {
            for (const rawForAircraft of rawData[DATA_KEY][MILITARY_KEY][AIRCRAFTS_KEY]) {
                const militaryAircraft: MilitaryAircraft = this.createMilitaryAircraft(rawForAircraft);
                if (militaryAircraft) {
                    resultAircrafts.push(militaryAircraft);
                }
            }
        }
        return resultAircrafts;
    }

    /**
     * @param rawAircraft
     */
    public static createCivilAircraft(rawAircraft: any): CivilAircraft {
        let civilAircraft: CivilAircraft;
        if (this.isAircraftRawValid(rawAircraft)
            && checkIfBoolExists(rawAircraft, GATELICENSE_KEY)) {
            civilAircraft = new CivilAircraft(
                rawAircraft[NAME_KEY],
                rawAircraft[DISTANCE_KEY],
                rawAircraft[SEATCOUNT_KEY],
                rawAircraft[MAXSEATCOUNT_KEY],
                rawAircraft[LENGTH_KEY],
                rawAircraft[WINGSPAN_KEY],
                rawAircraft[HEIGHT_KEY],
                rawAircraft[CONSUMPTION_KEY]
            );
            civilAircraft.id = rawAircraft[ID_KEY];
            civilAircraft.gateLicense = rawAircraft[GATELICENSE_KEY];
        }
        return civilAircraft;
    }

    /**
     * @param rawAircraft
     */
    public static createMilitaryAircraft(rawAircraft: any): MilitaryAircraft {
        let militaryAircraft: MilitaryAircraft;
        if (this.isAircraftRawValid(rawAircraft)
            && checkIfBoolExists(rawAircraft, FLARE_KEY)
            && checkIfBoolExists(rawAircraft, AIRREFUELING_KEY)) {
            militaryAircraft = new MilitaryAircraft(
                rawAircraft[NAME_KEY],
                rawAircraft[DISTANCE_KEY],
                rawAircraft[SEATCOUNT_KEY],
                rawAircraft[MAXSEATCOUNT_KEY],
                rawAircraft[LENGTH_KEY],
                rawAircraft[WINGSPAN_KEY],
                rawAircraft[HEIGHT_KEY],
                rawAircraft[CONSUMPTION_KEY]
            );
            militaryAircraft.id = rawAircraft[ID_KEY];
            militaryAircraft.flare = rawAircraft[FLARE_KEY];
            militaryAircraft.airRefueling = rawAircraft[AIRREFUELING_KEY];
        }
        return militaryAircraft;
    }

    /**
     * @param rawAircraft
     */
    private static isAircraftRawValid(rawAircraft: any): boolean {
        return checkIfStringExists(rawAircraft, NAME_KEY)
            && checkIfNumberExists(rawAircraft, DISTANCE_KEY)
            && checkIfNumberExists(rawAircraft, SEATCOUNT_KEY)
            && checkIfNumberExists(rawAircraft, MAXSEATCOUNT_KEY)
            && checkIfNumberExists(rawAircraft, LENGTH_KEY)
            && checkIfNumberExists(rawAircraft, WINGSPAN_KEY)
            && checkIfNumberExists(rawAircraft, HEIGHT_KEY)
            && checkIfNumberExists(rawAircraft, CONSUMPTION_KEY)
            && checkIfNumberExists(rawAircraft, ID_KEY);
    }
    public static jsonToReadySendPostJSON(json: object): object {
        return {
            [JSON_AIRCRAFT_KEY]: json
        };
    }
}
