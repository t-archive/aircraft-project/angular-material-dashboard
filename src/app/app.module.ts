import {BrowserModule, Title} from "@angular/platform-browser";
import {NgModule} from "@angular/core";

import {AppRoutingModule} from "./app-routing.module";
import {AppComponent} from "./app.component";
import {HomeComponent} from "./components/home/home.component";
import {AllAircraftComponent} from "./components/all-aircraft/all-aircraft.component";
import {CivilAircraftComponent} from "./components/civil-aircraft/civil-aircraft.component";
import {MilitaryAircraftComponent} from "./components/military-aircraft/military-aircraft.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {SearchAircraftsComponent} from "./components/search-aircrafts/search-aircrafts.component";
import {EditModalComponent} from "./components/edit-modal/edit-modal.component";
import {
    MatButtonModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    MatGridListModule,
    MatCardModule,
    MatTabsModule,
    MatProgressBarModule,
    MatBadgeModule,
    MatExpansionModule,
    MatTooltipModule
} from "@angular/material";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {CreateModalComponent} from "./components/create-modal/create-modal.component";
import {HttpClientModule} from "@angular/common/http";
import {MaterialModule} from "./material.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {ChartModalComponent} from "./components/chart-modal/chart-modal.component";
import {ChartsModule} from "ng2-charts";
import {AircraftCardComponent} from "./components/aircraft-card/aircraft-card.component";
import {CdkTableModule} from "@angular/cdk/table";
import {FlightDataComponent} from "./components/flight-data/flight-data.component";
import {DetailPageComponent} from "./components/detail-page/detail-page.component";
import {FlightDetailsModalComponent} from "./components/flight-details-modal/flight-details-modal.component";
import {LoginComponent} from "./components/login/login.component";
import {RegisterComponent} from "./components/register/register.component";
import { ModalLoadingComponent } from "./components/modal-loading/modal-loading.component";






@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        AllAircraftComponent,
        CivilAircraftComponent,
        MilitaryAircraftComponent,
        SearchAircraftsComponent,
        EditModalComponent,
        CreateModalComponent,
        ChartModalComponent,
        AircraftCardComponent,
        FlightDataComponent,
        DetailPageComponent,
        FlightDetailsModalComponent,
        LoginComponent,
        RegisterComponent,
        ModalLoadingComponent
    ],
    imports: [
        BrowserModule,
        MaterialModule,
        AppRoutingModule,
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        BrowserModule,
        // import HttpClientModule after BrowserModule.
        HttpClientModule,
        MatButtonModule,
        MatButtonToggleModule,
        FlexLayoutModule,
        MatTableModule,
        MatSortModule,
        MatInputModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatGridListModule,
        MatCardModule,
        ReactiveFormsModule,
        ChartsModule,
        MatTabsModule,
        MatProgressBarModule,
        MatBadgeModule,
        CdkTableModule,
        MatExpansionModule,
        MatTooltipModule

    ],
    exports: [
        MatDialogModule
    ],
    providers: [Title],
    bootstrap: [AppComponent],
    entryComponents: [EditModalComponent, CreateModalComponent, ChartModalComponent, FlightDataComponent, FlightDetailsModalComponent, ModalLoadingComponent]
})
export class AppModule {
}
