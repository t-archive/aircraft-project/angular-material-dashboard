import {LANGUAGE_KEY_GERMAN} from "../app/keys/config";

const baseApiEndpoint: string = "http://192.168.10.39/aircraft/index.php";
const localStorageDeleteTime: number = 172800;
const language: string = LANGUAGE_KEY_GERMAN;
export const environment: any = {
    production: true,
    baseApiEndpoint: baseApiEndpoint,
    allAircraftURL: baseApiEndpoint + "/aircrafts",
    civilAircraftURL: baseApiEndpoint + "/aircrafts/civil",
    militaryAircraftURL: baseApiEndpoint + "/aircrafts/military",
    loginURL: baseApiEndpoint + "/aircrafts/login",
    registerURL: baseApiEndpoint + "/aircrafts/register",
    deleteAircraftURL: baseApiEndpoint + "/aircrafts/id/",
    localStorageDeleteTime: localStorageDeleteTime,
    language: language
};
