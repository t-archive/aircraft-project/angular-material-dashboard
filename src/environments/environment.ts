// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {LANGUAGE_KEY_ENGLISH, LANGUAGE_KEY_GERMAN} from "../app/keys/config";

const baseApiEndpoint: string = "http://192.168.10.39/aircraft/index.php";
const localStorageDeleteTime: number = 600;
const language: string = LANGUAGE_KEY_GERMAN;
export const environment: any = {
    production: false,
    baseApiEndpoint: baseApiEndpoint,
    allAircraftURL: baseApiEndpoint + "/aircrafts",
    civilAircraftURL: baseApiEndpoint + "/aircrafts/civil",
    militaryAircraftURL: baseApiEndpoint + "/aircrafts/military",
    loginURL: baseApiEndpoint + "/aircrafts/login",
    registerURL: baseApiEndpoint + "/aircrafts/register",
    deleteAircraftURL: baseApiEndpoint + "/aircrafts/id/",
    localStorageDeleteTime: localStorageDeleteTime,
    language: language
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
